//
//  Presenter.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

import Foundation

class Presenter: NSObject, EntityProtocol
{
    weak var delegate: PresenterDelegate?
    
    override init()
    {
        super.init()
    }

    // MARK: Callback FUNC
    
    func sendApiGetUserInfo()
    {
        if (!getLoginStatus())
        {
            self.delegate?.onSendApiGetUserInfo(errCode: -999)
            
            return
        }
        
        let token = SSBKeychain.password(forService: KEYCHAIN_ACCESS_GROUPS, account: KEYCHAIN_USER_AUTH_TOKEN)
        
        let uuid = SSBKeychain.password(forService: KEYCHAIN_ACCESS_GROUPS, account: KEYCHAIN_USER_UUID)
        
        let cloudManager:HiroiaCloudManager = HiroiaCloudManager.sharedInstance() as! HiroiaCloudManager
        
        cloudManager.sendCmdGetInfo(withToken: token, uuid: uuid)
        {
            (hiroiaResultStruc) in
            
            hiroiaResultStruc?.printSelf()
            
            if (hiroiaResultStruc?.errCode != 0)
            {
                Utilities.run
                {
                    self.delegate?.onSendApiGetUserInfo(errCode: hiroiaResultStruc!.errCode)
                }
            }
            else
            {
                let dic:NSDictionary = hiroiaResultStruc?.result as! NSDictionary
                
                let userDic:NSDictionary = dic.object(forKey: "user") as! NSDictionary
                
                let userStruc = UserStruc.init(dictionary: userDic as? [AnyHashable : Any])
                
                self.runLoginStepToSaveData(userStruc: userStruc!, token: token!, uuid: uuid!)
                
                self.getImageWithUrl(url: userStruc!.imageUrl)
                
                Utilities.run
                {
                    self.delegate?.onSendApiGetUserInfo(errCode: hiroiaResultStruc!.errCode)
                }
            }
        }
    }
    
    func sendApiLoginWith(acc:String, pwd:String, longitude:Double, latitude:Double)
    {
        let cloudManager:HiroiaCloudManager = HiroiaCloudManager.sharedInstance() as! HiroiaCloudManager
        
        cloudManager.sendCmdLogin(withEmail: acc, password: pwd, longitude: longitude, latitude: latitude)
        {
            (hiroiaResultStruc) in
            
            hiroiaResultStruc!.printSelf()
            
            if (hiroiaResultStruc?.errCode != 0)
            {
                Utilities.run
                {
                    self.delegate?.onSendApiLoginWith(errCode: hiroiaResultStruc!.errCode)
                }
            }
            else
            {
                let dic:NSDictionary = hiroiaResultStruc!.result as! NSDictionary
                
                let token:String = dic.object(forKey: "token") as! String
                
                let userDic:NSDictionary = dic.object(forKey: "user") as! NSDictionary
                
                let uuid:String = userDic.object(forKey: "uuid") as! String
                
                let userStruc = UserStruc.init(dictionary: userDic as? [AnyHashable : Any])
                
                self.runLoginStepToSaveData(userStruc: userStruc!, token: token, uuid: uuid)
                
                self.getImageWithUrl(url: userStruc!.imageUrl)
                
                Utilities.run
                {
                    self.delegate?.onSendApiLoginWith(errCode: hiroiaResultStruc!.errCode)
                }
            }
        }
    }
    
    func sendApiRegWith(acc:String, pwd:String, name:String, longitude:Double, latitude:Double)
    {
        let cloudManager:HiroiaCloudManager = HiroiaCloudManager.sharedInstance() as! HiroiaCloudManager
        
        let languageKey = MultiLangManager.getCurLangKey()
        
        cloudManager.sendCmdCreate(withEmail: acc, password: pwd, nickName: name, lang: languageKey, longitude: longitude, latitude: latitude)
        {
            (hiroiaResultStruc) in
            
            hiroiaResultStruc?.printSelf()
            
            if (hiroiaResultStruc!.errCode != 0)
            {
                Utilities.run
                {
                    self.delegate?.onSendApiRegWith(errCode: hiroiaResultStruc!.errCode)
                }
            }
            else
            {
                let dic:NSDictionary = hiroiaResultStruc!.result as! NSDictionary
                
                let token:String = dic.object(forKey: "token") as! String
                
                let userDic:NSDictionary = dic.object(forKey: "user") as! NSDictionary
                
                let uuid:String = userDic.object(forKey: "uuid") as! String
                
                let userStruc = UserStruc.init(dictionary: userDic as? [AnyHashable : Any])
                
                self.runLoginStepToSaveData(userStruc: userStruc!, token: token, uuid: uuid)
                
                self.getImageWithUrl(url: userStruc!.imageUrl)
                
                Utilities.run
                {
                    self.delegate?.onSendApiRegWith(errCode: hiroiaResultStruc!.errCode)
                }
            }
        }
    }
    
    func sendApiReSendActiviteEmail(acc:String)
    {
        let cloudManager:HiroiaCloudManager = HiroiaCloudManager.sharedInstance() as! HiroiaCloudManager
        
        cloudManager.sendCmdReSendActiviteMail(acc)
        {
            (hiroiaResultStruc) in
            
            hiroiaResultStruc!.printSelf()
            
            Utilities.run
            {
                self.delegate?.onSendApiReSendActiviteEmail(errCode: hiroiaResultStruc!.errCode)
            }
        }
    }
    
    func sendApiForgetWith(acc:String)
    {
        let cloudManager:HiroiaCloudManager = HiroiaCloudManager.sharedInstance() as! HiroiaCloudManager
        
        let languageKey = MultiLangManager.getCurLangKey()
        
        cloudManager.sendCmdResetPassword(withEmail: acc, lang: languageKey!)
        {
            (hiroiaResultStruc) in
            
            hiroiaResultStruc?.printSelf()

            Utilities.run
            {
                self.delegate?.onSendApiForgetWith(errCode: hiroiaResultStruc!.errCode)
            }
        }
    }    
    
    func sendApiDetectFace(imageData:Data)
    {
        let faceManager:FaceApiCloudManager = FaceApiCloudManager.sharedInstance() as! FaceApiCloudManager
        
        faceManager.sendCmdDetectFace(with: imageData)
        {
            (faceApiResultStruc:FaceApiResultStruc?) in
            
            faceApiResultStruc?.printSelf()
            
            Utilities.run
            {
                self.delegate?.onSendApiDetectFace(errCode: faceApiResultStruc!.errCode, result: faceApiResultStruc!.result as Any)
            }
        }
    }
    
    // MARK: Return FUNC
    
    func getUserInfo() -> UserStruc?
    {
        if (getLoginStatus())
        {
            return getUserStruc()
        }
        else
        {
            return nil
        }
    }
}


//MARK: - PresenterDelegate

protocol PresenterDelegate: class
{
    func onSendApiGetUserInfo(errCode:Int)
    func onSendApiLoginWith(errCode:Int)
    func onSendApiReSendActiviteEmail(errCode:Int)
    func onSendApiRegWith(errCode:Int)
    func onSendApiForgetWith(errCode:Int)
    
    func onSendApiDetectFace(errCode:Int, result:Any)
}

extension PresenterDelegate
{
    func onSendApiGetUserInfo(errCode:Int)
    {
        print("Presenter onGetUserInfoDone errCode = " + String(errCode))
    }
    
    func onSendApiLoginWith(errCode:Int)
    {
        print("Presenter onSendApiLoginWith errCode = " + String(errCode))
    }
    
    func onSendApiReSendActiviteEmail(errCode:Int)
    {
        print("Presenter onSendApiReSendActiviteEmail errCode = " + String(errCode))
    }
    
    func onSendApiRegWith(errCode:Int)
    {
        print("Presenter onSendApiRegWith errCode = " + String(errCode))
    }
    
    func onSendApiForgetWith(errCode:Int)
    {
        print("Presenter onSendApiForgetWith errCode = " + String(errCode))
    }
    
    func onSendApiDetectFace(errCode:Int, result:Any)
    {
        print("Presenter onSendApiDetectFace errCode = " + String(errCode))
    }
}


//MARK: - AlertProtocol

protocol AlertProtocol: class
{
    func showAlertUserOrPwdError(handler: @escaping AlertHandler)
    func showAlertUserNotActivated(handler: @escaping AlertHandler)
    func showAlertNetworkError(errCode:Int, handler: @escaping AlertHandler)
    func showAlertActiviteSendDone(handler: @escaping AlertHandler)
    func showAlertAccHasUse(handler: @escaping AlertHandler)
    func showAlertRegSucces(handler: @escaping AlertHandler)
    func showAlertResetPwdSucces(acc:String, handler: @escaping AlertHandler)
}

extension AlertProtocol
{
    func showAlertUserOrPwdError(handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertAccPwdError(self, handler: handler)
    }
    
    func showAlertUserNotActivated(handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertNotActivatedError(self, handler: handler)
    }
    func showAlertNetworkError(errCode:Int, handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertNetworkError(withCode: errCode, delegate: self, handler: handler)
    }
    func showAlertActiviteSendDone(handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertSendedDone(self, handler: handler)
    }
    func showAlertAccHasUse(handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertAccHasUsed(self, handler: handler)
    }
    func showAlertRegSucces(handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertRegDone(self, handler: handler)
    }
    func showAlertResetPwdSucces(acc:String, handler: @escaping AlertHandler)
    {
        AlertSupport.showAlertResetPwdDone(withAcc: acc, delegate: self, handler: handler)
    }
}
