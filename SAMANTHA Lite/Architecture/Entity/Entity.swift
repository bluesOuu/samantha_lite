//
//  Entity.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/21.
//

import Foundation

class Entity: NSObject
{
    static let sharedInstance = Entity()
    
    var userStruc:UserStruc?
    
    var faceStruc:FaceStruc?
    
    override init()
    {
        super.init()
    }
}

protocol EntityProtocol: class
{
    func getLoginStatus() -> Bool
    func setLoginStatus(loginStatus: Bool)
    
    func getUserStruc() -> UserStruc
    func setUserStruc(userStruc: UserStruc)
    
    func runLoginStepToSaveData(userStruc: UserStruc, token: String, uuid: String)
    func runLogoutStep()
    
    func getFaceStruc() -> FaceStruc
    func setFaceStruc(faceStruc: FaceStruc)
    
    func getImageWithUrl(url: String)
}

extension EntityProtocol
{    
    func getLoginStatus() -> Bool
    {
        return BOUserInfo.getBooleanForKey(LOGIN_STATUS)
    }
    
    func setLoginStatus(loginStatus: Bool)
    {
        BOUserInfo.setBoolean(loginStatus, forKey: LOGIN_STATUS)
    }
    
    func getUserStruc() -> UserStruc
    {
        return Entity.sharedInstance.userStruc!
    }
    
    func setUserStruc(userStruc: UserStruc)
    {
        Entity.sharedInstance.userStruc = userStruc
    }
    
    func getFaceStruc() -> FaceStruc
    {
        return Entity.sharedInstance.faceStruc!
    }
    
    func setFaceStruc(faceStruc: FaceStruc)
    {
        Entity.sharedInstance.faceStruc = faceStruc
    }

    func runLoginStepToSaveData(userStruc: UserStruc, token: String, uuid: String)
    {
        setUserStruc(userStruc: userStruc)
        
        SSBKeychain.setPassword(token, forService: KEYCHAIN_ACCESS_GROUPS, account: KEYCHAIN_USER_AUTH_TOKEN)
        
        SSBKeychain.setPassword(uuid, forService: KEYCHAIN_ACCESS_GROUPS, account: KEYCHAIN_USER_UUID)
        
        setLoginStatus(loginStatus: true)
    }
    
    func runLogoutStep()
    {
        Entity.sharedInstance.userStruc = nil
        
        SSBKeychain.deletePassword(forService: KEYCHAIN_ACCESS_GROUPS, account: KEYCHAIN_USER_AUTH_TOKEN)
        
        SSBKeychain.deletePassword(forService: KEYCHAIN_ACCESS_GROUPS, account: KEYCHAIN_USER_UUID)

        BOUserInfo.remove(forKey: LOGIN_STATUS)
    }
    
    func getImageWithUrl(url: String)
    {
        print("getImageWithUrl = " + url)
        
        SDWebImageManager.shared.loadImage(with: URL.init(string: url), options: SDWebImageOptions.continueInBackground, progress: nil)
        {
            (uiImage, data, error, sdImageCacheType, bool, uRL) in
            
            if (uiImage != nil)
            {
                Entity.sharedInstance.userStruc!.imageData = uiImage!.pngData()!
            }
        }
    }
}
