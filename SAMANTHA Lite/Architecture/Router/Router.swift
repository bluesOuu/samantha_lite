//
//  Router.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/19.
//

import Foundation

class Router: NSObject
{
    static let sharedInstance = Router()
    
    let window = UIWindow(frame: UIScreen.main.bounds);
    
    var indexHomeNavi:BaseNavigationController? = nil
    
    var lgNavi:BaseNavigationController? = nil
    
    override init()
    {
        super.init()
    }
    
    // MARK: - Window Root View
    
    func showSplashView()
    {
        let svs: UIStoryboard = UIStoryboard.init(name: "SplashView", bundle: Bundle.main)
                
        let svc: SplashViewController = svs.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        
        window.rootViewController = svc
        
        window.makeKeyAndVisible()
    }
    
    func gotoIndexHomeView()
    {
        if (indexHomeNavi == nil)
        {
            let ihvs: UIStoryboard = UIStoryboard.init(name: "IndexHomeView", bundle: Bundle.main)
                
            let ihvc: IndexHomeViewController = ihvs.instantiateViewController(withIdentifier: "IndexHomeViewController") as! IndexHomeViewController
        
            indexHomeNavi = BaseNavigationController.init(rootViewController: ihvc)
        }
        
        Utilities.addAnimation(ANI_FLIP, direction: ANI_RIGHT, duration: 1, onTheView: window, completion: nil)
        
        window.rootViewController = indexHomeNavi;
        
        lgNavi = nil
    }
    
    func gotoLoginView()
    {
        let lvs: UIStoryboard = UIStoryboard.init(name: "LoginView", bundle: Bundle.main)
            
        let lvc: LoginViewController = lvs.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        lgNavi = BaseNavigationController.init(rootViewController: lvc)
        
        Utilities.addAnimation(ANI_FLIP, direction: ANI_RIGHT, duration: 1, onTheView: window, completion: nil)
        
        window.rootViewController = lgNavi;
        
//        indexHomeNavi = nil
    }
    
    // MARK: - GET VC
    
    func getIndexNavigation() -> BaseNavigationController
    {
        return indexHomeNavi!
    }
    
    func getHomeView() -> HomeViewController
    {
        let hvs: UIStoryboard = UIStoryboard.init(name: "HomeView", bundle: Bundle.main)
            
        let hvc: HomeViewController = hvs.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        return hvc
    }
    
    func getRegView() -> RegViewController
    {
        let rvs: UIStoryboard = UIStoryboard.init(name: "RegView", bundle: Bundle.main)
            
        let rvc: RegViewController = rvs.instantiateViewController(withIdentifier: "RegViewController") as! RegViewController
        
        return rvc
    }
    
    func getForgetView() -> ForgetViewController
    {
        let fvs: UIStoryboard = UIStoryboard.init(name: "ForgetView", bundle: Bundle.main)
            
        let fvc: ForgetViewController = fvs.instantiateViewController(withIdentifier: "ForgetViewController") as! ForgetViewController
        
        return fvc
    }
    
    func getMyView() -> MyViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "MyView", bundle: Bundle.main)
            
        let mvc: MyViewController = mvs.instantiateViewController(withIdentifier: "MyViewController") as! MyViewController
        
        return mvc
    }
    
    func getMyDetailView() -> MyDetailViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "MyDetailView", bundle: Bundle.main)
            
        let mvc: MyDetailViewController = mvs.instantiateViewController(withIdentifier: "MyDetailViewController") as! MyDetailViewController
        
        return mvc
    }
    
    func getRecentView() -> RecentViewController
    {
        let rvs: UIStoryboard = UIStoryboard.init(name: "RecentView", bundle: Bundle.main)
            
        let rvc: RecentViewController = rvs.instantiateViewController(withIdentifier: "RecentViewController") as! RecentViewController
        
        return rvc
    }
    
    func getEvaluteView() -> EvaluteViewController
    {
        let evs: UIStoryboard = UIStoryboard.init(name: "EvaluteView", bundle: Bundle.main)
            
        let evc: EvaluteViewController = evs.instantiateViewController(withIdentifier: "EvaluteViewController") as! EvaluteViewController
        
        return evc
    }
    
    func getFavoriteView() -> FavoriteViewController
    {
        let fvs: UIStoryboard = UIStoryboard.init(name: "FavoriteView", bundle: Bundle.main)
            
        let fvc: FavoriteViewController = fvs.instantiateViewController(withIdentifier: "FavoriteViewController") as! FavoriteViewController
        
        return fvc
    }
    
    func getNotLoginView() -> NotLoginViewController
    {
        let nvs: UIStoryboard = UIStoryboard.init(name: "NotLoginView", bundle: Bundle.main)
            
        let nvc: NotLoginViewController = nvs.instantiateViewController(withIdentifier: "NotLoginViewController") as! NotLoginViewController
        
        return nvc
    }
    
    func getNotNetworkView() -> NotNetworkViewController
    {
        let nvs: UIStoryboard = UIStoryboard.init(name: "NotNetworkView", bundle: Bundle.main)
            
        let nvc: NotNetworkViewController = nvs.instantiateViewController(withIdentifier: "NotNetworkViewController") as! NotNetworkViewController
        
        return nvc
    }
    
    func getChnagePwdView() -> ChangePwdViewController
    {
        let cvs: UIStoryboard = UIStoryboard.init(name: "ChangePwdView", bundle: Bundle.main)
        
        let cvc: ChangePwdViewController = cvs.instantiateViewController(withIdentifier: "ChangePwdViewController") as! ChangePwdViewController
        
        return cvc
    }
    
    func getChosenView() -> ChosenViewController
    {
        let cvs: UIStoryboard = UIStoryboard.init(name: "ChosenView", bundle: Bundle.main)
        
        let cvc: ChosenViewController = cvs.instantiateViewController(withIdentifier: "ChosenViewController") as! ChosenViewController
        
        return cvc
    }
    
    func getMoodView() -> MoodViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "MoodView", bundle: Bundle.main)
        
        let mvc: MoodViewController = mvs.instantiateViewController(withIdentifier: "MoodViewController") as! MoodViewController
        
        return mvc
    }
    
    func getAnalyzeView() -> AnalyzeViewController
    {
        let avs: UIStoryboard = UIStoryboard.init(name: "AnalyzeView", bundle: Bundle.main)
        
        let avc: AnalyzeViewController = avs.instantiateViewController(withIdentifier: "AnalyzeViewController") as! AnalyzeViewController
        
        return avc
    }
    
    func getMoodResultView() -> MoodResultViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "MoodResultView", bundle: Bundle.main)
        
        let mvc: MoodResultViewController = mvs.instantiateViewController(withIdentifier: "MoodResultViewController") as! MoodResultViewController
        
        return mvc
    }
    
    func getDeviceView() -> DeviceViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "DeviceView", bundle: Bundle.main)
        
        let mvc: DeviceViewController = mvs.instantiateViewController(withIdentifier: "DeviceViewController") as! DeviceViewController
        
        return mvc
    }
    
    func getPairingView() -> PairingViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "PairingView", bundle: Bundle.main)
        
        let mvc: PairingViewController = mvs.instantiateViewController(withIdentifier: "PairingViewController") as! PairingViewController
        
        return mvc
    }
    
    func getNotFoundView() -> NotFoundViewController
    {
        let mvs: UIStoryboard = UIStoryboard.init(name: "NotFoundView", bundle: Bundle.main)
        
        let mvc: NotFoundViewController = mvs.instantiateViewController(withIdentifier: "NotFoundViewController") as! NotFoundViewController
        
        return mvc
    }
}
