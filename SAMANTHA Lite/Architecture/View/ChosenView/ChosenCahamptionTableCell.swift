//
//  ChosenCahamptionTableCell.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class ChosenCahamptionTableCell: UITableViewCell
{
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var collectCahamption: UICollectionView!
    
    weak var delegate: ChosenCahamptionTableCellDelegate?
    
    let cahamptionProtocol: CahamptionProtocol = CahamptionProtocol()
    
    var isCahamption:Bool?
    
    override func awakeFromNib()
    {
        setup()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupFontSize()
        
//        setupCollectionView()
    }
    
    func setupFontSize()
    {
        self.labName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
    }
    
    func setupCollectionView()
    {
        cahamptionProtocol.delegate = self
        
        cahamptionProtocol.isCahamption = isCahamption!
        
        self.collectCahamption.delegate = cahamptionProtocol
        
        self.collectCahamption.dataSource = cahamptionProtocol
        
        self.collectCahamption.register(UINib.init(nibName: "CollectCahamptionCell", bundle: nil), forCellWithReuseIdentifier: "CollectCahamptionCell")
        
        self.collectCahamption.allowsSelection = true;

        self.collectCahamption.allowsMultipleSelection = false;

        self.collectCahamption.reloadData()
    }
}

// MARK: - CahamptionProtocolDelegate

extension ChosenCahamptionTableCell: CahamptionProtocolDelegate
{
    func collectCahamptionCellClick(_ cell: CollectCahamptionCell!)
    {
        let indexPath = self.collectCahamption.indexPath(for: cell)
        
        self.delegate?.cahamptionTableCellClickWith(cell: self, indexPath: indexPath!)
    }
}

// MARK: - ChosenCahamptionTableCellDelegate

protocol ChosenCahamptionTableCellDelegate : class
{
    func cahamptionTableCellClickWith(cell: ChosenCahamptionTableCell, indexPath: IndexPath)
}

extension ChosenCahamptionTableCellDelegate
{
    func cahamptionTableCellClickWith(cell: ChosenCahamptionTableCell, indexPath: IndexPath)
    {
        print("cahamptionTableCellClickWith = " + String(indexPath.row))
    }
}
