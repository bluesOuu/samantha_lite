//
//  CelebrityProtocol.h
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectCelebrityCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CelebrityProtocolDelegate <CollectCelebrityCellDelegate>

@end

@interface CelebrityProtocol : NSObject<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, CollectCelebrityCellDelegate>

@property (weak, nonatomic) id<CelebrityProtocolDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
