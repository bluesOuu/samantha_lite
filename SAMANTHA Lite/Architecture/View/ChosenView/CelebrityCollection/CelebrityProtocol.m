//
//  CelebrityProtocol.m
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "CelebrityProtocol.h"

@implementation CelebrityProtocol
@synthesize delegate;
//@synthesize RecordArray;

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectCelebrityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectCelebrityCell" forIndexPath:indexPath];
    
    switch (indexPath.row)
    {
        case 0:
            cell.labOwner.text = @"蔡依林配方";
            break;
        case 1:
            cell.labOwner.text = @"蕭敬騰配方";
            break;
        case 2:
            cell.labOwner.text = @"五月天配方";
            break;
        case 3:
            cell.labOwner.text = @"鄧紫棋配方";
            break;
        default:
            break;
    }
    
    cell.beanImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"A%ld", indexPath.row + 1]];
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - CollectCelebrityCellDelegate

- (void)collectCelebrityCellClick:(CollectCelebrityCell *)cell
{
    if ([delegate respondsToSelector:@selector(collectCelebrityCellClick:)])
        [delegate collectCelebrityCellClick:cell];
}

@end
