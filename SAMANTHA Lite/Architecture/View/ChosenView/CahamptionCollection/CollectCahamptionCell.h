//
//  CollectCahamptionCell.h
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CollectCahamptionCellDelegate;

@interface CollectCahamptionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *bkView;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UIImageView *imageHead;

@property (weak, nonatomic) id<CollectCahamptionCellDelegate> delegate;

@end

@protocol CollectCahamptionCellDelegate <NSObject>

- (void)collectCahamptionCellClick:(CollectCahamptionCell *)cell;

@end
