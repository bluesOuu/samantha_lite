//
//  RecipeCollectionProtocol.m
//  HiroiaBleSample
//
//  Created by BluesOu on 2019/7/31.
//

#import "CahamptionProtocol.h"

@implementation CahamptionProtocol
@synthesize delegate;
@synthesize isCahamption;

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectCahamptionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectCahamptionCell" forIndexPath:indexPath];
    
    if (isCahamption)
    {
        switch (indexPath.row)
        {
            case 0:
                cell.labName.text = @"粕谷哲配方";
                break;
            case 1:
                cell.labName.text = @"王策配方";
                break;
            case 2:
                cell.labName.text = @"Daria Pinchuk配方";
                break;
            case 3:
                cell.labName.text = @"劉邦瑀配方";
                break;
            default:
                break;
        }
        
        cell.imageHead.image = [UIImage imageNamed:[NSString stringWithFormat:@"B%ld", indexPath.row + 1]];
    }
    else
    {
        switch (indexPath.row)
        {
            case 0:
                cell.labName.text = @"許志榮配方";
                break;
            case 1:
                cell.labName.text = @"松村眾三配方";
                break;
            case 2:
                cell.labName.text = @"胡元正配方";
                break;
            case 3:
                cell.labName.text = @"余侃儒配方";
                break;
            default:
                break;
        }
        
        cell.imageHead.image = [UIImage imageNamed:[NSString stringWithFormat:@"C%ld", indexPath.row + 1]];
    }
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = ((CGRectGetWidth(collectionView.frame) / 414) * 147);
    
    CGFloat height = ((width / 147) * 182);
    
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 21, 0, 21);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 12;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - UICollectionViewDelegate methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - CollectCahamptionCellDelegate

- (void)collectCahamptionCellClick:(CollectCahamptionCell *)cell
{
    if ([delegate respondsToSelector:@selector(collectCahamptionCellClick:)])
        [delegate collectCahamptionCellClick:cell];
}

@end
