//
//  RecipeCollectionProtocol.h
//  HiroiaBleSample
//
//  Created by BluesOu on 2019/7/31.
//

#import <Foundation/Foundation.h>
#import "CollectCahamptionCell.h"

@protocol CahamptionProtocolDelegate <CollectCahamptionCellDelegate>

@end

@interface CahamptionProtocol : NSObject<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CollectCahamptionCellDelegate>

@property (assign, nonatomic) BOOL isCahamption;

@property (weak, nonatomic) id<CahamptionProtocolDelegate> delegate;

@end

