//
//  CollectCahamptionCell.m
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "CollectCahamptionCell.h"

@implementation CollectCahamptionCell
@synthesize delegate;

- (void)awakeFromNib 
{
    [super awakeFromNib];
    
    [self setupTap];
}

- (void)drawRect:(CGRect)rect
{
    self.imageHead.layer.cornerRadius = 10;
    
    [Utilities setShadow:self.bkView.layer Opacity:0.38 Radius:8 Offset:CGSizeMake(6, 8) Color:[UIColor blackColor]];
}

- (void)setupTap
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    
    [self.bkView addGestureRecognizer:gesture];
}

#pragma mark - Tap Event

- (void)handleTap:(UITapGestureRecognizer *)gesture
{
    if ([delegate respondsToSelector:@selector(collectCahamptionCellClick:)])
        [delegate collectCahamptionCellClick:self];
}

@end
