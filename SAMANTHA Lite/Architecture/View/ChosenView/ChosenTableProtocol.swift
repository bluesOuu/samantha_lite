//
//  ChosenTableProtocol.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class ChosenTableProtocol: NSObject, UITableViewDelegate, UITableViewDataSource
{
    weak var delegate: ChosenTableProtocolDelegate?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height = ((tableView.frame.size.width / 414) * 258)
        
        return height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0)
        {
            let cell: ChosenCelebrityTableCell = tableView.dequeueReusableCell(withIdentifier: "ChosenCelebrityTableCell", for: indexPath) as! ChosenCelebrityTableCell
            
            cell.setupCollectionView()
            
            cell.labName.text = MultiLangManager.getLangString(forKey: STR_CELEBRITY)
            
            cell.delegate = self
            
            return cell
        }
        else
        {
            let cell: ChosenCahamptionTableCell = tableView.dequeueReusableCell(withIdentifier: "ChosenCahamptionTableCell", for: indexPath) as! ChosenCahamptionTableCell
            
            if (indexPath.row == 1)
            {
                cell.isCahamption = true
                
                cell.labName.text = MultiLangManager.getLangString(forKey: STR_CAHAMPTION)
            }
            else
            {
                cell.isCahamption = false
                
                cell.labName.text = MultiLangManager.getLangString(forKey: STR_TALENT)
            }
            
            cell.setupCollectionView()
            
            cell.delegate = self
            
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool
    {
        return false
    }
}

// MARK: ChosenCelebrityTableCellDelegate

extension ChosenTableProtocol: ChosenCelebrityTableCellDelegate
{
    func celebrityTableCellClickWith(indexPath: IndexPath)
    {
        self.delegate?.celebrityTableCellClickWith(indexPath: indexPath)
    }
}

// MARK: ChosenCahamptionTableCellDelegate

extension ChosenTableProtocol: ChosenCahamptionTableCellDelegate
{
    func cahamptionTableCellClickWith(cell: ChosenCahamptionTableCell, indexPath: IndexPath)
    {
        self.delegate?.cahamptionTableCellClickWith(cell: cell, indexPath: indexPath)
    }
}

// MARK: ChosenTableProtocolDelegate

protocol ChosenTableProtocolDelegate: ChosenCelebrityTableCellDelegate, ChosenCahamptionTableCellDelegate
{
    
}

