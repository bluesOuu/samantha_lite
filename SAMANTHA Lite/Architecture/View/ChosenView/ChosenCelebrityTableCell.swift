//
//  ChosenCelebrityTableCell.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class ChosenCelebrityTableCell: UITableViewCell
{
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var collectCelebrity: UICollectionView!
    
    weak var delegate: ChosenCelebrityTableCellDelegate?
    
    let celebrityProtocol: CelebrityProtocol = CelebrityProtocol()
    
    override func awakeFromNib()
    {
        setup()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupFontSize()
        
        setupCollectionView()
    }
    
    func setupFontSize()
    {
        self.labName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
    }
    
    func setupCollectionView()
    {
        let width = ((UIScreen.main.bounds.size.width / 414) * 260)
        
        let height = ((width / 260) * 210)

        let lineLayout: LineLayout = LineLayout(cellSize: CGSize.init(width: width, height: height))
        
        celebrityProtocol.delegate = self
        
        self.collectCelebrity.collectionViewLayout = lineLayout
        
        self.collectCelebrity.delegate = celebrityProtocol
        
        self.collectCelebrity.dataSource = celebrityProtocol
        
        self.collectCelebrity.register(UINib.init(nibName: "CollectCelebrityCell", bundle: nil), forCellWithReuseIdentifier: "CollectCelebrityCell")
        
        self.collectCelebrity.allowsSelection = true;

        self.collectCelebrity.allowsMultipleSelection = false;

        self.collectCelebrity.decelerationRate = UIScrollView.DecelerationRate(rawValue: 0.1);

        self.collectCelebrity.reloadData()
    }
}

// MARK: - CelebrityProtocolDelegate

extension ChosenCelebrityTableCell: CelebrityProtocolDelegate
{
    func collectCelebrityCellClick(_ cell: CollectCelebrityCell!)
    {
        let indexPath = self.collectCelebrity.indexPath(for: cell)
        
        self.delegate?.celebrityTableCellClickWith(indexPath: indexPath!)
    }
}

// MARK: - ChosenCelebrityTableCellDelegate

protocol ChosenCelebrityTableCellDelegate : class
{
    func celebrityTableCellClickWith(indexPath: IndexPath)
}

extension ChosenCelebrityTableCellDelegate
{
    func celebrityTableCellClickWith(indexPath: IndexPath)
    {
        print("celebrityTableCellClickWith = " + String(indexPath.row))
    }
}
