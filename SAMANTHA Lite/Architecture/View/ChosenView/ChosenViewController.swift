//
//  ChosenViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class ChosenViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var chosenTableProtocol: ChosenTableProtocol!

    let presenter = Presenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //TODO: Show Alert
        
        self.tableView.reloadData()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupNaviBar()
        
        setupTableView()
    }
    
    func setupInitValue()
    {
//        presenter.delegate = self
    }
    
    func setupNaviBar()
    {
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_CHOSEN)
    }
    
    func setupTableView()
    {
        self.chosenTableProtocol.delegate = self
    }
}

// MARK: - ChosenTableProtocolDelegate

extension ChosenViewController: ChosenTableProtocolDelegate
{
    func celebrityTableCellClickWith(indexPath: IndexPath)
    {
        print("table Celebrity Click = " + String(indexPath.row))
    }

    func cahamptionTableCellClickWith(cell: ChosenCahamptionTableCell, indexPath: IndexPath)
    {
        let index = self.tableView.indexPath(for: cell)
        
        print("table Cahamption Click = " + String(index!.row) + String(indexPath.row))
    }
}
