//
//  FavoriteTableProtocol.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class FavoriteTableProtocol: NSObject, UITableViewDelegate, UITableViewDataSource
{
    weak var delegate: FavoriteTableProtocolDelegate?
    
    weak var presenter: FavoritePresenter?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height = ((tableView.frame.size.width / 414) * 120)
        
        return height;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return presenter!.favoriteList.count;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: FavoriteTableCell = tableView.dequeueReusableCell(withIdentifier: "FavoriteTableCell", for: indexPath) as! FavoriteTableCell
        
        cell.delegate = self
        
        cell.favoriteImage.isHighlighted = ((indexPath.row % 2) != 0)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (self.delegate != nil)
        {
            self.delegate?.favoriteTableCellClick(cell: tableView.cellForRow(at: indexPath) as! FavoriteTableCell)
        }
        else
        {
            print("FavoriteTableProtocol Cell Click");
        }
    }
}

protocol FavoriteTableProtocolDelegate: FavoriteTableCellDelegate
{
    func favoriteTableCellClick(cell: FavoriteTableCell)
}

extension FavoriteTableCellDelegate
{
    func favoriteTableCellClick(cell: FavoriteTableCell)
    {
        print("FavoriteTableProtocol Cell Click");
    }
}

// MARK: - FavoriteTableCellDelegate

extension FavoriteTableProtocol: FavoriteTableCellDelegate
{
    func favoriteTableCellFavoriteClick(cell: FavoriteTableCell)
    {
        if (self.delegate != nil)
        {
            self.delegate?.favoriteTableCellFavoriteClick(cell: cell)
        }
        else
        {
            print("FavoriteTableProtocol Favorite Click");
        }
    }
}
