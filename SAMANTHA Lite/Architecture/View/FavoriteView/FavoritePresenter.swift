//
//  FavoritePresenter.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class FavoritePresenter: NSObject
{
    weak var delegate: FavoritePresenterDelegate?
    
    var favoriteList:Array<SamanthaRecipeStruc> = []
    
    override init()
    {
        super.init()
    }
    
    func getFavoriteList()
    {
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        favoriteList.append(SamanthaRecipeStruc())
        
        self.delegate?.onGetFavoriteListDone()
    }
}

//MARK: - FavoritePresenterDelegate

protocol FavoritePresenterDelegate: class
{
    func onGetFavoriteListDone()
}

extension FavoritePresenterDelegate
{
    func onGetFavoriteListDone()
    {
        print("onGetFavoriteListDone")
    }
}
