//
//  FavoriteTableCell.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class FavoriteTableCell: UITableViewCell
{
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    @IBOutlet weak var labOwner: UILabel!
    @IBOutlet weak var labRecipeName: UILabel!
    @IBOutlet weak var labTime: UILabel!
    
    @IBOutlet weak var moveImage: UIImageView!
    
    weak var delegate: FavoriteTableCellDelegate?
    
    override func awakeFromNib()
    {
        setup()
    }
    
    override func draw(_ rect: CGRect)
    {
        self.bkView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10
        
        self.recipeImage.layer.cornerRadius = 10
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool)
    {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {

    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupFontSize()
        
        setupTap()
    }
    
    func setupFontSize()
    {
        self.labOwner.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)

        self.labRecipeName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 2)), weight: UIFont.Weight.medium)

        self.labTime.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.light)
    }
    
    func setupTap()
    {
        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap(recognizer:)))
        
        self.favoriteImage.addGestureRecognizer(gesture)
    }
    
    // MARK: - Utilities
    
    @objc func handleTap(recognizer:UITapGestureRecognizer)
    {
        if (self.delegate != nil)
        {
            self.delegate?.favoriteTableCellFavoriteClick(cell: self)
        }
        else
        {
            print("FavoriteTableCell Favorite Click");
        }
        
    }
}

protocol FavoriteTableCellDelegate: class
{
    func favoriteTableCellFavoriteClick(cell: FavoriteTableCell)
}

extension FavoriteTableCellDelegate
{
    func favoriteTableCellFavoriteClick(cell: FavoriteTableCell)
    {
        print("FavoriteTableCell Favorite Click");
    }
}
