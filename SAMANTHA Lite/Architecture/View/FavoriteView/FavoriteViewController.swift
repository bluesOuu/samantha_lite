//
//  FavoriteViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class FavoriteViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var favoriteTableProtocol: FavoriteTableProtocol!

    let presenter = FavoritePresenter.init()
    
    var isEndTouch:Bool = false
    
    var snapshot:UIView?
    
    var sourceIndexPath:IndexPath?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //TODO: Show Alert
        
        if (getLoginStatus())
        {
            presenter.getFavoriteList()
        }
        else
        {
            showNotLoginView()
        }
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupNaviBar()
        
        setupTableView()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
    }
    
    func setupNaviBar()
    {
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)

        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.delegate = self
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 4)), weight: UIFont.Weight.semibold)
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_HOME_FAVORITE)
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
        
        self.naviBar.btnRightBar.isHidden = false
        
        self.naviBar.btnRightBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnRightBar.setImage(UIImage.init(named: "icon_search_white"), for: UIControl.State.normal)
    }
    
    func setupTableView()
    {
        let gesture:UILongPressGestureRecognizer =  UILongPressGestureRecognizer.init(target: self, action: #selector(longPressGestureRecognized(sender:)))
        
        gesture.minimumPressDuration = 0.5;

        self.tableView.addGestureRecognizer(gesture)
        
        self.favoriteTableProtocol.presenter = presenter
        
        self.favoriteTableProtocol.delegate = self
    }
    
    // MARK: - Utilities
    
    @objc func longPressGestureRecognized(sender: UILongPressGestureRecognizer)
    {
        let longPress:UILongPressGestureRecognizer = sender
        
        let state = longPress.state

        let location:CGPoint = longPress.location(in: self.tableView)
        
        let indexPath:IndexPath = self.tableView.indexPathForRow(at: location) ?? IndexPath.init(row: 999, section: 999)

        switch state
        {
            case UIGestureRecognizer.State.began:
                
                if (indexPath.section != 999)
                {
                    isEndTouch = false
                    
                    sourceIndexPath = indexPath
                    
                    let cell = self.tableView.cellForRow(at: indexPath)
                    
                    cell?.isHighlighted = false
                    
                    snapshot = customSnapshotFromView(inputView: cell!)
                    
                    var center:CGPoint = cell!.center
                    
                    snapshot!.center = center
                    
                    snapshot!.alpha = 0
                    
                    self.tableView.addSubview(snapshot!)
                    
                    UIView.animate(withDuration: 0.25)
                    {
                        center.y = location.y;
                        
                        self.snapshot!.center = center;
                        
                        self.snapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05);
                        
                        self.snapshot!.alpha = 0.98;
                        
                        cell!.alpha = 0.0;
                    }
                    completion:
                    {
                        (finish) in
                        
                        if (!self.isEndTouch)
                        {
                            cell!.isHidden = true
                        }
                    }
                }
                break
            case UIGestureRecognizer.State.changed:
                
                var center:CGPoint = self.snapshot!.center
                
                center.y = location.y
                
                self.snapshot!.center = center;
                
                if (indexPath.section != 999 && !indexPath.elementsEqual(sourceIndexPath!))
                {
//                    [[DataCenter sharedInstance].locationList exchangeObjectAtIndex: indexPath.row withObjectAtIndex:sourceIndexPath.row];

                    self.tableView.moveRow(at: sourceIndexPath!, to: indexPath)
                    
                    sourceIndexPath = indexPath;
                }
                
                break
            default:
                
                let cell:UITableViewCell = self.tableView.cellForRow(at: sourceIndexPath!)!
                
                cell.isHidden = false

                cell.alpha = 0.0
                
                isEndTouch = false
                
                UIView.animate(withDuration: 0.25)
                {
                    self.snapshot!.center = cell.center;

                    self.snapshot!.transform = CGAffineTransform.identity;

                    self.snapshot!.alpha = 0.0;

                    cell.alpha = 1.0;
                }
                completion:
                {
                    (finish) in
                    
                    self.sourceIndexPath = nil;

                    self.snapshot!.removeFromSuperview()

                    self.snapshot = nil;
                }
                
                break
        }
    }
    
    func customSnapshotFromView(inputView:UIView) -> UIView
    {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0);
        
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext();
        
        let tempSnapshot:UIView = UIImageView.init(image: image)
        
        tempSnapshot.layer.masksToBounds = false
        
        return tempSnapshot;
    }
    
    func showNotLoginView()
    {
        let nlvc:NotLoginViewController = Router.sharedInstance.getNotLoginView()

        self.addChild(nlvc)

        nlvc.delegate = self

        nlvc.view.frame = self.tableView.frame

        self.tableView.addSubview(nlvc.view)
    }
}

// MARK: - BaseNavigationBarDelegate

extension FavoriteViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func baseNavigationBarRightBtnClick()
    {
        
    }
}

// MARK: - FavoritePresenterDelegate

extension FavoriteViewController: FavoritePresenterDelegate
{
    func onGetFavoriteListDone()
    {
        //TODO: hide alert
        
        self.tableView.reloadData()
        
        if (presenter.favoriteList.count == 0)
        {
            
        }
    }
}

// MARK: - FavoriteTableProtocolDelegate

extension FavoriteViewController: FavoriteTableProtocolDelegate
{
    func favoriteTableCellClick(cell: FavoriteTableCell)
    {
        print("FavoriteView Cell Click");
    }

    func favoriteTableCellFavoriteClick(cell: FavoriteTableCell)
    {
        print("FavoriteView Favorite Click");
    }
}

// MARK: - NotLoginViewControllerDelegate

extension FavoriteViewController: NotLoginViewControllerDelegate
{
    func notLoginViewClick(nlvc: NotLoginViewController)
    {
        nlvc.view.removeFromSuperview()
        
        nlvc.removeFromParent()
        
        self.tableView.isHidden = false
        
        Router.sharedInstance.gotoLoginView()
    }
}
