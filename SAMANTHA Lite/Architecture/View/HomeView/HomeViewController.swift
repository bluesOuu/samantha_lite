//
//  HomeViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/19.
//

import Foundation
import Charts

class HomeViewController: BaseViewController
{
    // MARK: - IBOutlet
    
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var favoriteView: UIView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var labWelcome: UILabel!
    @IBOutlet weak var labFavorite: UILabel!
    @IBOutlet weak var labRecorde: UILabel!
    @IBOutlet weak var labDeviceTitle: UILabel!
    @IBOutlet weak var labDeviceContent: UILabel!
    @IBOutlet weak var labCollection: UILabel!
    @IBOutlet var collectRecordProtocol: CollectRecordProtocol!
    
    // MARK: - Variable
    
    // MARK: - Init
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
        
        setupCollectionView()
        
        setupNotification()
    }
    
    func setupInitValue()
    {
        
    }
    
    func setupFontSize()
    {
        self.labWelcome.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 2)), weight: UIFont.Weight.bold)

        self.labFavorite.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 8)), weight: UIFont.Weight.medium)

        self.labRecorde.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 8)), weight: UIFont.Weight.medium)
        
        self.labCollection.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.medium)
    }
    
    func setupLocalized()
    {
        self.labFavorite.text = MultiLangManager.getLangString(forKey: STR_HOME_FAVORITE)
        
        self.labRecorde.text = MultiLangManager.getLangString(forKey: STR_HOME_RECORD)
        
        self.labDeviceTitle.text = MultiLangManager.getLangString(forKey: STR_HOME_DEVICE_TITLE)
        
        self.labDeviceContent.text = ""
        
        self.labCollection.text = MultiLangManager.getLangString(forKey: STR_HOME_RECENT)
        
//        self.labCollection.text = MultiLangManager.getLangString(forKey: STR_HOME_HOT)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.naviBar.imageLogo.isHidden = false
        
        self.naviBar.btnRightBar.isHidden = false
        
        self.naviBar.btnRightBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnRightBar.setImage(UIImage.init(named: "icon_scan_white"), for: UIControl.State.normal)
    }
    
    func setupUILayer()
    {
        Utilities.setShadow(self.favoriteView.layer, opacity: 0.38, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
        
        Utilities.setShadow(self.recordView.layer, opacity: 0.38, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }
    
    func setupCollectionView()
    {
        let width = ((UIScreen.main.bounds.size.width / 414) * 370)
        
        let height = ((width / 370) * 274)

        let lineLayout = LineLayout(cellSize: CGSize.init(width: width, height: height))

        self.collectionView.collectionViewLayout = lineLayout!

        self.collectionView.register(UINib.init(nibName: "CollectRecordCell", bundle: nil), forCellWithReuseIdentifier: "CollectRecordCell")

        self.collectRecordProtocol.delegate = self;

        self.collectionView.allowsSelection = true;

        self.collectionView.allowsMultipleSelection = false;

        self.collectionView.decelerationRate = UIScrollView.DecelerationRate(rawValue: 0.1);

        self.collectionView.reloadData()
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(smtDisConnect), name: NSNotification.Name(rawValue: SMT_DIS_CONNECT), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(smtP5Callback), name: NSNotification.Name(rawValue: SMT_P5_CALLBACK), object: nil)
    }
    
    // MARK: - Notification
    
    @objc func smtDisConnect()
    {
        self.labDeviceContent.text = ""
    }
    
    @objc func smtP5Callback()
    {
        self.labDeviceContent.text = SamanthaManager.sharedInstance().smtGetConnectName()
    }
    
    // MARK: - Button Events
    
    @IBAction func favoriteClick(_ sender: UIButton)
    {
        let indexHomeNavi = Router.sharedInstance.getIndexNavigation()
        
        let fvc: FavoriteViewController = Router.sharedInstance.getFavoriteView()
        
        indexHomeNavi.pushViewController(fvc, animated: true)
    }
    
    @IBAction func recordClick(_ sender: UIButton)
    {
        let indexHomeNavi = Router.sharedInstance.getIndexNavigation()
        
        let rvc: RecentViewController = Router.sharedInstance.getRecentView()
        
        indexHomeNavi.pushViewController(rvc, animated: true)
    }
}

// MARK: - BaseNavigationBarDelegate

extension HomeViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarRightBtnClick()
    {
        AlertSupport.showImageSelector(self, isDevice: true)
        {
            (alertAction) in
            
            let asv:AlertScanQRCodeView = AlertScanQRCodeView.init(delegate: self)
            
            asv.show()
        }
        albumHander:
        {
            (alertAction) in
         
            Utilities_LoadMedia.load(fromAlbumType: _LOAD_MEDIA_TYPE.LOAD_MEDIA_TYPE_IMAGE, canEdit: true, delegate: self)
        }
    }
}

// MARK: - UIImagePickerControllerDelegate

extension HomeViewController: UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        NSLog("imagePicker Info = %@",info)
        
        let image:UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let barCodeInfo = BOBarCodeScanView.decode(from: image)
        
        NSLog("barCodeInfo = %@", barCodeInfo!)
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func  imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - AlertScanQRCodeViewDelegate

extension HomeViewController: AlertScanQRCodeViewDelegate
{
    @objc func alertScanQRCodeView(_ alertView: AlertScanQRCodeView!, barCodeInfo: String!)
    {
        NSLog("barCodeInfo = %@", barCodeInfo)
    }
}

// MARK: - CollectRecordProtocolDelegate

extension HomeViewController: CollectRecordProtocolDelegate
{
    @objc func collectRecordCellClick(_ cell: CollectRecordCell!)
    {
        print("Cell Click")
    }
}
