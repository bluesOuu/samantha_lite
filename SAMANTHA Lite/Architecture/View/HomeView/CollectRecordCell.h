//
//  CollectRecordCell.h
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CollectRecordCellDelegate;

@interface CollectRecordCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *bkView;
@property (weak, nonatomic) IBOutlet UIImageView *bkImage;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImage;
@property (weak, nonatomic) IBOutlet UIImageView *beanImage;
@property (weak, nonatomic) IBOutlet UILabel *labOwner;
@property (weak, nonatomic) IBOutlet UILabel *labRecipeName;
@property (weak, nonatomic) IBOutlet UILabel *labTime;

@property (weak, nonatomic) id<CollectRecordCellDelegate> delegate;

@end

@protocol CollectRecordCellDelegate <NSObject>

- (void)collectRecordCellClick:(CollectRecordCell *)cell;

@end
