//
//  CollectRecordProtocol.m
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "CollectRecordProtocol.h"

@implementation CollectRecordProtocol
@synthesize delegate;
//@synthesize RecordArray;

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectRecordCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectRecordCell" forIndexPath:indexPath];
    
    [self mappingRecordCell:cell InsexPath:indexPath];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - CollectRecordCellDelegate

- (void)collectRecordCellClick:(CollectRecordCell *)cell
{
    if ([delegate respondsToSelector:@selector(collectRecordCellClick:)])
        [delegate collectRecordCellClick:cell];
}

#pragma mark - Mapping Cell

- (void)mappingRecordCell:(CollectRecordCell *)cell InsexPath:(NSIndexPath *)indexPath
{
    cell.delegate = self;
}

@end
