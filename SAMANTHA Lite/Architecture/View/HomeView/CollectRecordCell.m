//
//  CollectRecordCell.m
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "CollectRecordCell.h"

@implementation CollectRecordCell
@synthesize delegate;

- (void)awakeFromNib 
{
    [super awakeFromNib];
    
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    self.bkView.layer.cornerRadius = 10;
    
    self.recipeImage.layer.cornerRadius = 10;
    
    self.bkImage.layer.cornerRadius = 10;
    
    self.beanImage.layer.cornerRadius = (CGRectGetHeight(self.beanImage.frame) / 2);
    
    [Utilities setShadow:self.bkView.layer Opacity:0.38 Radius:8 Offset:CGSizeMake(6, -10) Color:[UIColor blackColor]];
}

- (void)setup
{
    [self setupFont];
    
    [self setupTap];
}

- (void)setupFont
{
    [self.labOwner setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightMedium]];

    [self.labRecipeName setFont:[UIFont systemFontOfSize:([Utilities getFontSize] + 2) weight:UIFontWeightMedium]];
    
    [self.labTime setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 4) weight:UIFontWeightLight]];
}

- (void)setupTap
{
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    
    [self.bkView addGestureRecognizer:gesture];
}

#pragma mark - Tap Event

- (void)handleTap:(UITapGestureRecognizer *)gesture
{
    if ([delegate respondsToSelector:@selector(collectRecordCellClick:)])
        [delegate collectRecordCellClick:self];
}

@end
