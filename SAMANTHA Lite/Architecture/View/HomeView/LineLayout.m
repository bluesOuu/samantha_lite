//
//  LineLayout.m
//
//  Created by Tengfei on 15/12/27.
//  Copyright © 2015年 tengfei. All rights reserved.
//

#import "LineLayout.h"

//static const CGFloat ItemH = 274;
//static const CGFloat ItemW = 370;

@interface LineLayout()
{
    CGSize cellSize;
}

@end

@implementation LineLayout

- (instancetype)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

- (instancetype)initWithCellSize:(CGSize)size
{
    if (self = [super init])
    {
        cellSize = size;
    }
    
    return self;
}

- (void)prepareLayout
{
    [super prepareLayout];
    
    self.itemSize = CGSizeMake(cellSize.width, cellSize.height);
    
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.minimumLineSpacing = 10;
    
    self.minimumInteritemSpacing = 10;
    
    CGFloat inset = (((CGRectGetWidth(self.collectionView.frame) - cellSize.width - (self.minimumLineSpacing * 2)) / 2) + self.minimumLineSpacing);
    
    self.sectionInset = UIEdgeInsetsMake(0, inset, 0, inset);
}

- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset withScrollingVelocity:(CGPoint)velocity
{
    CGRect lastRect ;
    
    lastRect.origin = proposedContentOffset;
    
    lastRect.size = self.collectionView.frame.size;
    
    NSArray *array = [super layoutAttributesForElementsInRect:lastRect];
    
    CGFloat adjustOffsetX = MAXFLOAT;
    
    NSInteger index = 0;
    
    for (UICollectionViewLayoutAttributes *attrs in array)
    {
        if(ABS(attrs.frame.origin.x - proposedContentOffset.x) < ABS(adjustOffsetX))
        {
            adjustOffsetX = (attrs.frame.origin.x - proposedContentOffset.x);
            
            index = [array indexOfObject:attrs];
        }
    }
    
    CGFloat inset = (((CGRectGetWidth(self.collectionView.frame) - cellSize.width - (self.minimumLineSpacing * 2)) / 2) + self.minimumLineSpacing);
    
    UICollectionViewLayoutAttributes *attrs = array[index];
    
    return CGPointMake(attrs.frame.origin.x - inset, proposedContentOffset.y);
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    CGRect visiableRect;
    
    visiableRect.size = self.collectionView.frame.size;
    
    visiableRect.origin = self.collectionView.contentOffset;

    NSArray *array = [super layoutAttributesForElementsInRect:rect];

    CGFloat leftEdge = self.collectionView.contentOffset.x + 10;

    CGFloat adjustOffsetX = MAXFLOAT;
    
    NSInteger index = 0;
    
    for (UICollectionViewLayoutAttributes *attrs in array)
    {
        CGFloat itemMinX = CGRectGetMinX(attrs.frame);
        
        if(ABS(itemMinX - leftEdge) < ABS(adjustOffsetX))
        {
            adjustOffsetX = (itemMinX - leftEdge);
            
            index = [array indexOfObject:attrs];
        }
    }
        
    for (UICollectionViewLayoutAttributes *attrs in array)
    {
        NSInteger index1 = [array indexOfObject:attrs];
//
//        if (index1 == index)
//        {
//            attrs.alpha = 1;
//        }
//        else
//        {
//            attrs.alpha = 0.3;
//        }
    }

    return array;
}

@end









