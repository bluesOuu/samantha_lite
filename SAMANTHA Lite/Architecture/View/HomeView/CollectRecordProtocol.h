//
//  CollectRecordProtocol.h
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectRecordCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CollectRecordProtocolDelegate <CollectRecordCellDelegate>

@end

@interface CollectRecordProtocol : NSObject<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, CollectRecordCellDelegate>
{
    CollectRecordCell *tempCell;
}
@property (weak, nonatomic) id<CollectRecordProtocolDelegate> delegate;
//
//@property (weak, nonatomic) NSMutableArray<AnswerHistory *> *RecordArray;

@end

NS_ASSUME_NONNULL_END
