//
//  PairingViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class PairingViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var dsView: DeviceStatusView!
    @IBOutlet weak var scanView: DeviceScanView!
    @IBOutlet weak var doneView: DeviceDoneView!
    
    let presenter = Presenter.init()
    
    var cacheScanArray = NSMutableArray()
    
    var tempDevice:HiroiaDevice?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
        
        startScan()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        SamanthaManager.sharedInstance().autoConnFlag = true
        
        SamanthaManager.sharedInstance().sendCmdStartScan()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
    }
    
    func setupInitValue()
    {
        SamanthaManager.sharedInstance().sendCmdStopScan()
        
        SamanthaManager.sharedInstance().autoConnFlag = false
        
        SamanthaManager.sharedInstance().delegate = nil
        
        SamanthaManager.sharedInstance().smtDoDisConnectAllDevice()
        
        self.scanView.delegate = self
        
        self.doneView.delegate = self
    }
    
    func setupFontSize()
    {

    }
    
    func setupLocalized()
    {

    }
    
    func setupUILayer()
    {
        self.dsView.layer.cornerRadius = 10

        self.scanView.layer.cornerRadius = 10
        
        Utilities.setShadow(self.dsView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
        
        Utilities.setShadow(self.scanView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_BAR_DEVICE)
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
    }
    
    // MARK: Button Events
    
    // MARK: Utilities
    
    func showAlertNeedPwd(device:HiroiaDevice)
    {
        tempDevice = device
        
        let attrStr = Utilities.createAttrStr(with: MultiLangManager.getLangString(forKey: STR_PWD), font: UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.regular), color: UIColor.init(white: 1, alpha: 0.33))
        
        let alertView:AlertTextView = AlertTextView()

        alertView.labTitle.text = String(format: MultiLangManager.getLangString(forKey: STR_NEED_PWD), device.name)
        
        alertView.labMsg.text = MultiLangManager.getLangString(forKey: STR_PWD_RULE)
        
        alertView.text.delegate = self;
        
        alertView.text.attributedPlaceholder = attrStr
        
        alertView.text.isSecureTextEntry = true
        
        alertView.show(on: nil)
        {
            (button) in
            
            if (button == alertView.rightBtn)
            {
                if (Utilities.isNullOrNil(alertView.text.text))
                {
                    self.showAlertNeedPwd(device: self.tempDevice!)
                }
                else
                {
                    self.tempDevice!.password = alertView.text.text

                    Utilities.run
                    {
                        self.scanView.uiStatus = SCAN_VIEW_UI_STATUS.CONNECTING

                        SamanthaManager.sharedInstance().sendCmdConnect(with: self.tempDevice!)
                    }
                }
            }
            else
            {
                SamanthaManager.sharedInstance().sendCmdStopScan()
                
                self.startScan()
            }
            
            NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(textChange(notification:)), name: UITextField.textDidChangeNotification, object: nil)
    }

    @objc func textChange(notification: NSNotification)
    {
        let textField:UITextField = notification.object as! UITextField

        if (textField.text!.count > 4)
        {
            textField.text = String(textField.text!.prefix(4))
        }
    }
    
    func startScan()
    {
        Utilities.delayBlock(with: 0.3, queue: DispatchQueue.main)
        {
            self.startScanTimer()
            
            SamanthaManager.sharedInstance().delegate = self
            
            SamanthaManager.sharedInstance().sendCmdStartScan()
        }
        
        self.scanView.uiStatus = SCAN_VIEW_UI_STATUS.SCANING
        
        self.doneView.isHidden = true
        
        self.scanView.isHidden = false
    }
    
    // MARK: Timer

    func startScanTimer()
    {
        let timer = Timer.scheduledTimer(timeInterval: TimeInterval(SCAN_TIMEOUT), target: self, selector: #selector(scanTimeoutCallback), userInfo: nil, repeats: false)
        
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
    
    @objc func scanTimeoutCallback()
    {
        if (cacheScanArray.count == 0)
        {
            self.scanView.isHidden = true
        
            self.doneView.isHidden = false
        
            self.doneView.uiStatus = DONE_VIEW_UI_STATUS.NOTFOUND
        }
    }
    
    // MARK: Utilities
    
//    func getMachineRecipe()
//    {
//        HiroiaManager.sharedInstance()?.hiroiaDeviceDictionary
//
//        HiroiaManager.sharedInstance()?.sendSamanthaCmdP8GetNumber(ofRecipe: 1, with: <#T##HiroiaDevice!#>)
//    }
}

// MARK: - BaseNavigationBarDelegate

extension PairingViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PairingViewController: SamanthaManagerDelegate
{
    func onSendCmdStartScanBLENotReady()
    {
        
    }

    func onSendCmdStartScanResult(_ scanArray: [HiroiaDevice])
    {
        cacheScanArray.removeAllObjects()
        
        cacheScanArray.addObjects(from: scanArray)
        
        self.scanView.scanArray = cacheScanArray
        
        self.scanView.reloadData()
        
        self.scanView.uiStatus = SCAN_VIEW_UI_STATUS.SCANED
    }
    
    func onSendCmdConnect(withDeviceSuccess hiroiaDevice: HiroiaDevice)
    {
        NSLog("onSendCmdConnect withDeviceSuccess")
        
        BOUserInfo.setString(hiroiaDevice.mac, forKey: SAVE_SMT_MAC)
        
        BOUserInfo.setString(hiroiaDevice.password, forKey: SAVE_SMT_PWD)
        
        self.scanView.isHidden = true
        
        self.doneView.isHidden = false
        
        self.doneView.uiStatus = DONE_VIEW_UI_STATUS.SUCCESS
    }
    
    func onSendCmdConnect(withDeviceFailed hiroiaDevice: HiroiaDevice)
    {
        NSLog("onSendCmdConnect withDeviceFailed")
        
        tempDevice = hiroiaDevice
        
        self.scanView.isHidden = true
        
        self.doneView.isHidden = false
        
        self.doneView.uiStatus = DONE_VIEW_UI_STATUS.FAILED
    }
    
    func onSendCmdConnect(withDevicePwdError hiroiaDevice: HiroiaDevice)
    {
        NSLog("onSendCmdConnect withDevicePwdError")
        
        self.showAlertNeedPwd(device: hiroiaDevice)
    }
    
    func smtResponseDisConnected()
    {
        NSLog("smtResponseDisConnected")
        
        self.dsView.uiStatus = DS_VIEW_UI_STATUS.DISPLAY
        
        self.dsView.connStatus = DS_VIEW_CONN_STATUS.NOT_CONNECT
    }
    
    func smtResponseUpdateStatus(_ device: HiroiaDevice)
    {
        self.dsView.uiStatus = DS_VIEW_UI_STATUS.DISPLAY
        
        self.dsView.connStatus = DS_VIEW_CONN_STATUS.CONNTECTED
        
        self.dsView.deviceName = SamanthaManager.sharedInstance().smtGetConnectName() as NSString
    }
    
    func smtResponseBrewFailed(_ device: HiroiaDevice)
    {
        
    }
    
    func smtResponseLackWater(_ device: HiroiaDevice)
    {
        
    }
    
    func smtResponseHotWater(_ device: HiroiaDevice)
    {
        
    }
}

extension PairingViewController: DeviceScanViewDelegate
{
    func deviceScanViewCellClick(_ indexPath: IndexPath!)
    {
        let device:HiroiaDevice = cacheScanArray[indexPath.row] as! HiroiaDevice
        
        if (device.lock)
        {
            self.showAlertNeedPwd(device: device)
        }
        else
        {
            self.scanView.uiStatus = SCAN_VIEW_UI_STATUS.CONNECTING
        
            SamanthaManager.sharedInstance().sendCmdConnect(with: device)
        }
    }
}

extension PairingViewController: DeviceDoneViewDelegate
{
    func deviceDoneViewClick(_ status: DONE_VIEW_UI_STATUS)
    {
        if (status == DONE_VIEW_UI_STATUS.SUCCESS)
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if (status == DONE_VIEW_UI_STATUS.NOTFOUND)
        {
            self.startScan()
        }
        else
        {
            if (tempDevice != nil)
            {
                SamanthaManager.sharedInstance().sendCmdConnect(with: tempDevice!)
            }
        }
    }
    
    func deviceDoneViewNotFoundClick()
    {
        let nvc = Router.sharedInstance.getNotFoundView()
        
        self.present(nvc, animated: true, completion: nil)
    }
}

//extension PairingViewController: AlertTextViewDelegate
//{
//    func alert(_ alertView: AlertTextView!, textViewButtonClick button: UIButton!)
//    {
//        if (button == alertView.rightBtn)
//        {
//            if (Utilities.isNullOrNil(alertView.text.text))
//            {
//                self.showAlertNeedPwd(device: self.tempDevice!)
//            }
//            else
//            {
//                self.tempDevice!.password = alertView.text.text
//
//                Utilities.run
//                {
//                    self.scanView.uiStatus = SCAN_VIEW_UI_STATUS.CONNECTING
//
//                    SamanthaManager.sharedInstance().sendCmdConnect(with: self.tempDevice!)
//                }
//            }
//        }
//        else
//        {
//            SamanthaManager.sharedInstance().sendCmdStopScan()
//
//            self.startScan()
//        }
//
//        NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
//    }
//}

extension PairingViewController: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return Utilities.checkInputChar(string, withThisCase: "0123456789")
    }
}
