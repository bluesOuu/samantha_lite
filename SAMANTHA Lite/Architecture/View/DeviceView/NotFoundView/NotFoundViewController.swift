//
//  NotFoundViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class NotFoundViewController: BaseViewController
{
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var labHead: UILabel!
    @IBOutlet weak var labTitle1: UILabel!
    @IBOutlet weak var labTitle2: UILabel!
    @IBOutlet weak var labTitle3: UILabel!
    @IBOutlet weak var labMsg1: UILabel!
    @IBOutlet weak var labMsg2: UILabel!
    @IBOutlet weak var labMsg3: UILabel!
    @IBOutlet weak var labBtn: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }

    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()

        setupUILayer()
    }

    // MARK: - Setup

    func setup()
    {
        setupFontSize()

        setupLocalized()
    }

    func setupFontSize()
    {
        self.labHead.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.bold)
        
        self.labTitle1.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.regular)
        self.labTitle2.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.regular)
        self.labTitle3.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.regular)
        
        self.labMsg1.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.regular)
        self.labMsg2.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.regular)
        self.labMsg3.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.regular)
        
        self.labBtn.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.bold)
    }

    func setupLocalized()
    {
        self.labHead.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL)
        
        self.labTitle1.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL_TITLE1)
        self.labTitle2.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL_TITLE2)
        self.labTitle3.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL_TITLE3)
        
        self.labMsg1.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL_MSG1)
        self.labMsg2.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL_MSG2)
        self.labMsg3.text = MultiLangManager.getLangString(forKey: STR_SEARCH_FAIL_MSG3)
        
        self.labBtn.text = MultiLangManager.getLangString(forKey: STR_OK)
    }

    func setupUILayer()
    {
        self.bottomView.layer.cornerRadius = 10

        self.bkImage.layer.cornerRadius = 10
        
        self.doneBtn.layer.cornerRadius = 10
        
        Utilities.setShadow(self.bottomView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }

    // MARK: Button Events
    
    @IBAction func doneClick(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
