//
//  DeviceCell.h
//  samantha
//
//  Created by Lich Wang on 2017/10/9.
//  Copyright © 2017年 Hiroia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLab;

+ (CGFloat)getCellHeightFromTableViewWidth:(CGFloat)width;

@end
