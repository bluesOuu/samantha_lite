//
//  DeviceScanView.h
//  12Ours
//
//  Created by Blues on 2017/7/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SCAN_VIEW_UI_STATUS)
{
    SCAN_VIEW_UI_STATUS_SCANING,
    SCAN_VIEW_UI_STATUS_SCANED,
    SCAN_VIEW_UI_STATUS_CONNECTING,
};

@protocol DeviceScanViewDelegate;

@interface DeviceScanView : UIView

@property (assign, nonatomic) SCAN_VIEW_UI_STATUS uiStatus;

@property (weak, nonatomic) id<DeviceScanViewDelegate> delegate;

@property (weak, nonatomic) NSMutableArray<HiroiaDevice *> *scanArray;

- (void)reloadData;

@end

@protocol DeviceScanViewDelegate <NSObject>

- (void)deviceScanViewCellClick:(NSIndexPath *)indexPath;

@end
