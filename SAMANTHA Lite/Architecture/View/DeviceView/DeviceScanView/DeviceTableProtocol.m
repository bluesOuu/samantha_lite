//
//  DeviceTableProtocol.m
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "DeviceTableProtocol.h"

@implementation DeviceTableProtocol
@synthesize delegate;
@synthesize scanArray;

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return scanArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [DeviceCell getCellHeightFromTableViewWidth:CGRectGetWidth(tableView.frame)];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeviceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeviceCell" forIndexPath:indexPath];
    
    HiroiaDevice *device = scanArray[indexPath.row];
    
    cell.titleLab.text = device.name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([delegate respondsToSelector:@selector(deviceCellClick:)])
    {
        [delegate deviceCellClick:indexPath];
    }
}

@end
