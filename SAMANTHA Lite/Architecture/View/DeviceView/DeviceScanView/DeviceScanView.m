//
//  DeviceScanView.m
//  12Ours
//
//  Created by Blues on 2017/7/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "DeviceScanView.h"
#import "DeviceTableProtocol.h"

@interface DeviceScanView ()<DeviceTableProtocolDelegate>

@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *processImage;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet DeviceTableProtocol *deviceTableProtocol;
@end

@implementation DeviceScanView
@synthesize delegate;
@synthesize scanArray;

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (void)loadXIB
{
    if ([self.view isDescendantOfView:self])
        return;
    
    self.view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    
    [self addSubview:self.view];
    
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:@[
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0.0] ,
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeLeading
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeading
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeTrailing
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeTrailing
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0.0]
                           ]];
    
    [self setupUILayer];
    
    
    
    [self layoutIfNeeded];
}

#pragma mark - Public

- (void)setUiStatus:(SCAN_VIEW_UI_STATUS)uiStatus
{
//    if (_uiStatus == uiStatus)
//        return;

    _uiStatus = uiStatus;

    if (uiStatus == SCAN_VIEW_UI_STATUS_SCANING)
    {
        self.labTitle.text = [MultiLangManager getLangStringForKey:STR_SEARCHING];
        
        self.imageView.hidden = NO;
        
        [self.imageView startAnimating];
        
        self.tableView.hidden = YES;
        
        self.processImage.hidden = YES;
        
        [self.processImage stopAnimating];
        
        self.labName.hidden = YES;
    }
    else
    {
        if (uiStatus == SCAN_VIEW_UI_STATUS_SCANED)
        {
            self.labTitle.text = [MultiLangManager getLangStringForKey:STR_SEARCHED];
            
            self.labName.hidden = YES;
            
            self.tableView.hidden = NO;
            
            self.processImage.hidden = YES;
            
            [self.processImage stopAnimating];
        }
        else
        {
            self.labTitle.text = [MultiLangManager getLangStringForKey:STR_CONNECTING];
            
            self.labName.hidden = NO;
            
            self.tableView.hidden = YES;
            
            self.processImage.hidden = NO;
            
            [self.processImage startAnimating];
        }
        
        self.imageView.hidden = YES;
        
        [self.imageView stopAnimating];
    }
}


#pragma mark - Setup

- (void)setup
{
    _uiStatus = SCAN_VIEW_UI_STATUS_SCANING;
    
    [self prepareImage];
    
    [self prepareImageProgress];
    
    [self setupFont];
    
    [self setupLocalized];
    
    [self setupTableView];
}

- (void)setupFont
{
    [self.labName setFont:[UIFont systemFontOfSize:([Utilities getFontSize] + 3) weight:UIFontWeightBold]];

    [self.labTitle setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightLight]];
}

- (void)setupLocalized
{
    
}

- (void)setupUILayer
{
    self.view.layer.cornerRadius = 10;
}

- (void)prepareImage
{
    NSMutableArray *imageArray = [NSMutableArray new];
    
    for (int i = 1 ; i < 27 ; i++)
    {
        [imageArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"searching_%d", i]]];
    }
    
    self.imageView.image = nil;
    
    self.imageView.animationImages = imageArray;
    
    self.imageView.animationRepeatCount = 0;
    
    self.imageView.animationDuration = 1.5;
}

- (void)prepareImageProgress
{
    NSMutableArray *imageArray = [NSMutableArray new];
    
    for (int i = 1 ; i < 10 ; i++)
    {
        [imageArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"linking2%02d", i]]];
    }
    
    self.processImage.image = nil;
    
    self.processImage.animationImages = imageArray;
    
    self.processImage.animationRepeatCount = 0;
    
    self.processImage.animationDuration = 2;
    
    [self.processImage startAnimating];
}

- (void)setupTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"DeviceCell" bundle:nil] forCellReuseIdentifier:@"DeviceCell"];
    
    self.deviceTableProtocol.delegate = self;

    self.deviceTableProtocol.scanArray = scanArray;

    [self.tableView reloadData];
}

- (void)reloadData
{
    self.deviceTableProtocol.scanArray = scanArray;
    
    [self.tableView reloadData];
}

#pragma mark - DeviceTableProtocolDelegate

- (void)deviceCellClick:(NSIndexPath *)indexPath
{
    if ([delegate respondsToSelector:@selector(deviceScanViewCellClick:)])
    {
        [delegate deviceScanViewCellClick:indexPath];
        
        HiroiaDevice *device = scanArray[indexPath.row];
        
        self.labName.text = device.name;
    }
}

@end
