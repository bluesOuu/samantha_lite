//
//  DeviceTableProtocol.h
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeviceCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol DeviceTableProtocolDelegate <NSObject>

- (void)deviceCellClick:(NSIndexPath *)indexPath;

@end

@interface DeviceTableProtocol : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) id<DeviceTableProtocolDelegate> delegate;

@property (weak, nonatomic) NSMutableArray<HiroiaDevice *> *scanArray;

@end

NS_ASSUME_NONNULL_END
