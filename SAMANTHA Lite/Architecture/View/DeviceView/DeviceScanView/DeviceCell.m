//
//  DeviceCell.m
//  samantha
//
//  Created by Blues on 2020/2/27.
//  Copyright © 2020 Hiroia. All rights reserved.
//

#import "DeviceCell.h"

@implementation DeviceCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setup];
}

- (void)setup
{
    [self setupFont];
}

- (void)setupFont
{
    [self.titleLab setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightRegular]];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted)
    {
        self.titleLab.textColor = [UIColor colorWithHexString:COLOR_ED5FFF];
    }
    else
    {
        self.titleLab.textColor = [UIColor colorWithWhite:1 alpha:0.6];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (selected)
    {
        self.titleLab.textColor = [UIColor colorWithHexString:COLOR_ED5FFF];
    }
    else
    {
        self.titleLab.textColor = [UIColor colorWithWhite:1 alpha:0.6];
    }
}

#pragma mark - Public

+ (CGFloat)getCellHeightFromTableViewWidth:(CGFloat)width
{
    return ((width / 414) * 40);
}

@end
