//
//  DeviceDoneView.m
//  12Ours
//
//  Created by Blues on 2017/7/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "DeviceDoneView.h"

@interface DeviceDoneView ()

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UIView *notFooundView;
@property (weak, nonatomic) IBOutlet UILabel *labBtnDone;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UILabel *labNotFound;
@end

@implementation DeviceDoneView
@synthesize delegate;

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (void)loadXIB
{
    if ([self.view isDescendantOfView:self])
        return;
    
    self.view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    
    [self addSubview:self.view];
    
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:@[
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0.0] ,
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeLeading
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeading
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeTrailing
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeTrailing
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0.0]
                           ]];
    
    [self setupUILayer];
    
    [self layoutIfNeeded];
}

#pragma mark - Public

- (void)setUiStatus:(DONE_VIEW_UI_STATUS)uiStatus
{
    _uiStatus = uiStatus;
    
    if (uiStatus == DONE_VIEW_UI_STATUS_SUCCESS)
    {
        self.labTitle.text = [MultiLangManager getLangStringForKey:STR_CONNECT_SUCCESS];
        
        self.labBtnDone.text = [MultiLangManager getLangStringForKey:STR_DONE];
        
        self.notFooundView.hidden = YES;
    }
    else if (uiStatus == DONE_VIEW_UI_STATUS_NOTFOUND)
    {
        self.labTitle.text = [MultiLangManager getLangStringForKey:STR_SEARCH_NO_DEVICE];
        
        self.labBtnDone.text = [MultiLangManager getLangStringForKey:STR_RE_SEARCH];
        
        self.notFooundView.hidden = NO;
    }
    else
    {
        self.labTitle.text = [MultiLangManager getLangStringForKey:STR_CONNECT_FAIL];
        
        self.labBtnDone.text = [MultiLangManager getLangStringForKey:STR_ALERT_RETRY];
        
        self.notFooundView.hidden = YES;
    }
}

#pragma mark - Setup

- (void)setup
{
    [self setupFont];
    
    [self setupLocalized];
}

- (void)setupFont
{
    [self.labBtnDone setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 1) weight:UIFontWeightBold]];

    [self.labTitle setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightLight]];
}

- (void)setupLocalized
{
    self.labTitle.text = [MultiLangManager getLangStringForKey:STR_CONNECT_SUCCESS];
    
    self.labNotFound.text = [MultiLangManager getLangStringForKey:STR_SEARCH_FAIL];
}

- (void)setupUILayer
{
    self.view.layer.cornerRadius = 10;
    
    self.doneBtn.layer.cornerRadius = 10;
}

- (IBAction)doneClick:(id)sender
{
    if ([delegate respondsToSelector:@selector(deviceDoneViewClick:)])
        [delegate deviceDoneViewClick:self.uiStatus];
}

- (IBAction)notFoundClick:(id)sender
{
    if ([delegate respondsToSelector:@selector(deviceDoneViewNotFoundClick)])
        [delegate deviceDoneViewNotFoundClick];
}

@end
