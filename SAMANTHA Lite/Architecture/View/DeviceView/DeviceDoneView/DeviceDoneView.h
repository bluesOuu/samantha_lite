//
//  DeviceDoneView.h
//  12Ours
//
//  Created by Blues on 2017/7/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DONE_VIEW_UI_STATUS)
{
    DONE_VIEW_UI_STATUS_SUCCESS,
    DONE_VIEW_UI_STATUS_FAILED,
    DONE_VIEW_UI_STATUS_NOTFOUND,
};

@protocol DeviceDoneViewDelegate;

@interface DeviceDoneView : UIView

@property (assign, nonatomic) DONE_VIEW_UI_STATUS uiStatus;

@property (weak, nonatomic) id<DeviceDoneViewDelegate> delegate;

@end

@protocol DeviceDoneViewDelegate <NSObject>

- (void)deviceDoneViewClick:(DONE_VIEW_UI_STATUS)status;

- (void)deviceDoneViewNotFoundClick;

@end
