//
//  DeviceStatusView.h
//  12Ours
//
//  Created by Blues on 2017/7/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DS_VIEW_CONN_STATUS)
{
    DS_VIEW_CONN_STATUS_NOT_CONNECT,
    DS_VIEW_CONN_STATUS_CONNTECTED,
    DS_VIEW_CONN_STATUS_FAILED,
    DS_VIEW_CONN_STATUS_NOT_FOUND,
};

typedef NS_ENUM(NSInteger, DS_VIEW_UI_STATUS)
{
    DS_VIEW_UI_STATUS_DISPLAY,
    DS_VIEW_UI_STATUS_PAIRING,
};

@interface DeviceStatusView : UIView

@property (assign, nonatomic) DS_VIEW_CONN_STATUS connStatus;

@property (assign, nonatomic) DS_VIEW_UI_STATUS uiStatus;

@property (weak, nonatomic) NSString *deviceName;

@end
