//
//  DeviceStatusView.m
//  12Ours
//
//  Created by Blues on 2017/7/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "DeviceStatusView.h"

@interface DeviceStatusView ()

@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIView *bollView;
@property (weak, nonatomic) IBOutlet UILabel *labConnect;
@property (weak, nonatomic) IBOutlet UIImageView *deviceImage;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *labDeviceTitle;
@property (weak, nonatomic) IBOutlet UILabel *labDevice;

@end

@implementation DeviceStatusView

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (void)loadXIB
{
    if ([self.view isDescendantOfView:self])
        return;
    
    self.view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    
    [self addSubview:self.view];
    
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:@[
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0.0] ,
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeLeading
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeading
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeTrailing
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeTrailing
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0.0]
                           ]];
    
    [self setupUILayer];
    
    [self layoutIfNeeded];
}

#pragma mark - Public

- (void)setDeviceName:(NSString *)deviceName
{
    _deviceName = deviceName;
    
    self.labDevice.text = _deviceName;
}

- (void)setUiStatus:(DS_VIEW_UI_STATUS)uiStatus
{
    if (_uiStatus == uiStatus)
        return;

    _uiStatus = uiStatus;
    
    if (uiStatus == DS_VIEW_UI_STATUS_DISPLAY)
    {
        self.bottomView.hidden = YES;
    }
    else
    {
        self.bottomView.hidden = NO;
    }
}

- (void)setConnStatus:(DS_VIEW_CONN_STATUS)connStatus
{
    if (_connStatus == connStatus)
        return;

    _connStatus = connStatus;
    
    switch (connStatus)
    {
        case DS_VIEW_CONN_STATUS_NOT_CONNECT:
            self.bollView.backgroundColor = [UIColor whiteColor];
            self.labConnect.text = [MultiLangManager getLangStringForKey:STR_NOT_CONNECTED];
            self.deviceImage.image = [UIImage imageNamed:@"device_white"];
            break;
        case DS_VIEW_CONN_STATUS_CONNTECTED:
            self.bollView.backgroundColor = [UIColor colorWithHexString:COLOR_CONNECT];
            self.labConnect.text = [MultiLangManager getLangStringForKey:STR_CONNECTED];
            self.deviceImage.image = [UIImage imageNamed:@"device_pink"];
            break;
        case DS_VIEW_CONN_STATUS_FAILED:
            self.bollView.backgroundColor = [UIColor whiteColor];
            self.labConnect.text = [MultiLangManager getLangStringForKey:STR_NOT_CONNECTED];
            self.deviceImage.image = [UIImage imageNamed:@"device_red"];
            break;
        case DS_VIEW_CONN_STATUS_NOT_FOUND:
            self.bollView.backgroundColor = [UIColor whiteColor];
            self.labConnect.text = [MultiLangManager getLangStringForKey:STR_NOT_CONNECTED];
            self.deviceImage.image = [UIImage imageNamed:@"device_blue"];
            break;
        default:
            break;
    }
}

#pragma mark - Setup

- (void)setup
{
    _uiStatus = DS_VIEW_UI_STATUS_DISPLAY;
    
    _connStatus = DS_VIEW_CONN_STATUS_NOT_CONNECT;
    
    [self setupFont];
    
    [self setupLocalized];
}

- (void)setupFont
{
    [self.labConnect setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 4) weight:UIFontWeightMedium]];
    
    [self.labDeviceTitle setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 4) weight:UIFontWeightMedium]];
    
    [self.labDevice setFont:[UIFont systemFontOfSize:([Utilities getFontSize] + 2) weight:UIFontWeightThin]];
}

- (void)setupLocalized
{
    self.labDeviceTitle.text = [MultiLangManager getLangStringForKey:STR_HOME_DEVICE_TITLE];
    
    self.labDevice.text = [MultiLangManager getLangStringForKey:STR_NO_CONNECT_DEVICE];
    
    self.labConnect.text = [MultiLangManager getLangStringForKey:STR_NOT_CONNECTED];
}

- (void)setupUILayer
{
    self.view.layer.cornerRadius = 10;
    
    self.bollView.layer.cornerRadius = (CGRectGetWidth(self.bollView.frame) / 2);
}

@end
