//
//  DeviceViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class DeviceViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var dsView: DeviceStatusView!
    @IBOutlet weak var connectBtn: UIButton!
    @IBOutlet weak var labBtnConnect: UILabel!
    
    @IBOutlet weak var recipeViewA: UIView!
    @IBOutlet weak var recipeViewB: UIView!
    @IBOutlet weak var recipeViewC: UIView!
    @IBOutlet weak var recipeViewD: UIView!
    
    @IBOutlet weak var labRecipeNameA: UILabel!
    @IBOutlet weak var labRecipeNameB: UILabel!
    @IBOutlet weak var labRecipeNameC: UILabel!
    @IBOutlet weak var labRecipeNameD: UILabel!
    
    @IBOutlet weak var labRecipeUserNameA: UILabel!
    @IBOutlet weak var labRecipeUserNameB: UILabel!
    @IBOutlet weak var labRecipeUserNameC: UILabel!
    @IBOutlet weak var labRecipeUserNameD: UILabel!
    
    
    let presenter = Presenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
        
        setupNotification()
    }
    
    func setupInitValue()
    {
        smtDisConnect()
    }
    
    func setupFontSize()
    {
        self.labBtnConnect.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
    }
    
    func setupLocalized()
    {
        self.labBtnConnect.text = MultiLangManager.getLangString(forKey: STR_CONN_COFFEE)
    }
    
    func setupUILayer()
    {
        self.dsView.layer.cornerRadius = 10

        Utilities.setShadow(self.dsView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
        
        Utilities.setShadow(self.recipeViewA.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
        Utilities.setShadow(self.recipeViewB.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
        Utilities.setShadow(self.recipeViewC.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
        Utilities.setShadow(self.recipeViewD.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_BAR_DEVICE)
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(smtDisConnect), name: NSNotification.Name(rawValue: SMT_DIS_CONNECT), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(smtP5Callback), name: NSNotification.Name(rawValue: SMT_P5_CALLBACK), object: nil)
    }
    
    // MARK: - Notification
    
    @objc func smtDisConnect()
    {
        self.labBtnConnect.text = MultiLangManager.getLangString(forKey: STR_CONN_COFFEE)
        
        self.dsView.uiStatus = DS_VIEW_UI_STATUS.DISPLAY
        
        self.dsView.connStatus = DS_VIEW_CONN_STATUS.NOT_CONNECT
    }
    
    @objc func smtP5Callback()
    {
        self.labBtnConnect.text = SamanthaManager.sharedInstance().smtGetConnectName()
        
        self.dsView.uiStatus = DS_VIEW_UI_STATUS.DISPLAY
        
        self.dsView.connStatus = DS_VIEW_CONN_STATUS.CONNTECTED
    }
    
    // MARK: Button Events
    
    @IBAction func settingClick(_ sender: UIButton)
    {
        let indexHomeNavi = Router.sharedInstance.getIndexNavigation()
        
        let rvc: PairingViewController = Router.sharedInstance.getPairingView()
        
        indexHomeNavi.pushViewController(rvc, animated: true)
    }
    
    // MARK: Utilities
    
    
}
