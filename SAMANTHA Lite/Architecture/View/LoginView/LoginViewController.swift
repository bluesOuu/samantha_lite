//
//  LoginViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

import UIKit

class LoginViewController: BaseViewController, AlertProtocol
{
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var labWelcome: UILabel!
    @IBOutlet weak var labInfo: UILabel!
    @IBOutlet weak var accView: UIView!
    @IBOutlet weak var textAcc: UITextField!
    @IBOutlet weak var pwdView: UIView!
    @IBOutlet weak var textPwd: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var regBtn: UIButton!
    @IBOutlet weak var appleBtn: UIButton!
    @IBOutlet weak var forgetBtn: UIButton!
    @IBOutlet weak var fbCenter: NSLayoutConstraint!
    @IBOutlet weak var fbRight: NSLayoutConstraint!
    @IBOutlet weak var labForget: UILabel!
    @IBOutlet weak var viewUp: NSLayoutConstraint!
    @IBOutlet weak var viewDown: NSLayoutConstraint!
    @IBOutlet weak var labLoginWith: UILabel!
    @IBOutlet weak var labBtnLogin: UILabel!
    @IBOutlet weak var labBtnReg: UILabel!
    
    let presenter = Presenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.textAcc.text = "blues@hiroia.com"
        
        self.textPwd.text = "1234"
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupNaviBar()
        
        setupFontSize()
        
        setupLocalized()
        
        setupTextField()
        
        setupAppleBtn()
        
        setupNotification()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
    }
    
    func setupAppleBtn()
    {
        if #available(iOS 13, *)
        {
            self.fbCenter.priority = UILayoutPriority.defaultLow
            
            self.fbRight.priority = UILayoutPriority.defaultHigh
        }
        else
        {
            self.fbCenter.priority = UILayoutPriority.defaultHigh
            
            self.fbRight.priority = UILayoutPriority.defaultLow
            
            self.appleBtn.isHidden = true
        }
        
        self.view.layoutIfNeeded()
    }
    
    func setupFontSize()
    {
        self.labWelcome.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 10)), weight: UIFont.Weight.bold)

        self.labInfo.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        
        self.textAcc.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        self.textPwd.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        self.labForget.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        
        self.forgetBtn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        
        self.labLoginWith.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.medium)
        
        self.labBtnLogin.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        self.labBtnReg.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
    }
    
    func setupLocalized()
    {
        self.labWelcome.text = MultiLangManager.getLangString(forKey: STR_LOGIN_INFO_1)
        
        self.labInfo.text = MultiLangManager.getLangString(forKey: STR_LOGIN_INFO_2)
        
        self.labBtnReg.text = MultiLangManager.getLangString(forKey: STR_REGISTER)

        self.labBtnLogin.text = MultiLangManager.getLangString(forKey: STR_LOGIN)
        
        self.labLoginWith.text = MultiLangManager.getLangString(forKey: STR_LOGIN_WITH)
    }
    
    func setupUILayer()
    {
        self.loginView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10
        
        self.accView.layer.cornerRadius = 10
        
        self.loginBtn.layer.cornerRadius = 10
        
        self.regBtn.layer.cornerRadius = 10
        
        self.accView.layer.borderWidth = 1
        
        self.accView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        self.pwdView.layer.cornerRadius = 10
        
        self.pwdView.layer.borderWidth = 1
        
        self.pwdView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        Utilities.setShadow(self.loginView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
    }
    
    func setupTextField()
    {
        var placeholder = MultiLangManager.getLangString(forKey: STR_HINT_ACCOUNT)
        
        let font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        var attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])
        
        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))
        
        self.textAcc.attributedPlaceholder = attrStr
        
        placeholder = MultiLangManager.getLangString(forKey: STR_HINT_PASSWORD)
        
        attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])
        
        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))
        
        self.textPwd.attributedPlaceholder = attrStr
        
        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(gesture)
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Button Events
    
    @IBAction func regClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        let rvc:RegViewController = Router.sharedInstance.getRegView()
        
        self.navigationController?.pushViewController(rvc, animated: true)
    }
    
    @IBAction func loginClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        AlertProcessView.show()
        
        presenter.sendApiLoginWith(acc: self.textAcc.text!, pwd: self.textPwd.text!, longitude: 0, latitude: 0)
    }
    
    @IBAction func forgetClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        let fvc:ForgetViewController = Router.sharedInstance.getForgetView()
        
        self.navigationController?.pushViewController(fvc, animated: true)
    }
    
    @IBAction func fbClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
    }
    
    @IBAction func appleClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
//        let socailMediaManager = SocailMediaManager.sharedInstance()
//
//        socailMediaManager?.login(_SOCAIL_TYPE.SOCAIL_TYPE_APPLE, response:
//        {
//            (socailProfile) in
//
//            socailProfile?.printSelf()
//
//            let manager:HiroiaCloudManager = HiroiaCloudManager.sharedInstance() as! HiroiaCloudManager;
//
//            manager.sendCmdLogin(withAppleID: socailProfile?.userId, token: socailProfile?.accessToken, longitude: 0, latitude: 0, isSAMANTHA: true)
//            {
//                (hiroiaResultStruc: HiroiaResultStruc?) in
//
//                hiroiaResultStruc?.printSelf()
//            }
//        })
    }
    
    // MARK: - Utilities
    
    @objc func handleTap(recognizer:Any)
    {
        self.textAcc.resignFirstResponder()
        
        self.textPwd.resignFirstResponder()
    }
    
    func parserErrCode(errCode: Int)
    {
        print("parserErrCode = " + String(errCode))
        
        switch errCode
        {
            case 4010400, 4040101, 4040102:
                showAlertUserOrPwdError { (string) in }
                break
            case 4030100:
                showAlertUserNotActivated
                {
                    (string) in
                    
                    if (string.elementsEqual(MultiLangManager.getLangString(forKey: STR_RE_SEND)))
                    {
                        self.presenter.sendApiReSendActiviteEmail(acc: self.textAcc.text!)
                    }
                }

                break
            default:
                showAlertNetworkError(errCode: errCode) { (string) in }
                break
        }
    }
    
    // MARK: - Notification
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        
        self.viewUp.priority = UILayoutPriority.defaultHigh

        self.viewDown.priority = UILayoutPriority.defaultLow
        
        UIView.animate(withDuration: duration)
        {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double

        self.viewUp.priority = UILayoutPriority.defaultLow

        self.viewDown.priority = UILayoutPriority.defaultHigh
        
        UIView.animate(withDuration: duration)
        {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - UITextFieldDelegate

extension LoginViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == self.textAcc)
        {
            self.textPwd.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//        if (textField == self.textPwd)
//        {
//            return Utilities.checkInputChar(string, withThisCase: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@")
//        }
//        else
//        {
//            return Utilities.checkInputChar(string, withThisCase: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@")
//        }
//    }
}

// MARK: - LoginPresenterDelegate

extension LoginViewController: PresenterDelegate
{
    func onSendApiLoginWith(errCode:Int)
    {
        AlertProcessView.hide()
        
        if (errCode != 0)
        {
            parserErrCode(errCode: errCode)
        }
        else
        {
            Router.sharedInstance.gotoIndexHomeView()
        }
    }
    
    func onSendApiReSendActiviteEmail(errCode: Int)
    {
        showAlertActiviteSendDone { (string) in }
    }
}

// MARK: - BaseNavigationBarDelegate

extension LoginViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        Router.sharedInstance.gotoIndexHomeView()
    }
}
