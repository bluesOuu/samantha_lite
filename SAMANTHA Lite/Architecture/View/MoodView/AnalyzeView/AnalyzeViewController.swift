//
//  AnalyzeViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class AnalyzeViewController: BaseViewController, EntityProtocol
{
    // MARK: - IBOutlet
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var labPrgress: UILabel!
    @IBOutlet weak var labTitle: UILabel!
    @IBOutlet weak var progressView: CircleProgressView!
    
    let presenter = Presenter.init()
    
    var timer:Timer?
    
    var isAnalyzeDone:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
        
        startDetectFace()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
        
        self.progressView.progress = 0;
        
        self.labPrgress.text = "0%"
    }
    
    func setupFontSize()
    {
        self.labTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.labPrgress.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 19)), weight: UIFont.Weight.light)
    }
    
    func setupLocalized()
    {
        
    }
    
    func setupUILayer()
    {
        self.bkView.layer.cornerRadius = 10
        
        self.headImage.layer.cornerRadius = 10
        
        self.headImage.layer.borderWidth = 2.5
        
        self.headImage.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_BAR_MOOD)
    }
    
    // MARK: Timer
    
    func startTimer()
    {
        stopTimer()
        
        timer = Timer.init(timeInterval: 0.015, target: self, selector: #selector(callback), userInfo: nil, repeats: true)
        
        RunLoop.current.add(timer!, forMode: RunLoop.Mode.common)
    }
    
    func stopTimer()
    {
        if ((timer) != nil)
        {
            timer!.invalidate()
            
            timer = nil
        }
    }
    
    @objc func callback()
    {
        if (self.progressView.progress >= 0.99)
        {
            self.progressView.roundedCap = false
            
            self.progressView.progress = 1
            
            stopTimer()
            
            Utilities.delayBlock(with: 0.5, queue: DispatchQueue.main)
            {
                self.gotoResultview()
            }
        }
        else if (self.progressView.progress > 0.8)
        {
            if (isAnalyzeDone)
            {
                self.progressView.roundedCap = true
                
                self.progressView.progress += 0.01
            }
            else if (self.progressView.progress >= 0.98)
            {
                self.progressView.roundedCap = false
            }
            else
            {
                self.progressView.roundedCap = true
                
                self.progressView.progress += 0.005
            }
        }
        else
        {
            self.progressView.roundedCap = true
            
            self.progressView.progress += 0.01
        }
        
        self.labPrgress.text = String(format: "%.0f%%", (self.progressView.progress * 100))
    }
    
    // MARK: Utilities
    
    func startDetectFace()
    {
        startTimer()
        
        let imageData:Data = getFaceStruc().imageData
        
        self.headImage.image = UIImage.init(data: imageData)
        
        self.headImage.transform = CGAffineTransform.init(scaleX: -1, y: 1)
        
        presenter.sendApiDetectFace(imageData: imageData)
    }
    
    func gotoResultview()
    {
        let mvc = Router.sharedInstance.getMoodResultView()
        
        self.navigationController?.pushViewController(mvc, animated: true)
    }
}

extension AnalyzeViewController: PresenterDelegate
{
    func onSendApiDetectFace(errCode: Int, result: Any)
    {
        if (errCode == 0)
        {
            let array:Array = result as! Array<Any>
            
            if (array.count > 0)
            {
                let dic = array[0] as! NSDictionary
                
                let faceID = dic.object(forKey: "faceId")
                
                let age = (dic.object(forKey: "faceAttributes") as! NSDictionary).object(forKey: "age")
                
                let gender = (dic.object(forKey: "faceAttributes") as! NSDictionary).object(forKey: "gender")
                
                let emotion = (dic.object(forKey: "faceAttributes") as! NSDictionary).object(forKey: "emotion")
                
                if (getLoginStatus())
                {
                    getFaceStruc().name = getUserStruc().userName
                }
                else
                {
                    getFaceStruc().name = ""
                }
                
                getFaceStruc().faceID = faceID as! String
                
                getFaceStruc().age = age as! Int
                
                getFaceStruc().gender = gender as! String
                
                getFaceStruc().emotion = emotion as! [AnyHashable : Any]
                
                self.isAnalyzeDone = true
            }
            else
            {
                NSLog("onSendApiDetectFace Failed")
            }
        }
        else
        {
            NSLog("onSendApiDetectFace Failed")
            
            stopTimer()
        }
    }
}
