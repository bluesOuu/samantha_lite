//
//  MoodViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MoodViewController: BaseViewController, EntityProtocol
{
    // MARK: - IBOutlet
    
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var labBtn: UILabel!
    @IBOutlet weak var previewView: UIImageView!
    
    let presenter = Presenter.init()
    
    var faceDetectionManager:FaceDetectionManager?
    
    let gpsManager:BOGPSManager = BOGPSManager.sharedInstance() as! BOGPSManager
    
    var latestLocation:CLLocation? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setupFaceDetect()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        if (faceDetectionManager != nil)
        {
            faceDetectionManager!.teardownAVCapture()
        }
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
        
        startStandardUpdates()
    }
    
    func setupInitValue()
    {
        gpsManager.delegate = self
    }
    
    func setupFontSize()
    {
        self.labBtn.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 4)), weight: UIFont.Weight.medium)
    }
    
    func setupLocalized()
    {
        
    }
    
    func setupUILayer()
    {
        if (faceDetectionManager != nil)
        {
            faceDetectionManager!.resetFrame()
        }
        
//        self.bkView.layer.cornerRadius = 10
//
//        self.bkImage.layer.cornerRadius = 10
//
//        self.headImage.layer.cornerRadius = (self.headImage.frame.height / 2)
//
//        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_BAR_MOOD)
    }
    
    func setupFaceDetect()
    {
        if (faceDetectionManager != nil)
        {
            faceDetectionManager?.teardownAVCapture()
            
            faceDetectionManager = nil
        }
        
        faceDetectionManager = FaceDetectionManager.init(previewView: self.previewView, useCameraPosition: FDM_CAMERA_POSITION.FRONT)
        
        faceDetectionManager!.delegate = self;

        faceDetectionManager!.faceLayerImage = UIImage.init(named: "frame_white")
        
        faceDetectionManager!.setupAVCapture()
    }
    
    // MARK: Utilities
    
    func getWeather()
    {
        if (latestLocation != nil)
        {
            YahooWeatherAPI.shared().weather(forLat: String(format: "%f", latestLocation!.coordinate.latitude), lon: String(format: "%f", latestLocation!.coordinate.longitude), format: "json", unit: "c")
            {
                (error:Error?) in
                
                NSLog("error = %@", error.debugDescription)
            }
            success:
            {
                (data:Data?) in
                
                guard let dic = try? JSONSerialization.jsonObject(with: data!, options: []) else
                {
                    return
                }
                
                let dicc:NSDictionary = dic as! NSDictionary
                
                let current_observation:NSDictionary = dicc.object(forKey: "current_observation") as! NSDictionary
                
                let condition:NSDictionary = current_observation.object(forKey: "condition") as! NSDictionary
                
                let temperature:Int = condition.object(forKey: "temperature") as! Int
                
                self.getFaceStruc().temperature = temperature
            }
        }
        else
        {
            getFaceStruc().temperature = -1
        }
    }
    
    func startStandardUpdates()
    {
        let returnCode = gpsManager.startLocationUpdate()
        
        switch (returnCode)
        {
            case GPS_CODE_SUCESS:
                break
            case GPS_CODE_ERROR_AUTH_NOT_YET:
                break
            case GPS_CODE_ERROR_SYS_OFF, GPS_CODE_ERROR_AUTH_OFF:
            
                AlertSupport.showAlertGPS(self)
                {
                    (string) in
                    
                    let url:NSURL = NSURL.init(string: UIApplication.openSettingsURLString)!

                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    
                }
                
                break
            
            default:
                break
        }
    }
    
    // MARK: Button Events
    
    @IBAction func startClick(_ sender: UIButton)
    {
        faceDetectionManager?.captureImage()
        
        faceDetectionManager?.faceLayerImage = UIImage.init(named: "frame_green")
        
        faceDetectionManager?.isShowFaceFrame = true
    }
}

extension MoodViewController: FaceDetectionManagerDelegate
{
    func faceDetectionManagerCaptureOutput(_ image: UIImage!)
    {
        let faceStruc = FaceStruc()
        
        faceStruc.imageData = image.jpegData(compressionQuality: 1)!
        
        setFaceStruc(faceStruc: faceStruc)
        
        getWeather()

        Utilities.delayBlock(with: 0.5, queue: DispatchQueue.main)
        {
            let indexHomeNavi = Router.sharedInstance.getIndexNavigation()
            
            let avc = Router.sharedInstance.getAnalyzeView()
            
            indexHomeNavi.pushViewController(avc, animated: true)
        }
    }
    
    func faceDetectionManagerIsDetectedFace(_ manager: FaceDetectionManager!)
    {
        var frame:CGRect = self.previewView.bounds
    
        frame.size.width = (self.previewView.frame.width * 0.8);

        frame.size.height = (self.previewView.frame.height * 0.8);

        frame.origin.x += ((self.previewView.frame.width - frame.size.width) / 2);

        frame.origin.y += ((self.previewView.frame.height - frame.size.height) / 2);

        for faceLayer in manager.faceLayerArray
        {
            let layer:CALayer = faceLayer as! CALayer

            if (layer.frame.minX >= frame.minX) && (layer.frame.minY >= frame.minY) && (layer.frame.maxX <= frame.maxX) && (layer.frame.maxY <= frame.maxY)
            {
                self.faceDetectionManager!.isShowFaceFrame = true;
            }
            else
            {
                self.faceDetectionManager!.isShowFaceFrame = false;
            }
        }
    }
    
    func faceDetectionCameraError(_ manager: FaceDetectionManager!, error: Error!)
    {
        
    }
}

extension MoodViewController: BOGPSManagerDelegate
{
    func updateGPSInfo(_ cLLocation: CLLocation!)
    {
        latestLocation = cLLocation
        
        NSLog("latestLocation = %f , %f", latestLocation!.coordinate.longitude, latestLocation!.coordinate.latitude)
    }
}
