//
//  MoodRecipeCell.m
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "MoodRecipeCell.h"

@implementation MoodRecipeCell
@synthesize delegate;

- (void)awakeFromNib 
{
    [super awakeFromNib];
    
    [self setup];
}

- (void)drawRect:(CGRect)rect
{
    self.bkView.layer.cornerRadius = 10;
    
    self.bkView2.layer.cornerRadius = 10;
    
    self.recipeImage.layer.cornerRadius = 10;
    
    self.bkImage.layer.cornerRadius = 10;
    
    self.moodView.layer.cornerRadius = (CGRectGetHeight(self.moodView.frame) / 2);
    
    self.brewBtn.layer.cornerRadius = 10;
    
    [Utilities setShadow:self.bkView.layer Opacity:0.38 Radius:8 Offset:CGSizeMake(6, -8) Color:[UIColor blackColor]];
}

- (void)setup
{
    [self setupFont];
    
//    [self setupTap];
    
    [self setupChart];
}

- (void)setupFont
{
//    [self.labOwner setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightMedium]];
//
//    [self.labRecipeName setFont:[UIFont systemFontOfSize:([Utilities getFontSize] + 2) weight:UIFontWeightMedium]];
//    
//    [self.labTime setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 4) weight:UIFontWeightLight]];
}

//- (void)setupTap
//{
//    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
//
//    [self.bkView addGestureRecognizer:gesture];
//}

- (void)setupChart
{
    self.labRecipeFlavor1.text = @"餘韻\n2.5";
    
    self.labRecipeFlavor2.text = @"醇厚\n4.0";
    
    self.labRecipeFlavor3.text = @"酸度\n1.5";
    
    self.labRecipeFlavor4.text = @"香氣\n5.0";
    
    self.labRecipeFlavor5.text = @"甜度\n3.5";
    
    NSArray *valueArray = [NSArray arrayWithObjects:[NSNumber numberWithDouble:2.5], [NSNumber numberWithDouble:1.5], [NSNumber numberWithDouble:3.5], [NSNumber numberWithDouble:5.0], [NSNumber numberWithDouble:4.0], nil];
    
    NSArray *value2Array = [NSArray arrayWithObjects:[NSNumber numberWithDouble:1.0], [NSNumber numberWithDouble:5.0], [NSNumber numberWithDouble:4.0], [NSNumber numberWithDouble:3.0], [NSNumber numberWithDouble:2.0], [NSNumber numberWithDouble:6.0], nil];
    
    RadarChartData *chartData = [RadarChartData new];
    
    for (int j = 0 ; j < 7 ; j++)
    {
        NSMutableArray<ChartDataEntry *> *dataEntries = [NSMutableArray new];
        
        for (int i = 0 ; i < 5 ; i++)
        {
            double value = 0.0;
            
            if (j <= 5)
            {
                value = [value2Array[j] doubleValue];
            }
            else
            {
                value = [valueArray[i] doubleValue];
            }
            
            ChartDataEntry *dataEntry = [[ChartDataEntry alloc] initWithX:i y:value];
            
            [dataEntries addObject:dataEntry];
        }
        
        RadarChartDataSet *chartDataSet = [[RadarChartDataSet alloc] initWithEntries:dataEntries label:@""];
        
        if (j == 5)
        {
            chartDataSet.colors = [NSArray arrayWithObjects:[UIColor clearColor], nil];

            chartDataSet.fillColor = [UIColor clearColor];

            chartDataSet.fillAlpha = 0;

            chartDataSet.lineWidth = 1;

            chartDataSet.valueTextColor = [UIColor clearColor];

            chartDataSet.drawValuesEnabled = NO;
        }
        else if (j < 5)
        {
            chartDataSet.colors = [NSArray arrayWithObjects:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.39], nil];

            chartDataSet.fillColor = [UIColor clearColor];

            chartDataSet.fillAlpha = 0;

            chartDataSet.lineWidth = 1;

            chartDataSet.valueTextColor = [UIColor clearColor];

            chartDataSet.drawValuesEnabled = NO;
        }
        else
        {
            chartDataSet.colors = [NSArray arrayWithObjects:[UIColor clearColor], nil];

            chartDataSet.fillColor = [UIColor colorWithHexString:COLOR_CHART];

            chartDataSet.fillAlpha = 0.77;

            chartDataSet.lineWidth = 0;

            chartDataSet.drawFilledEnabled = YES;

            chartDataSet.drawValuesEnabled = NO;
        }

        [chartData addDataSet:chartDataSet];
    }
    
    self.chartView.data = chartData;

    self.chartView.webColor = [UIColor whiteColor];

    self.chartView.webLineWidth = 1;

    self.chartView.innerWebColor = [UIColor clearColor];

    self.chartView.innerWebLineWidth = 1;

    self.chartView.yAxis.axisMaximum = 5;

    self.chartView.yAxis.axisMinimum = 0;

    self.chartView.xAxis.drawLabelsEnabled = NO;

    self.chartView.yAxis.drawLabelsEnabled = NO;

    self.chartView.legend.enabled = NO;
    
    self.chartView.userInteractionEnabled = NO;
}

#pragma mark - Buttons Event

- (IBAction)brewClick:(id)sender
{
    if ([delegate respondsToSelector:@selector(moodRecipeBrewClick:)])
        [delegate moodRecipeBrewClick:self];
}

@end
