//
//  MoodRecipeProtocol.m
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "MoodRecipeProtocol.h"

@implementation MoodRecipeProtocol
@synthesize delegate;
//@synthesize RecordArray;

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MoodRecipeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MoodRecipeCell" forIndexPath:indexPath];
    
    switch (indexPath.row)
    {
        case 0:
            cell.moodView.backgroundColor = [UIColor colorWithHexString:COLOR_RECIPE_1];
            break;
        case 1:
            cell.moodView.backgroundColor = [UIColor colorWithHexString:COLOR_RECIPE_2];
            break;
        case 2:
            cell.moodView.backgroundColor = [UIColor colorWithHexString:COLOR_RECIPE_3];
            break;
        default:
            break;
    }
//
//    cell.beanImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"A%ld", indexPath.row + 1]];
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - MoodRecipeCellDelegate

 
- (void)moodRecipeBrewClick:(MoodRecipeCell *)cell
{
    if ([delegate respondsToSelector:@selector(moodRecipeBrewClick:)])
        [delegate moodRecipeBrewClick:cell];
}

@end
