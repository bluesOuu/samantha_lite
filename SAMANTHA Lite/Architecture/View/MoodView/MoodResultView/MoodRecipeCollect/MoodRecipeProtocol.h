//
//  MoodRecipeProtocol.h
//  Cafe
//
//  Created by Blues on 2020/6/12.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MoodRecipeCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MoodRecipeProtocolDelegate <MoodRecipeCellDelegate>

@end

@interface MoodRecipeProtocol : NSObject<UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, MoodRecipeCellDelegate>

@property (weak, nonatomic) id<MoodRecipeProtocolDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
