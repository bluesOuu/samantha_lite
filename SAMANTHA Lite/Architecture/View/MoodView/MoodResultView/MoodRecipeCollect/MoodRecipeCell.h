//
//  MoodRecipeCell.h
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Charts/Charts-Swift.h>

@protocol MoodRecipeCellDelegate;

@interface MoodRecipeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *bkView;
@property (weak, nonatomic) IBOutlet UIView *bkView2;
@property (weak, nonatomic) IBOutlet UIView *moodView;
@property (weak, nonatomic) IBOutlet UIImageView *bkImage;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImage;
@property (weak, nonatomic) IBOutlet UILabel *labMood;
@property (weak, nonatomic) IBOutlet UILabel *labFlavor;
@property (weak, nonatomic) IBOutlet UILabel *labLocation;
@property (weak, nonatomic) IBOutlet UILabel *labBean;
@property (weak, nonatomic) IBOutlet UILabel *labFlavorTitle;
@property (weak, nonatomic) IBOutlet UILabel *labLocationTitle;
@property (weak, nonatomic) IBOutlet UILabel *labBeanTitle;
@property (weak, nonatomic) IBOutlet RadarChartView *chartView;

@property (weak, nonatomic) IBOutlet UILabel *labRecipeFlavor1;
@property (weak, nonatomic) IBOutlet UILabel *labRecipeFlavor2;
@property (weak, nonatomic) IBOutlet UILabel *labRecipeFlavor3;
@property (weak, nonatomic) IBOutlet UILabel *labRecipeFlavor4;
@property (weak, nonatomic) IBOutlet UILabel *labRecipeFlavor5;

@property (weak, nonatomic) IBOutlet UILabel *labBtn;
@property (weak, nonatomic) IBOutlet UIButton *brewBtn;

@property (weak, nonatomic) id<MoodRecipeCellDelegate> delegate;

@end

@protocol MoodRecipeCellDelegate <NSObject>

- (void)moodRecipeBrewClick:(MoodRecipeCell *)cell;

@end
