//
//  MoodResultViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MoodResultViewController: BaseViewController, EntityProtocol
{
    // MARK: - IBOutlet
    
    @IBOutlet weak var bkView1: UIView!
    @IBOutlet weak var bkImage1: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var labGender: UILabel!
    @IBOutlet weak var labAge: UILabel!
    @IBOutlet weak var labWeather: UILabel!
    @IBOutlet weak var labGenderTitle: UILabel!
    @IBOutlet weak var labAgeTitle: UILabel!
    @IBOutlet weak var labWeatherTitle: UILabel!
    @IBOutlet weak var labBtnReScan: UILabel!
    @IBOutlet weak var progressView1: CircleProgressView!
    @IBOutlet weak var progressView2: CircleProgressView!
    @IBOutlet weak var progressView3: CircleProgressView!
    @IBOutlet weak var labMoodKey1: UILabel!
    @IBOutlet weak var labMoodKey2: UILabel!
    @IBOutlet weak var labMoodKey3: UILabel!
    @IBOutlet weak var labMoodValue1: UILabel!
    @IBOutlet weak var labMoodValue2: UILabel!
    @IBOutlet weak var labMoodValue3: UILabel!
    @IBOutlet var moodRecipeProtocol:MoodRecipeProtocol!
    @IBOutlet var chosenRecipeProtocol:ChosenRecipeProtocol!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var collectionChosenView:UICollectionView!
    
    let presenter = Presenter.init()
    
    var emotionArray:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        updateInfo()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()

        setupUILayer()
    }

    // MARK: - Setup

    func setup()
    {
        setupInitValue()

        setupFontSize()

        setupLocalized()

        setupNaviBar()
        
        setupCollectionView()
        
        setupCollectionChosenView()
    }
    
    func setupInitValue()
    {
        self.progressView1.roundedCap = true
        self.progressView2.roundedCap = true
        self.progressView3.roundedCap = true
    }

    func setupFontSize()
    {
        self.labName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 4)), weight: UIFont.Weight.bold)
        
        self.labBtnReScan.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.bold)
        
        self.labAgeTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.medium)
        
        self.labGenderTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.medium)
        
        self.labWeatherTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.medium)
        
        self.labAge.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.labGender.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.labWeather.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.labMoodValue1.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 3)), weight: UIFont.Weight.light)
        
        self.labMoodValue2.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 3)), weight: UIFont.Weight.light)
        
        self.labMoodValue3.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 3)), weight: UIFont.Weight.light)
        
        self.labMoodKey1.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 5)), weight: UIFont.Weight.medium)
        
        self.labMoodKey2.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 5)), weight: UIFont.Weight.medium)
        
        self.labMoodKey3.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 5)), weight: UIFont.Weight.medium)
    }

    func setupLocalized()
    {

    }

    func setupUILayer()
    {
        self.bkView1.layer.cornerRadius = 10
        
        self.bkImage1.layer.cornerRadius = 10
        
        self.headImage.layer.cornerRadius = (self.headImage.frame.height / 2)
        
        Utilities.setShadow(self.bkView1.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)

        self.naviBar.imageLogo.isHidden = true

        self.naviBar.labTitle.isHidden = false

        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_BAR_MOOD)
    }
    
    func setupCollectionView()
    {
        let width = ((UIScreen.main.bounds.size.width / 414) * 360)
        
        let height = ((width / 360) * 380)

        let lineLayout: LineLayout = LineLayout(cellSize: CGSize.init(width: width, height: height))
        
        moodRecipeProtocol.delegate = self
        
        self.collectionView.collectionViewLayout = lineLayout
        
        self.collectionView.register(UINib.init(nibName: "MoodRecipeCell", bundle: nil), forCellWithReuseIdentifier: "MoodRecipeCell")
        
        self.collectionView.allowsSelection = true;

        self.collectionView.allowsMultipleSelection = false;

        self.collectionView.decelerationRate = UIScrollView.DecelerationRate(rawValue: 0.1);

        self.collectionView.reloadData()
    }
    
    func setupCollectionChosenView()
    {
        chosenRecipeProtocol.delegate = self
        
        self.collectionChosenView.register(UINib.init(nibName: "ChosenRecipeCell", bundle: nil), forCellWithReuseIdentifier: "ChosenRecipeCell")
        
        self.collectionChosenView.allowsSelection = true;

        self.collectionChosenView.allowsMultipleSelection = false;

        self.collectionChosenView.reloadData()
    }

    // MARK: Button Events

    @IBAction func reScanClick(_ sender: UIButton)
    {
        let indexHomeNavi = Router.sharedInstance.getIndexNavigation()
        
        let ivc:IndexHomeViewController = indexHomeNavi.viewControllers.first as! IndexHomeViewController
        
        let mvc = ivc.getChildren(aClass: MoodViewController.self) as! MoodViewController
        
        mvc.setupFaceDetect()
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: Utilities
    
    func strEmotionChange(inPutEmotion:String) -> String
    {
        if (inPutEmotion.elementsEqual("anger"))
        {
            return "生氣"
        }
        
        if (inPutEmotion.elementsEqual("contempt"))
        {
            return "輕蔑鄙視"
        }
        
        if (inPutEmotion.elementsEqual("disgust"))
        {
            return "失望厭惡"
        }
        
        if (inPutEmotion.elementsEqual("fear"))
        {
            return "害怕"
        }

        if (inPutEmotion.elementsEqual("happiness"))
        {
            return "開心"
        }
        
        if (inPutEmotion.elementsEqual("neutral"))
        {
            return "自然"
        }
        
        if (inPutEmotion.elementsEqual("sadness"))
        {
            return "悲傷"
        }
        
        if (inPutEmotion.elementsEqual("surprise"))
        {
            return "驚訝"
        }
        
        return "自然"
    }
    
    func calculationEmotion(emotion:NSDictionary) -> String
    {
        var max:Double = 0.0
        
        var maxKey:String = ""
        
        for key in emotion.allKeys
        {
            let cur:Double = emotion.object(forKey: key) as! Double
            
            if (cur > max)
            {
                max = cur
                
                maxKey = key as! String
            }
        }
        
        return maxKey
    }
    
    func sortEmotion()
    {
        emotionArray.removeAllObjects()
        
        let emotion:NSDictionary = getFaceStruc().emotion as NSDictionary
        
        for key in emotion.allKeys
        {
            let value:Double = emotion.object(forKey: key) as! Double
            
            let dic: [String:Any] = ["emotion":key, "value":value]
            
            emotionArray.add(dic)
        }

        let sortDescriptor = NSSortDescriptor(key: "value", ascending: false)
        
        emotionArray.sort(using: [sortDescriptor])
    }
    
    func updateInfo()
    {
        getFaceStruc().printSelf()
        
        self.headImage.image = UIImage.init(data: getFaceStruc().imageData)
        
        self.headImage.transform = CGAffineTransform.init(scaleX: -1, y: 1)
        
        self.labName.text = String(format: "Hi %@", getFaceStruc().name)
        
        self.labAge.text = String(format: "%d", getFaceStruc().age)
        
        let gender:NSString = getFaceStruc().gender as NSString
        
        self.labGender.text = gender.isEqual("male") ? "男性" : "女性"
        
        if (getFaceStruc().temperature > 0)
        {
            self.labWeather.text = String(format: "%d°C", getFaceStruc().temperature)
        }
        else
        {
            self.labWeather.text = ""
        }
        
        sortEmotion()
        
        let dic1:NSDictionary = emotionArray[0] as! NSDictionary
        
        let dic2:NSDictionary = emotionArray[1] as! NSDictionary
        
        let dic3:NSDictionary = emotionArray[2] as! NSDictionary
        
        let value1 = dic1.object(forKey: "value") as! Double
        
        let value2 = dic2.object(forKey: "value") as! Double
        
        let value3 = dic3.object(forKey: "value") as! Double
        
        let str1 = String(format: "%.0f", (value1 * 100))
        
        let str2 = String(format: "%.0f", (value2 * 100))
        
        let str3 = String(format: "%.0f", (value3 * 100))
        
        let valuev1 = (Double(str1)! / 100)
        
        let valuev2 = (Double(str2)! / 100)
        
        let valuev3 = (Double(str3)! / 100)
            
        self.progressView1.progress = valuev1
        
        self.progressView2.progress = valuev2
        
        self.progressView3.progress = valuev3
        
        self.labMoodValue1.text = String(format: "%@%%", str1)
        
        self.labMoodValue2.text = String(format: "%@%%", str2)
        
        self.labMoodValue3.text = String(format: "%@%%", str3)
        
        self.labMoodKey1.text = strEmotionChange(inPutEmotion: dic1.object(forKey: "emotion") as! String)
        
        self.labMoodKey2.text = strEmotionChange(inPutEmotion: dic2.object(forKey: "emotion") as! String)
        
        self.labMoodKey3.text = strEmotionChange(inPutEmotion: dic3.object(forKey: "emotion") as! String)
        
        if (self.progressView1.progress > 0.97)
        {
            self.progressView1.roundedCap = false
        }
        
        if (self.progressView2.progress > 0.97)
        {
            self.progressView2.roundedCap = false
        }
        
        if (self.progressView3.progress > 0.97)
        {
            self.progressView3.roundedCap = false
        }
    }
}

extension MoodResultViewController: MoodRecipeProtocolDelegate
{
    func moodRecipeBrewClick(_ cell: MoodRecipeCell!)
    {
        let indexPath = self.collectionView.indexPath(for: cell)
        
        print("moodRecipeBrewClick = " + String(format: "%d", indexPath!.row))
    }
}

extension MoodResultViewController: ChosenRecipeProtocolDelegate
{
    func chosenRecipeCellClick(_ cell: ChosenRecipeCell!)
    {
        let indexPath = self.collectionChosenView.indexPath(for: cell)
        
        print("chosenRecipeCellClick = " + String(format: "%d", indexPath!.row))
    }
}
