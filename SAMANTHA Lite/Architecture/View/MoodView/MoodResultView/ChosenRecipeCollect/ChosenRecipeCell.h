//
//  ChosenRecipeCell.h
//  12Ours
//
//  Created by Blues on 2017/6/20.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChosenRecipeCellDelegate;

@interface ChosenRecipeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *bkView;
@property (weak, nonatomic) IBOutlet UIImageView *bkImage;
@property (weak, nonatomic) IBOutlet UILabel *labChosen;
@property (weak, nonatomic) IBOutlet UILabel *labRecipe;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImage;

@property (weak, nonatomic) id<ChosenRecipeCellDelegate> delegate;

@end

@protocol ChosenRecipeCellDelegate <NSObject>

- (void)chosenRecipeCellClick:(ChosenRecipeCell *)cell;

@end
