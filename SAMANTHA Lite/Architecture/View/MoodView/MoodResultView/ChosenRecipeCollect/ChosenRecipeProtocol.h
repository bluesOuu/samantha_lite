//
//  RecipeCollectionProtocol.h
//  HiroiaBleSample
//
//  Created by BluesOu on 2019/7/31.
//

#import <Foundation/Foundation.h>
#import "ChosenRecipeCell.h"

@protocol ChosenRecipeProtocolDelegate <ChosenRecipeCellDelegate>

@end

@interface ChosenRecipeProtocol : NSObject<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ChosenRecipeCellDelegate>

@property (assign, nonatomic) BOOL isCahamption;

@property (weak, nonatomic) id<ChosenRecipeProtocolDelegate> delegate;

@end

