//
//  RecipeCollectionProtocol.m
//  HiroiaBleSample
//
//  Created by BluesOu on 2019/7/31.
//

#import "ChosenRecipeProtocol.h"

@implementation ChosenRecipeProtocol
@synthesize delegate;
@synthesize isCahamption;

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ChosenRecipeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ChosenRecipeCell" forIndexPath:indexPath];
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = ((CGRectGetWidth(collectionView.frame) / 414) * 194);
    
    CGFloat height = ((width / 194) * 256);
    
    return CGSizeMake(width, height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 21, 0, 21);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

#pragma mark - UICollectionViewDelegate methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - ChosenRecipeCellDelegate

- (void)chosenRecipeCellClick:(ChosenRecipeCell *)cell
{
    if ([delegate respondsToSelector:@selector(chosenRecipeCellClick:)])
        [delegate chosenRecipeCellClick:cell];
}

@end
