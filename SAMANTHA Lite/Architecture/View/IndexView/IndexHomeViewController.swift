//
//  IndexHomeViewCoontroller.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/14.
//

import Foundation

class IndexHomeViewController: BaseViewController
{
    // MARK: - IBOutlet
    
    @IBOutlet weak var toolBar: BaseToolBar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    
    // MARK: - Variable
    
    var curPage = 0
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupToolBar()
        
        setupHomeView()
        
        setupChosenView()
        
        setupMoodView()
        
        setupDeviceView()
        
        setupMyView()
    }
    
    func setupInitValue()
    {
        
    }
    
    func setupToolBar()
    {
        self.toolBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.bottomView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.toolBar.delegate = self
        
        self.toolBar.setTabImgWithTag(0, imageName: "bar_icon_home")
        self.toolBar.setTabImgWithTag(1, imageName: "bar_icon_featured")
        self.toolBar.setTabImgWithTag(2, imageName: "bar_icon_mood")
        self.toolBar.setTabImgWithTag(3, imageName: "bar_icon_device")
        self.toolBar.setTabImgWithTag(4, imageName: "bar_icon_my")
        
        self.toolBar.setTabLabelWithTag(0, text: MultiLangManager.getLangString(forKey: STR_BAR_HOME))
        self.toolBar.setTabLabelWithTag(1, text: MultiLangManager.getLangString(forKey: STR_BAR_FEATURED))
        self.toolBar.setTabLabelWithTag(2, text: MultiLangManager.getLangString(forKey: STR_BAR_MOOD))
        self.toolBar.setTabLabelWithTag(3, text: MultiLangManager.getLangString(forKey: STR_BAR_DEVICE))
        self.toolBar.setTabLabelWithTag(4, text: MultiLangManager.getLangString(forKey: STR_BAR_MY))
        
        let btn:UIButton = self.toolBar.viewWithTag(Int(BTN_TAG_INIT_VALUE)) as! UIButton
        
        btn.sendActions(for: UIControl.Event.touchUpInside)
    }

    func setupHomeView()
    {
        let hvc: HomeViewController = Router.sharedInstance.getHomeView()
        
        self.addChild(hvc)
        
        hvc.view.frame = CGRect.init(x: 0, y: 0, width: self.view1.frame.size.width, height: self.view1.frame.size.height)
        
        self.view1.addSubview(hvc.view)
    }
    
    func setupChosenView()
    {
        let mvc: ChosenViewController = Router.sharedInstance.getChosenView()
        
        self.addChild(mvc)
        
        mvc.view.frame = CGRect.init(x: 0, y: 0, width: self.view2.frame.size.width, height: self.view2.frame.size.height)
        
        self.view2.addSubview(mvc.view)
    }
    
    func setupMoodView()
    {
        let mvc: MoodViewController = Router.sharedInstance.getMoodView()
        
        self.addChild(mvc)
        
        mvc.view.frame = CGRect.init(x: 0, y: 0, width: self.view3.frame.size.width, height: self.view3.frame.size.height)
        
        self.view3.addSubview(mvc.view)
    }
    
    func setupDeviceView()
    {
        let mvc: DeviceViewController = Router.sharedInstance.getDeviceView()
        
        self.addChild(mvc)
        
        mvc.view.frame = CGRect.init(x: 0, y: 0, width: self.view4.frame.size.width, height: self.view4.frame.size.height)
        
        self.view4.addSubview(mvc.view)
    }
    
    func setupMyView()
    {
        let mvc: MyViewController = Router.sharedInstance.getMyView()
        
        self.addChild(mvc)
        
        mvc.view.frame = CGRect.init(x: 0, y: 0, width: self.view5.frame.size.width, height: self.view5.frame.size.height)
        
        self.view5.addSubview(mvc.view)
    }
    
    // MARK: - Utilities
    
    func getChildren(aClass:AnyClass) -> Any
    {
        var vc:Any?
        
        for chldren1 in self.children
        {
            if (chldren1.isKind(of: aClass.self))
            {
                vc = chldren1
            }
        }
        
        return vc as Any
    }
    
    func scrollToPage(page:Int, animated:Bool)
    {
        if (page == 0)
        {
            for chldren1 in self.children
            {
                if (chldren1.isKind(of: HomeViewController.self))
                {
                    let hvc = chldren1 as! HomeViewController

                    hvc.viewWillAppear(true)
                    
                    break
                }
            }
        }
        else if (page == 1)
        {
            for chldren1 in self.children
            {
                if (chldren1.isKind(of: ChosenViewController.self))
                {
                    let hvc = chldren1 as! ChosenViewController

                    hvc.viewWillAppear(true)
                    
                    break
                }
            }
        }
        else if (page == 2)
        {
            for chldren1 in self.children
            {
                if (chldren1.isKind(of: MoodViewController.self))
                {
                    let hvc = chldren1 as! MoodViewController

                    hvc.viewWillAppear(true)
                                        
                    break
                }
            }
        }
        else if (page == 4)
        {
            for chldren1 in self.children
            {
                if (chldren1.isKind(of: MyViewController.self))
                {
                    let hvc = chldren1 as! MyViewController

                    hvc.viewWillAppear(true)
                    
                    break
                }
            }
        }
        
        self.scrollView.setContentOffset(CGPoint.init(x: (Int(self.scrollView.frame.size.width) * page), y: 0), animated: animated)
    }
}

extension IndexHomeViewController: BaseToolBarDelegate
{
    @objc func baseToolBarBtnClick(_ tag: Int)
    {
        if (curPage == tag)
        {
            return
        }
        
        if (tag > 0)
        {
            self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        }
        else
        {
            self.topView.backgroundColor = UIColor.clear
        }
        
        curPage = tag
        
        if (curPage != 2)
        {
            for chldren1 in self.children
            {
                if (chldren1.isKind(of: MoodViewController.self))
                {
                    let hvc = chldren1 as! MoodViewController

                    hvc.viewWillDisappear(true)
                    
                    break
                }
            }
        }
        
        scrollToPage(page: tag, animated: true)
    }
}
