//
//  ForgetViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

import UIKit

class ForgetViewController: BaseViewController, AlertProtocol
{
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var forgetView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var labWelcome: UILabel!
    @IBOutlet weak var labInfo: UILabel!
    @IBOutlet weak var accView: UIView!
    @IBOutlet weak var textAcc: UITextField!
    @IBOutlet weak var forgetBtn: UIButton!
    @IBOutlet weak var labBtnForget: UILabel!
    @IBOutlet weak var viewUp: NSLayoutConstraint!
    @IBOutlet weak var viewDown: NSLayoutConstraint!
    
    let presenter = Presenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupTextField()
        
        setupNaviBar()
        
        setupNotification()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
        
        self.textAcc.text = "blues@hiroia.com"
    }
    
    func setupFontSize()
    {
        self.labWelcome.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 10)), weight: UIFont.Weight.bold)

        self.labInfo.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        
        self.textAcc.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        self.labBtnForget.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
    }
    
    func setupLocalized()
    {
        self.labWelcome.text = MultiLangManager.getLangString(forKey: STR_FORGET_INFO_1)
        
        self.labInfo.text = MultiLangManager.getLangString(forKey: STR_FORGET_INFO_2)
        
        self.labBtnForget.text = MultiLangManager.getLangString(forKey: STR_FORGET)
    }
    
    func setupUILayer()
    {
        self.forgetView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10
        
        self.forgetBtn.layer.cornerRadius = 10
        
        self.accView.layer.cornerRadius = 10
        
        self.accView.layer.borderWidth = 1
        
        self.accView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        Utilities.setShadow(self.forgetView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }
    
    func setupTextField()
    {
        let placeholder = MultiLangManager.getLangString(forKey: STR_HINT_ACCOUNT)
        
        let font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        let attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])
        
        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))
        
        self.textAcc.attributedPlaceholder = attrStr
        
        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(gesture)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Button Events
    
    @IBAction func forgetClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        AlertProcessView.show()
        
        presenter.sendApiForgetWith(acc: self.textAcc.text!)
    }
    
    // MARK: - Utilities
    
    @objc func handleTap(recognizer:Any)
    {
        self.textAcc.resignFirstResponder()
    }
    
    // MARK: - Notification
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        
        self.viewUp.priority = UILayoutPriority.defaultHigh

        self.viewDown.priority = UILayoutPriority.defaultLow
        
        UIView.animate(withDuration: duration)
        {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double

        self.viewUp.priority = UILayoutPriority.defaultLow

        self.viewDown.priority = UILayoutPriority.defaultHigh
        
        UIView.animate(withDuration: duration)
        {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - UITextFieldDelegate

extension ForgetViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//        if (textField == self.textPwd)
//        {
//            return Utilities.checkInputChar(string, withThisCase: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@")
//        }
//        else
//        {
//            return Utilities.checkInputChar(string, withThisCase: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@")
//        }
//    }
}

// MARK: - ForgetPresenterDelegate

extension ForgetViewController: PresenterDelegate
{
    func onSendApiForgetWith(errCode: Int)
    {
        AlertProcessView.hide()
        
        if (errCode != 0)
        {
            showAlertNetworkError(errCode: errCode) { (string) in }
        }
        else
        {
            showAlertResetPwdSucces(acc: self.textAcc.text!)
            {
                (String) in
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

// MARK: - BaseNavigationBarDelegate

extension ForgetViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
