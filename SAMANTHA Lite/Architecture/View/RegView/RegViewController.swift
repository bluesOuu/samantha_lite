//
//  RegViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

import UIKit

class RegViewController: BaseViewController, AlertProtocol
{
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var regView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var labWelcome: UILabel!
    @IBOutlet weak var labInfo: UILabel!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var accView: UIView!
    @IBOutlet weak var textAcc: UITextField!
    @IBOutlet weak var pwdView: UIView!
    @IBOutlet weak var textPwd: UITextField!
    @IBOutlet weak var pwdCfView: UIView!
    @IBOutlet weak var textPwdCf: UITextField!
    @IBOutlet weak var regBtn: UIButton!
    @IBOutlet weak var labBtnReg: UILabel!
    @IBOutlet weak var viewUp: NSLayoutConstraint!
    @IBOutlet weak var viewDown: NSLayoutConstraint!
    @IBOutlet weak var labPrivacy: UILabel!
    @IBOutlet weak var labUserTerms: UILabel!
    
    let presenter = Presenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupTextField()
        
        setupNaviBar()
        
        setupNotification()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
        
        self.textName.text = "Blues"
        
        self.textAcc.text = "blues@hiroia.com"
        
        self.textPwd.text = "1234"
        
        self.textPwdCf.text = "1234"
    }
    
    func setupFontSize()
    {
        self.labWelcome.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 10)), weight: UIFont.Weight.bold)

        self.labInfo.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)

        self.textName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)

        self.textAcc.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        self.textPwd.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)

        self.textPwdCf.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)

        self.labBtnReg.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        self.labPrivacy.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.bold)
        
        self.labUserTerms.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        self.labWelcome.text = MultiLangManager.getLangString(forKey: STR_REG_INFO_1)

        self.labInfo.text = MultiLangManager.getLangString(forKey: STR_REG_INFO_2)

        self.labBtnReg.text = MultiLangManager.getLangString(forKey: STR_REGISTER)
        
        self.labUserTerms.text = MultiLangManager.getLangString(forKey: STR_USER_TERMS)
        
        self.labPrivacy.text = MultiLangManager.getLangString(forKey: STR_PRIVACY)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
    }
    
    func setupUILayer()
    {
        self.regView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10

        self.regBtn.layer.cornerRadius = 10
        
        self.nameView.layer.cornerRadius = 10

        self.nameView.layer.borderWidth = 1

        self.nameView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor

        self.accView.layer.cornerRadius = 10

        self.accView.layer.borderWidth = 1

        self.accView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor

        self.pwdView.layer.cornerRadius = 10

        self.pwdView.layer.borderWidth = 1

        self.pwdView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        self.pwdCfView.layer.cornerRadius = 10

        self.pwdCfView.layer.borderWidth = 1

        self.pwdCfView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        Utilities.setShadow(self.regView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: -10), color: UIColor.black)
    }
    
    func setupTextField()
    {
        var placeholder = MultiLangManager.getLangString(forKey: STR_HINT_NAME)

        let font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        
        var attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])

        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))

        self.textName.attributedPlaceholder = attrStr

        placeholder = MultiLangManager.getLangString(forKey: STR_HINT_ACCOUNT)

        attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])

        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))

        self.textAcc.attributedPlaceholder = attrStr

        placeholder = MultiLangManager.getLangString(forKey: STR_HINT_PASSWORD)

        attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])

        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))

        self.textPwd.attributedPlaceholder = attrStr
        
        placeholder = MultiLangManager.getLangString(forKey: STR_HINT_PASSWORD_CONFIRM)

        attrStr = NSMutableAttributedString(string:placeholder!, attributes: [NSAttributedString.Key.font:font])

        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(white: 1, alpha: 0.33), range:NSRange(location:0,length:placeholder!.count))

        self.textPwdCf.attributedPlaceholder = attrStr

        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap))

        self.view.addGestureRecognizer(gesture)
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Button Events
    
    @IBAction func regClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        AlertProcessView.show()
        
        presenter.sendApiRegWith(acc: self.textAcc.text!, pwd: self.textPwd.text!, name: self.textName.text!, longitude: 0, latitude: 0)
    }
    
    @IBAction func userTermsClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        let languageKey = MultiLangManager.getCurLangKey()
        
        UIApplication.shared.open(URL.init(string: String(format: "https://shop.hiroia.com/service?app=true&language=%@&app_name=samantha", languageKey!))!, options: [:], completionHandler: nil)
    }
    
    @IBAction func privacyClick(_ sender: UIButton)
    {
        handleTap(recognizer: sender)
        
        UIApplication.shared.open(URL.init(string: String(format: "https://shop.hiroia.com/privacy?app=true&language=%@&app_name=samantha", "en"))!, options: [:], completionHandler: nil)
    }

    // MARK: - Utilities
    
    @objc func handleTap(recognizer:Any)
    {
        self.textName.resignFirstResponder()
        
        self.textAcc.resignFirstResponder()

        self.textPwd.resignFirstResponder()
        
        self.textPwdCf.resignFirstResponder()
    }
    
    func parserErrCode(errCode: Int)
    {
        print("parserErrCode = " + String(errCode))
        
        switch errCode
        {
            case 4090301:
                showAlertAccHasUse
                {
                    (string) in
                    
                    if (string.elementsEqual(MultiLangManager.getLangString(forKey: STR_FORGET)))
                    {
                        AlertProcessView.show()
                        
                        self.presenter.sendApiForgetWith(acc: self.textAcc.text!)
                    }
                }
                break
            case 4090302:
                showAlertRegSucces { (string) in }
                break
            default:
                showAlertNetworkError(errCode: errCode) { (string) in }
                break
        }
    }
    
    // MARK: - Notification
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double

        self.viewUp.priority = UILayoutPriority.defaultHigh

        self.viewDown.priority = UILayoutPriority.defaultLow

        UIView.animate(withDuration: duration)
        {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double

        self.viewUp.priority = UILayoutPriority.defaultLow

        self.viewDown.priority = UILayoutPriority.defaultHigh

        UIView.animate(withDuration: duration)
        {
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - UITextFieldDelegate

extension RegViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == self.textName)
        {
            self.textAcc.becomeFirstResponder()
        }
        else if (textField == self.textAcc)
        {
            self.textPwd.becomeFirstResponder()
        }
        else if (textField == self.textPwd)
        {
            self.textPwdCf.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//        if (textField == self.textPwd)
//        {
//            return Utilities.checkInputChar(string, withThisCase: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@")
//        }
//        else
//        {
//            return Utilities.checkInputChar(string, withThisCase: "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@")
//        }
//    }
}

// MARK: - RegPresenterDelegate

extension RegViewController: PresenterDelegate
{
    func onSendApiRegWith(errCode: Int)
    {
        AlertProcessView.hide()
        
        if (errCode != 0)
        {
            parserErrCode(errCode: errCode)
        }
        else
        {
            showAlertRegSucces { (string) in }
        }
    }
    
    func onSendApiReSendActiviteEmail(errCode: Int)
    {
        AlertProcessView.hide()

        if (errCode != 0)
        {
            showAlertNetworkError(errCode: errCode) { (string) in }
        }
        else
        {
            showAlertActiviteSendDone
            {
                (alertAction) in
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    func onSendApiForgetWith(errCode: Int)
    {
        AlertProcessView.hide()
        
        if (errCode != 0)
        {
            showAlertNetworkError(errCode: errCode) { (string) in }
        }
        else
        {
            showAlertResetPwdSucces(acc: self.textAcc.text!)
            {
                (String) in
                
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

// MARK: - BaseNavigationBarDelegate

extension RegViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        self.navigationController?.popViewController(animated: true)
    }
}
