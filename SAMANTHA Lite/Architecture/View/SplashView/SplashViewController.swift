//
//  SplashViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

import UIKit

class SplashViewController: BaseViewController, EntityProtocol
{
    let presenter = Presenter.init()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
        
        presenter.sendApiGetUserInfo()
    }
}

// MARK: - SplashPresenterDelegate

extension SplashViewController: PresenterDelegate
{
    func onSendApiGetUserInfo(errCode: Int)
    {
        if (errCode == 0)
        {
            getUserStruc().printSelf()
        }
        else
        {
            runLogoutStep()
        }
        
        Router.sharedInstance.gotoIndexHomeView()
    }
}
