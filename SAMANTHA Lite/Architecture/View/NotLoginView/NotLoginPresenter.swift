//
//  NotLoginPresenter.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/14.
//

import Foundation

class NotLoginPresenter: NSObject
{
    weak var delegate: NotLoginPresenterDelegate?
    
    override init()
    {
        super.init()
    }
}

protocol NotLoginPresenterDelegate: class
{
    
}

extension NotLoginPresenterDelegate
{
    
}
