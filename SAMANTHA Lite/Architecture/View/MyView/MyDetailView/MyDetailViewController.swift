//
//  MyDetailViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MyDetailViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var bkView1: UIView!
    @IBOutlet weak var bkView2: UIView!
    @IBOutlet weak var bkImage1: UIImageView!
    @IBOutlet weak var bkImage2: UIImageView!
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var labGenderTitle: UILabel!
    @IBOutlet weak var labBirthdayTitle: UILabel!
    @IBOutlet weak var labGender: UILabel!
    @IBOutlet weak var labBirthday: UILabel!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textGender: UITextField!
    @IBOutlet weak var textBirthday: UITextField!
    @IBOutlet weak var labBtnSave: UILabel!
    @IBOutlet weak var labBtnResetPwd: UILabel!
    @IBOutlet weak var labBtnEdit: UILabel!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var resetPwdBtn: UIButton!
    @IBOutlet var datePicker: UIDatePicker!
    
    let presenter = Presenter.init()
    
    var isEdit:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        getUserInfo()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
    }
    
    func setupInitValue()
    {
        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(gesture)
        
        let gesture1:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleHeadTap))
        
        self.headImage.addGestureRecognizer(gesture1)
        
        self.textGender.inputView = nil
        
        self.textBirthday.inputView = datePicker
        
        self.setupInputAccessoryView(#selector(keyboardClose))
        
        let array:Array = UserDefaults.standard.object(forKey: "AppleLanguages") as! Array<String>
        
        let language = array[0]
        
        datePicker.locale = Locale.init(identifier: language)
    }
    
    func setupFontSize()
    {
        self.labName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 8)), weight: UIFont.Weight.bold)
        
        self.labGenderTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        self.labBirthdayTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        
        self.labGender.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.light)
        self.labBirthday.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.light)
        
        self.textName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.medium)
        self.textGender.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.medium)
        self.textBirthday.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.medium)
        
        self.labBtnEdit.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        self.labBtnResetPwd.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
        self.labBtnSave.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        self.labBtnSave.text = MultiLangManager.getLangString(forKey: STR_EDIT_CONFIRM)
        
        self.labBtnEdit.text = MultiLangManager.getLangString(forKey: STR_PROFILE_EDIT)
        
        self.labBtnResetPwd.text = MultiLangManager.getLangString(forKey: STR_PASSWORD_EDIT)
        
        self.labGender.text = ""
        
        self.labBirthday.text = ""
        
        self.textGender.text = self.labGender.text
        
        self.textBirthday.text = self.labBirthday.text
        
        self.labGenderTitle.text = MultiLangManager.getLangString(forKey: STR_GENDER)
        
        self.labBirthdayTitle.text = MultiLangManager.getLangString(forKey: STR_BIRTHDAY)
    }
    
    func setupUILayer()
    {
        self.bkView1.layer.cornerRadius = 10
        self.bkView2.layer.cornerRadius = 10
        self.bkImage1.layer.cornerRadius = 10
        self.bkImage2.layer.cornerRadius = 10
        self.saveBtn.layer.cornerRadius = 10
        
        self.textName.layer.cornerRadius = 10
        self.textName.layer.borderWidth = 1
        self.textName.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        self.genderView.layer.cornerRadius = 10
        self.genderView.layer.borderWidth = 1
        self.genderView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        self.birthdayView.layer.cornerRadius = 10
        self.birthdayView.layer.borderWidth = 1
        self.birthdayView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        self.headImage.layer.cornerRadius = (self.headImage.frame.height / 2)
        
        Utilities.setShadow(self.bkView1.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
        
        Utilities.setShadow(self.bkView2.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_MY_PROFILE_SETTING)
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
    }
    
    // MARK: Button Events
    
    @IBAction func editClick(_ sender: UIButton)
    {
        isEdit = true
        
        changeUIStatus()
    }
    
    @IBAction func resetPwdClick(_ sender: UIButton)
    {
        let cvc: ChangePwdViewController = Router.sharedInstance.getChnagePwdView()
        
        self.navigationController?.pushViewController(cvc, animated: true)
    }
    
    @IBAction func saveClick(_ sender: UIButton)
    {
        isEdit = false
        
        self.labName.text = self.textName.text
        
        self.labGender.text = self.textGender.text
        
        self.labBirthday.text = self.textBirthday.text
        
        changeUIStatus()
    }
    
    // MARK: Utilities
    
    func getUserInfo()
    {
        guard let userStruc = presenter.getUserInfo() else
        {
            self.headImage.image = nil

            self.labName.text = ""

            return
        }

        self.headImage.image = UIImage.init(data: userStruc.imageData)

        self.labName.text = userStruc.userName
    }
    
    func changeUIStatus()
    {
        UIView.animate(withDuration: 0.3)
        {
            if (self.isEdit)
            {
                self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_EDIT)
                
                self.textName.alpha = 1
                self.genderView.alpha = 1
                self.birthdayView.alpha = 1
                
                self.labName.alpha = 0
                self.labGender.alpha = 0
                self.labBirthday.alpha = 0
                
                self.saveBtn.alpha = 1
                self.labBtnSave.alpha = 1
                
                self.editBtn.alpha = 0
                self.resetPwdBtn.alpha = 0
                
                self.editImage.alpha = 1
                
                self.textName.text = self.labName.text
                
                self.textGender.text = self.labGender.text
                
                self.textBirthday.text = self.labBirthday.text
            }
            else
            {
                self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_MY_PROFILE_SETTING)
                
                self.textName.alpha = 0
                self.genderView.alpha = 0
                self.birthdayView.alpha = 0
                
                self.labName.alpha = 1
                self.labGender.alpha = 1
                self.labBirthday.alpha = 1
                
                self.saveBtn.alpha = 0
                self.labBtnSave.alpha = 0
                
                self.editBtn.alpha = 1
                self.resetPwdBtn.alpha = 1
                
                self.editImage.alpha = 0
                
                self.labName.text = self.textName.text
                
                self.labGender.text = self.textGender.text
                
                self.labBirthday.text = self.textBirthday.text
            }
        }
    }
    
    @objc func handleTap(recognizer:Any)
    {
        self.textName.resignFirstResponder()
        
        self.textGender.resignFirstResponder()
        
        self.textBirthday.resignFirstResponder()
    }
    
    @objc func keyboardClose()
    {
        if (self.textBirthday.isFirstResponder)
        {
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy/MM/dd"
            
            self.textBirthday.text = dateFormatter.string(from: datePicker.date)
        }
        
        self.textBirthday.resignFirstResponder()
        
        self.textName.resignFirstResponder()
    }
    
    @objc func handleHeadTap(recognizer:Any)
    {
        if (isEdit)
        {
            AlertSupport.showImageSelector(self, isDevice: false)
            {
                (alertAction) in
                
                Utilities_LoadMedia.load(fromCamType: _LOAD_MEDIA_TYPE.LOAD_MEDIA_TYPE_IMAGE, canEdit: true, delegate: self)
            }
            albumHander:
            {
                (alertAction) in
             
                Utilities_LoadMedia.load(fromAlbumType: _LOAD_MEDIA_TYPE.LOAD_MEDIA_TYPE_IMAGE, canEdit: true, delegate: self)
            }
        }
    }
}

// MARK: - UIImagePickerControllerDelegate

extension MyDetailViewController: UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        NSLog("imagePicker Info = %@",info)
        
        let image:UIImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        
        Utilities.run
        {
            self.headImage.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func  imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - BaseNavigationBarDelegate

extension MyDetailViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        if (isEdit)
        {
            isEdit = false
            
            changeUIStatus()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - UITextFieldDelegate

extension MyDetailViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if (textField == self.textName)
        {
            textField.inputAccessoryView = nil
            
            return true
        }
        else if (textField == self.textGender)
        {
            textName.resignFirstResponder()
            
            textBirthday.resignFirstResponder()
            
            let alertView:UIAlertController = UIAlertController.init(title: MultiLangManager.getLangString(forKey: STR_GENDER), message: "", preferredStyle: UIAlertController.Style.actionSheet)
            
            let actionMale:UIAlertAction = UIAlertAction.init(title: MultiLangManager.getLangString(forKey: STR_MALE), style: UIAlertAction.Style.default)
            {
                (alertAction) in
                
                self.textGender.text = MultiLangManager.getLangString(forKey: STR_MALE)
            }
            
            let actionFemale:UIAlertAction = UIAlertAction.init(title: MultiLangManager.getLangString(forKey: STR_FEMALE), style: UIAlertAction.Style.default)
            {
                (alertAction) in
                
                self.textGender.text = MultiLangManager.getLangString(forKey: STR_FEMALE)
            }
            
            alertView.addAction(actionMale)
            
            alertView.addAction(actionFemale)
            
            self.present(alertView, animated: true, completion: nil)
            
            return false
        }
        else
        {
            textField.inputAccessoryView = inputAccessoryView
            
            return true
        }
    }
}
