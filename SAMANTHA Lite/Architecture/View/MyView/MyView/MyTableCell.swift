//
//  MyTableCell.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MyTableCell: UITableViewCell
{
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var labTitle: UILabel!
    
    override func awakeFromNib()
    {
        setup()
    }
    
    override func draw(_ rect: CGRect)
    {
        self.bkView.layer.cornerRadius = 10
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.08, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool)
    {
        if (highlighted)
        {
            self.bkView.alpha = 0.7
        }
        else
        {
            self.bkView.alpha = 1
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        if (selected)
        {
            self.bkView.alpha = 0.7
        }
        else
        {
            self.bkView.alpha = 1
        }
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupFontSize()
    }
    
    func setupFontSize()
    {
        self.labTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
    }
}
