//
//  MyTableBtnCell.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MyTableBtnCell: UITableViewCell
{
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var labTitle: UILabel!
    @IBOutlet weak var btn: UIButton!
 
    weak var delegate: MyTableBtnCellDelegate?
    
    override func awakeFromNib()
    {
        setup()
    }
    
    override func draw(_ rect: CGRect)
    {
        self.btn.layer.cornerRadius = 10
        
        Utilities.setShadow(self.btn.layer, opacity: 0.08, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool)
    {
        if (highlighted)
        {
            self.bkView.alpha = 0.7
        }
        else
        {
            self.bkView.alpha = 1
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        if (selected)
        {
            self.bkView.alpha = 0.7
        }
        else
        {
            self.bkView.alpha = 1
        }
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupFontSize()
    }
    
    func setupFontSize()
    {
        self.labTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.medium)
    }
    
    @IBAction func logoutClick(_ sender: UIButton)
    {
        self.delegate?.myTableBtnClick()
    }
}

protocol MyTableBtnCellDelegate: class
{
    func myTableBtnClick()
}

extension MyTableBtnCellDelegate
{
    func myTableBtnClick()
    {
        print("myTableBtnClick")
    }
}
