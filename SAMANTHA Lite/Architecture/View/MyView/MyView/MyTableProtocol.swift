//
//  MyTableProtocol.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MyTableProtocol: NSObject, UITableViewDelegate, UITableViewDataSource, EntityProtocol
{
    enum MyTableCellType: Int
    {
        case UserSetting
        case CustomSetting
        case Version
        case Btn
        case Size
    }
    
    weak var delegate: MyTableProtocolDelegate?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height = ((tableView.frame.size.width / 414) * 56)
        
        return height;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return MyTableCellType.Size.rawValue;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == MyTableCellType.Btn.rawValue)
        {
            let cell: MyTableBtnCell = tableView.dequeueReusableCell(withIdentifier: "MyTableBtnCell", for: indexPath) as! MyTableBtnCell
            
            if (getLoginStatus())
            {
                cell.labTitle.text = MultiLangManager.getLangString(forKey: STR_LOGOUT)
            }
            else
            {
                cell.labTitle.text = MultiLangManager.getLangString(forKey: STR_LOGIN)
            }
            
            cell.delegate = self
            
            return cell
        }
        else
        {
            let cell: MyTableCell = tableView.dequeueReusableCell(withIdentifier: "MyTableCell", for: indexPath) as! MyTableCell
            
            switch indexPath.row
            {
                case MyTableCellType.UserSetting.rawValue:
                    
                    cell.labTitle.text = MultiLangManager.getLangString(forKey: STR_MY_PROFILE_SETTING)
                    
                    break
                case MyTableCellType.CustomSetting.rawValue:
                    
                    cell.labTitle.text = MultiLangManager.getLangString(forKey: STR_MY_HOME_PAGE_SETTING)
                    
                    break
                case MyTableCellType.Version.rawValue:
                    
                    cell.labTitle.text =  MultiLangManager.getLangString(forKey: STR_APP_INFO)
                    
                    break
                default:
                    break
            }
            
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool
    {
        if (indexPath.row == MyTableCellType.Btn.rawValue)
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (self.delegate != nil)
        {
            self.delegate?.myTableCellClick(cell: tableView.cellForRow(at: indexPath) as! MyTableCell)
        }
        else
        {
            print("MyTableProtocol Cell Click");
        }
    }
}

// MARK: - MyTableProtocolDelegate

protocol MyTableProtocolDelegate: MyTableBtnCellDelegate
{
    func myTableCellClick(cell: MyTableCell)
}

extension MyTableProtocolDelegate
{
    func myTableCellClick(cell: MyTableCell)
    {
        print("MyTableProtocol Cell Click");
    }
}

// MARK: - MyTableBtnCellDelegate

extension MyTableProtocol: MyTableBtnCellDelegate
{
    func myTableBtnClick()
    {
        self.delegate?.myTableBtnClick()
    }
}
