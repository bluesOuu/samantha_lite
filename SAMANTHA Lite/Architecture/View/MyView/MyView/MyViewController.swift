//
//  MyViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class MyViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var labName: UILabel!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var headImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var myTableProtocol:MyTableProtocol!
    
    let presenter = Presenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        getUserInfo()
        
        self.tableView.reloadData()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
        
        setupTableView()
    }
    
    func setupInitValue()
    {
        
    }
    
    func setupFontSize()
    {
        self.labName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 8)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        
    }
    
    func setupUILayer()
    {
        self.bkView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10
        
        self.headImage.layer.cornerRadius = (self.headImage.frame.height / 2)
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_BAR_MY)
    }
    
    func setupTableView()
    {
        self.myTableProtocol.delegate = self
    }
    
    // MARK: Utilities
    
    func getUserInfo()
    {
        guard let userStruc = presenter.getUserInfo() else
        {
            self.headImage.image = nil

            self.labName.text = ""
            
            return
        }
        
        self.headImage.image = UIImage.init(data: userStruc.imageData)

        self.labName.text = userStruc.userName
    }
}

// MARK: - MyTableProtocolDelegate

extension MyViewController: MyTableProtocolDelegate
{
    func myTableCellClick(cell: MyTableCell)
    {
        print("MyViewController Cell Click");
        
        let indexPath = self.tableView.indexPath(for: cell)
        
        switch indexPath!.row
        {
            case 0:
                
                let indexHomeNavi = Router.sharedInstance.getIndexNavigation()
                
                let mvc: MyDetailViewController = Router.sharedInstance.getMyDetailView()
                
                indexHomeNavi.pushViewController(mvc, animated: true)

                break
            default:
                break
        }
    }
    
    func myTableBtnClick()
    {
        if (getLoginStatus())
        {
            runLogoutStep()
        }
        else
        {
            Router.sharedInstance.gotoLoginView()
        }
        
        self.getUserInfo()
        
        self.tableView.reloadData()
    }
}
