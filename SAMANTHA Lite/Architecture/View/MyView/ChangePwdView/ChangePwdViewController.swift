//
//  ChangePwdViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class ChangePwdViewController: BaseViewController, EntityProtocol
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var labOldTitle: UILabel!
    @IBOutlet weak var labNewTitle: UILabel!
    @IBOutlet weak var labNewCfrmTitle: UILabel!
    @IBOutlet weak var labBtnSave: UILabel!
    @IBOutlet weak var textOld: UITextField!
    @IBOutlet weak var textNew: UITextField!
    @IBOutlet weak var textNewCfrm: UITextField!
    @IBOutlet weak var oldView: UIView!
    @IBOutlet weak var newView: UIView!
    @IBOutlet weak var newCfrmView: UIView!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    let presenter = Presenter.init()
    
    var isEdit:Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
    }
    
    func setupInitValue()
    {
        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap))
        
        self.view.addGestureRecognizer(gesture)
    }
    
    func setupFontSize()
    {
        self.labOldTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        self.labNewTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        self.labNewCfrmTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)
        
        self.textOld.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.medium)
        self.textNew.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.medium)
        self.textNewCfrm.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 0)), weight: UIFont.Weight.medium)
        
        self.labBtnSave.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        self.labBtnSave.text = MultiLangManager.getLangString(forKey: STR_EDIT_CONFIRM)
        
        self.labOldTitle.text = MultiLangManager.getLangString(forKey: STR_OLD_PASSWORD)
        
        self.labNewTitle.text = MultiLangManager.getLangString(forKey: STR_NEW_PASSWORD)
        
        self.labNewCfrmTitle.text = MultiLangManager.getLangString(forKey: STR_NEW_PASSWORD_AGAIN)
    }
    
    func setupUILayer()
    {
        self.bkView.layer.cornerRadius = 10
        self.bkImage.layer.cornerRadius = 10
        self.saveBtn.layer.cornerRadius = 10
        
        self.oldView.layer.cornerRadius = 10
        self.oldView.layer.borderWidth = 1
        self.oldView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        self.newCfrmView.layer.cornerRadius = 10
        self.newCfrmView.layer.borderWidth = 1
        self.newCfrmView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        self.newView.layer.cornerRadius = 10
        self.newView.layer.borderWidth = 1
        self.newView.layer.borderColor = UIColor.init(hexString: COLOR_ED5FFF)?.cgColor
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    func setupNaviBar()
    {
        self.naviBar.delegate = self
        
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.imageLogo.isHidden = true
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_PASSWORD_EDIT)
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
    }
    
    // MARK: Button Events
    
    @IBAction func saveClick(_ sender: UIButton)
    {
        
    }
    
    // MARK: Utilities
    
    @objc func handleTap(recognizer:Any)
    {
        self.textOld.resignFirstResponder()
        
        self.textNew.resignFirstResponder()
        
        self.textNewCfrm.resignFirstResponder()
    }
}

// MARK: - BaseNavigationBarDelegate

extension ChangePwdViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UITextFieldDelegate

extension ChangePwdViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == self.textOld)
        {
            textNew.becomeFirstResponder()
        }
        else if (textField == self.textNew)
        {
            textNewCfrm.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
