//
//  EvaluteViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class EvaluteViewController: BaseViewController
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var labOwner: UILabel!
    @IBOutlet weak var labRecipeName: UILabel!
    @IBOutlet weak var labTime: UILabel!
    @IBOutlet weak var evaluteFrameView: UIView!
    @IBOutlet weak var labInfo1: UILabel!
    @IBOutlet weak var labInfo2: UILabel!
    @IBOutlet weak var labHint: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var labBtn: UILabel!
    @IBOutlet weak var textViewUp: NSLayoutConstraint!
    @IBOutlet weak var textViewDown: NSLayoutConstraint!
    @IBOutlet weak var bottomDown: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: UIButton!
    
    let presenter = EvalutePresenter.init()
    
    var starCount = 3
    
    let defaultBottomContraint:CGFloat = 20
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let btn:UIButton = self.view.viewWithTag(2 + 10) as! UIButton
        
        btn.sendActions(for: UIControl.Event.touchUpInside)
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
        
        setupNaviBar()
        
        setupNotification()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
        
        self.setupInputAccessoryView(#selector(keyboardClose))
        
        self.textView.inputAccessoryView = inputAccessoryView
    }
    
    func setupFontSize()
    {
        self.labOwner.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)

        self.labRecipeName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 2)), weight: UIFont.Weight.medium)

        self.labTime.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.light)
        
        self.labInfo1.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.light)
        
        self.labInfo2.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.light)
        
        self.labHint.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.textView.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.labBtn.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        self.labInfo1.text = MultiLangManager.getLangString(forKey: STR_EVALUTE_INFO1)
        
        self.labInfo2.text = MultiLangManager.getLangString(forKey: STR_EVALUTE_INFO2)
        
        self.labBtn.text = MultiLangManager.getLangString(forKey: STR_EVALUTE_SEND)
    }
    
    func setupNaviBar()
    {
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)

        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.delegate = self
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 4)), weight: UIFont.Weight.semibold)
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_RECENT_EVALUTE)
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
        
        self.naviBar.btnRightBar.isHidden = true
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupUILayer()
    {
        self.bkView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10
        
        self.recipeImage.layer.cornerRadius = 10
        
        self.sendBtn.layer.cornerRadius = 10;
        
        self.evaluteFrameView.layer.cornerRadius = 10
        
        self.evaluteFrameView.layer.borderWidth = 1
        
        self.evaluteFrameView.layer.borderColor = UIColor.init(hexString: COLOR_FF15D6)?.cgColor
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    // MARK: - Button Events
    
    @IBAction func starClick(_ sender: UIButton)
    {
        for i in 0...4
        {
            let btn:UIButton = self.view.viewWithTag(i + 10) as! UIButton
            
            if (btn.tag <= sender.tag)
            {
                btn.isSelected = true
            }
            else
            {
                btn.isSelected = false
            }
        }
        
        let key = String(format: "STR_EVALUTE_HINT%d", (sender.tag - 9))
        
        self.labHint.text = MultiLangManager.getLangString(forKey: key)
    }
    
    @IBAction func sendClick(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Notification
    
    @objc func keyboardWillShow(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double

        let keyboardFrame:CGRect = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        
        let keyboardHeight = keyboardFrame.height
        
        let newY = (self.view.frame.maxY - keyboardHeight)
        
        let newContraint = abs((newY - self.evaluteFrameView.frame.maxY))
        
        self.textViewUp.priority = UILayoutPriority.defaultHigh

        self.textViewDown.priority = UILayoutPriority.defaultLow
        
        self.bottomDown.constant = (newContraint + defaultBottomContraint + 25)
        
        UIView.animate(withDuration: duration)
        {
            self.labHint.isHidden = true
            
            self.labInfo2.isHidden = true

            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification)
    {
        let duration:Double = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double

        self.textViewUp.priority = UILayoutPriority.defaultLow

        self.textViewDown.priority = UILayoutPriority.defaultHigh

        self.bottomDown.constant = defaultBottomContraint
        
        UIView.animate(withDuration: duration)
        {
            if (self.textView.text.count > 0)
            {
                self.labHint.isHidden = true
            }
            else
            {
                self.labHint.isHidden = false
            }
            
            self.labInfo2.isHidden = false

            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardClose()
    {
        self.textView.resignFirstResponder()
    }
}

// MARK: - BaseNavigationBarDelegate

extension EvaluteViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        if (self.textView.isFirstResponder)
        {
            self.textView.resignFirstResponder()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - EvalutePresenterDelegate

extension EvaluteViewController: EvalutePresenterDelegate
{
    
}

// MARK: - UITextViewDelegate

extension EvaluteViewController: UITextViewDelegate
{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        print("textViewShouldBeginEditing")
        
        return true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool
    {
        print("textViewShouldEndEditing")
        
        return true
    }
}
