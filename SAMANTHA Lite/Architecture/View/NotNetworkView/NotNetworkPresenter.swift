//
//  NotNetworkPresenter.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/14.
//

import Foundation

class NotNetworkPresenter: NSObject
{
    weak var delegate: NotNetworkPresenterDelegate?
    
    override init()
    {
        super.init()
    }
}

protocol NotNetworkPresenterDelegate: class
{
    
}

extension NotNetworkPresenterDelegate
{
    
}
