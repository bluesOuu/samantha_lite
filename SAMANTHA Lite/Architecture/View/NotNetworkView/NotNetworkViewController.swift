//
//  NotNetworkViewCoontroller.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/14.
//

import Foundation

class NotNetworkViewController: BaseViewController
{
    // MARK: - IBOutlet
    
    @IBOutlet weak var labBtn: UILabel!
    @IBOutlet weak var labInfo1: UILabel!
    @IBOutlet weak var labInfo2: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    
    // MARK: - Variable
    
    weak var delegate: NotNetworkViewControllerDelegate?
    
    let presenter = NotNetworkPresenter.init()
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        setupUILayer()
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupFontSize()
        
        setupLocalized()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
    }
    
    func setupFontSize()
    {
        self.labInfo1.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 10)), weight: UIFont.Weight.bold)
        
        self.labInfo2.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.light)
        
        self.labBtn.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 1)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        self.labInfo1.text = MultiLangManager.getLangString(forKey: STR_OOPS)
        
        self.labInfo2.text = MultiLangManager.getLangString(forKey: STR_NET_ERROR_INFO)
        
        self.labBtn.text = MultiLangManager.getLangString(forKey: STR_RETRY)
    }
    
    func setupUILayer()
    {
        self.loginBtn.layer.cornerRadius = 10
    }
    
    // MARK: - Button Events
    
    @IBAction func loginClick(_ sender: UIButton)
    {
        if (self.delegate != nil)
        {
            self.delegate?.NotNetworkViewClick(nnvc: self)
        }
        else
        {
            print("NotNetworkClick")
        }
    }
}

protocol NotNetworkViewControllerDelegate: class
{
    func NotNetworkViewClick(nnvc: NotNetworkViewController)
}

extension NotNetworkViewControllerDelegate
{
    func NotNetworkViewClick(nnvc: NotNetworkViewController)
    {
        print("NotNetworkClick")
    }
}

extension NotNetworkViewController: NotNetworkPresenterDelegate
{
    
}
