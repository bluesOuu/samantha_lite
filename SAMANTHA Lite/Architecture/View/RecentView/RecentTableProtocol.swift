//
//  RecentTableProtocol.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class RecentTableProtocol: NSObject, UITableViewDelegate, UITableViewDataSource
{
    weak var delegate: RecentTableProtocolDelegate?
    
    weak var presenter: RecentPresenter?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let height = ((tableView.frame.size.width / 414) * 172)
        
        return height;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return presenter!.recentList.count;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: RecentTableCell = tableView.dequeueReusableCell(withIdentifier: "RecentTableCell", for: indexPath) as! RecentTableCell
        
        cell.delegate = self
        
        cell.evaluteView.isHidden = ((indexPath.row % 2) != 0)
        
        cell.starView.isHidden = !(((indexPath.row % 2) != 0))
        
        cell.favoriteImage.isHighlighted = ((indexPath.row % 2) != 0)
        
        cell.starWith(index: (indexPath.row % 6))
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (self.delegate != nil)
        {
            self.delegate?.recentTableCellClick(cell: tableView.cellForRow(at: indexPath) as! RecentTableCell)
        }
        else
        {
            print("RecentTableProtocol Cell Click");
        }
    }
}

protocol RecentTableProtocolDelegate: RecentTableCellDelegate
{
    func recentTableCellClick(cell: RecentTableCell)
}

extension RecentTableCellDelegate
{
    func recentTableCellClick(cell: RecentTableCell)
    {
        print("RecentTableProtocol Cell Click");
    }
}

// MARK: - RecentPresenterDelegate

extension RecentTableProtocol: RecentTableCellDelegate
{
    func recentTableCellFavoriteClick(cell: RecentTableCell)
    {
        if (self.delegate != nil)
        {
            self.delegate?.recentTableCellFavoriteClick(cell: cell)
        }
        else
        {
            print("RecentTableProtocol Favorite Click");
        }
    }
    
    func recentTableCellEvaluteClick(cell: RecentTableCell)
    {
        if (self.delegate != nil)
        {
            self.delegate?.recentTableCellEvaluteClick(cell: cell)
        }
        else
        {
            print("RecentTableProtocol Evalute Click");
        }
    }
    
    func recentTableCellAgainClick(cell: RecentTableCell)
    {
        if (self.delegate != nil)
        {
            self.delegate?.recentTableCellAgainClick(cell: cell)
        }
        else
        {
            print("RecentTableProtocol Again Click");
        }
    }
}
