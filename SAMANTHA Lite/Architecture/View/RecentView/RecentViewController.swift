//
//  RecentViewController.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class RecentViewController: BaseViewController
{
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var naviBar: BaseNavigationBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var recentTableProtocol: RecentTableProtocol!

    let presenter = RecentPresenter.init()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //TODO: Show Alert
        
//        if (Entity.sharedInstance.getLoginStatus())
//        {
            presenter.getRecentList()
//        }
//        else
//        {
//            showNotLoginView()
//        }
    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupInitValue()
        
        setupNaviBar()
        
        setupTableView()
    }
    
    func setupInitValue()
    {
        presenter.delegate = self
    }
    
    func setupNaviBar()
    {
        self.topView.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)

        self.naviBar.backgroundColor = UIColor.init(hexString: COLOR_3E3D4A)
        
        self.naviBar.delegate = self
        
        self.naviBar.labTitle.isHidden = false
        
        self.naviBar.labTitle.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 4)), weight: UIFont.Weight.semibold)
        
        self.naviBar.labTitle.text = MultiLangManager.getLangString(forKey: STR_HOME_RECORD)
        
        self.naviBar.btnLeftBar.isHidden = false
        
        self.naviBar.btnLeftBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnLeftBar.setImage(UIImage.init(named: "icon_back_white"), for: UIControl.State.normal)
        
        self.naviBar.btnRightBar.isHidden = false
        
        self.naviBar.btnRightBar.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        self.naviBar.btnRightBar.setImage(UIImage.init(named: "icon_search_white"), for: UIControl.State.normal)
    }
    
    func setupTableView()
    {
        self.recentTableProtocol.presenter = presenter
        
        self.recentTableProtocol.delegate = self
    }
    
    func showNotLoginView()
    {
        let nlvc:NotLoginViewController = Router.sharedInstance.getNotLoginView()

        self.addChild(nlvc)

        nlvc.delegate = self

        nlvc.view.frame = self.tableView.frame

        self.tableView.addSubview(nlvc.view)
    }
}

// MARK: - BaseNavigationBarDelegate

extension RecentViewController: BaseNavigationBarDelegate
{
    @objc func baseNavigationBarLeftBtnClick()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func baseNavigationBarRightBtnClick()
    {
        
    }
}

// MARK: - RecentPresenterDelegate

extension RecentViewController: RecentPresenterDelegate
{
    func onGetRecentListDone()
    {
        //TODO: hide alert
        
        self.tableView.reloadData()
        
        if (presenter.recentList.count == 0)
        {
            
        }
    }
}

// MARK: - NotLoginViewControllerDelegate

extension RecentViewController: NotLoginViewControllerDelegate
{
    func notLoginViewClick(nlvc: NotLoginViewController)
    {
        nlvc.view.removeFromSuperview()
        
        nlvc.removeFromParent()
        
        self.tableView.isHidden = false
        
        Router.sharedInstance.gotoLoginView()
    }
}

// MARK: - RecentTableProtocolDelegate

extension RecentViewController: RecentTableProtocolDelegate
{
    func recentTableCellClick(cell: RecentTableCell)
    {
        print("RecentView Cell Click");
    }
    
    func recentTableCellFavoriteClick(cell: RecentTableCell)
    {
        print("RecentView Favorite Click");
    }
    
    func recentTableCellEvaluteClick(cell: RecentTableCell)
    {
        print("RecentView Evalute Click");
        
        let evc: EvaluteViewController = Router.sharedInstance.getEvaluteView()
        
        self.navigationController?.pushViewController(evc, animated: true)
    }
    
    func recentTableCellAgainClick(cell: RecentTableCell)
    {
        print("RecentView Again Click");
    }
}
