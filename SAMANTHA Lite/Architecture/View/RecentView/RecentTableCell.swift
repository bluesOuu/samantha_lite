//
//  RecentTableCell.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class RecentTableCell: UITableViewCell
{
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var bkImage: UIImageView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var favoriteImage: UIImageView!
    
    @IBOutlet weak var labOwner: UILabel!
    @IBOutlet weak var labRecipeName: UILabel!
    @IBOutlet weak var labTime: UILabel!
    @IBOutlet weak var labBtnEvalute: UILabel!
    @IBOutlet weak var labBtnAgain: UILabel!
    
    @IBOutlet weak var evaluteView: UIView!
    @IBOutlet weak var againView: UIView!
    @IBOutlet weak var starView: UIView!
    
    weak var delegate: RecentTableCellDelegate?
    
    override func awakeFromNib()
    {
        setup()
    }
    
    override func draw(_ rect: CGRect)
    {
        self.bkView.layer.cornerRadius = 10
        
        self.bkImage.layer.cornerRadius = 10
        
        self.recipeImage.layer.cornerRadius = 10
        
        self.againView.layer.cornerRadius = 10;
        
        self.evaluteView.layer.cornerRadius = 10
        
        self.evaluteView.layer.borderWidth = 1
        
        self.evaluteView.layer.borderColor = UIColor.init(white: 1, alpha: 1).cgColor
        
        self.starView.layer.cornerRadius = 10
        
        self.starView.layer.borderWidth = 1
        
        self.starView.layer.borderColor = UIColor.init(white: 1, alpha: 0.18).cgColor
        
        Utilities.setShadow(self.bkView.layer, opacity: 0.18, radius: 8, offset: CGSize.init(width: 6, height: 8), color: UIColor.black)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool)
    {
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {

    }
    
    // MARK: - Setup
    
    func setup()
    {
        setupFontSize()
        
        setupLocalized()
        
        setupTap()
    }
    
    func setupFontSize()
    {
        self.labOwner.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.medium)

        self.labRecipeName.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() + 2)), weight: UIFont.Weight.medium)

        self.labTime.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 4)), weight: UIFont.Weight.light)
        
        self.labBtnEvalute.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.bold)
        
        self.labBtnAgain.font = UIFont.systemFont(ofSize: CGFloat((Utilities.getFontSize() - 2)), weight: UIFont.Weight.bold)
    }
    
    func setupLocalized()
    {
        self.labBtnEvalute.text = MultiLangManager.getLangString(forKey: STR_RECENT_EVALUTE)
        
        self.labBtnAgain.text = MultiLangManager.getLangString(forKey: STR_RECENT_BREW_AGAIN)
    }
    
    func setupTap()
    {
        let gesture:UITapGestureRecognizer =  UITapGestureRecognizer.init(target: self, action: #selector(handleTap(recognizer:)))
        
        self.favoriteImage.addGestureRecognizer(gesture)
    }
    
    // MARK: - Utilities
    
    @objc func handleTap(recognizer:UITapGestureRecognizer)
    {
        if (self.delegate != nil)
        {
            self.delegate?.recentTableCellFavoriteClick(cell: self)
        }
        else
        {
            print("RecentTableCell Favorite Click");
        }
    }
    
    func starWith(index: Int)
    {
        for i in 0...4
        {
            let starImage:UIImageView = self.viewWithTag(i + 10) as! UIImageView
            
            if (i < index)
            {
                starImage.isHighlighted = true
            }
            else
            {
                starImage.isHighlighted = false
            }
        }
        
    }
    
    // MARK: - Button Events
    
    @IBAction func againClick(_ sender: UIButton)
    {
        self.delegate?.recentTableCellAgainClick(cell: self)
    }
    
    @IBAction func evaluteClick(_ sender: UIButton)
    {
        self.delegate?.recentTableCellEvaluteClick(cell: self)
    }
}

protocol RecentTableCellDelegate: class
{
    func recentTableCellFavoriteClick(cell: RecentTableCell)
    func recentTableCellEvaluteClick(cell: RecentTableCell)
    func recentTableCellAgainClick(cell: RecentTableCell)
}

extension RecentTableCellDelegate
{
    func recentTableCellFavoriteClick(cell: RecentTableCell)
    {
        print("RecentTableCell Favorite Click");
    }
    
    func recentTableCellEvaluteClick(cell: RecentTableCell)
    {
        print("RecentTableCell Evalute Click");
    }
    
    func recentTableCellAgainClick(cell: RecentTableCell)
    {
        print("RecentTableCell Again Click");
    }
}
