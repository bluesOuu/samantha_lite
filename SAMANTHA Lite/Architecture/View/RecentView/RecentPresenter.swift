//
//  RecentPresenter.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/24.
//

import Foundation

class RecentPresenter: NSObject
{
    weak var delegate: RecentPresenterDelegate?
    
    var recentList:Array<SamanthaRecipeStruc> = []
    
    override init()
    {
        super.init()
    }
    
    func getRecentList()
    {
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        recentList.append(SamanthaRecipeStruc())
        
        self.delegate?.onGetRecentListDone()
    }
}

//MARK: - RecentPresenterDelegate

protocol RecentPresenterDelegate: class
{
    func onGetRecentListDone()
}

extension RecentPresenterDelegate
{
    func onGetRecentListDone()
    {
        print("onGetRecentListDone")
    }
}
