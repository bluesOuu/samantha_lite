//
//  AppDelegate.swift
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

import UIKit

@main

class AppDelegate: UIResponder, UIApplicationDelegate
{
    static let sharedInstance:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        SamanthaManager.sharedInstance()
        
        Router.sharedInstance.showSplashView()
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication)
    {
        SamanthaManager.sharedInstance().sendCmdStartScan()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        SamanthaManager.sharedInstance().sendCmdStopScan()
    }
}

