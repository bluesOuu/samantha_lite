//
//  SAMANTHA Lite-Bridging-Header.h
//  SAMANTHA Lite
//
//  Created by Blues on 2020/11/18.
//

#ifndef SAMANTHA_Lite_Bridging_Header_h
#define SAMANTHA_Lite_Bridging_Header_h


#endif /* SAMANTHA_Lite_Bridging_Header_h */

#import <CommonCrypto/CommonCrypto.h>
#import "Utilities.h"
#import "BOUserInfo.h"
#import "BaseToolBar.h"
#import "BaseNavigationBar.h"
#import "UIColor+Expanded.h"
#import "SocailMediaManager.h"
#import "MultiLangManager.h"
#import "StringDefine.h"
#import "LineLayout.h"
#import "CollectRecordProtocol.h"
#import "CelebrityProtocol.h"
#import "CahamptionProtocol.h"
#import "AlertScanQRCodeView.h"
#import "AlertSupport.h"
#import "NSMutableAttributedString+SettingMethod.h"
#import "NSString+Convert.h"
#import "AlertProcessView.h"
#import "SSBKeychain.h"
#import "HiroiaManager.h"
#import "SamanthaManager.h"
#import "UserStruc.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SamanthaRecipeStruc.h"
#import "BaseHriManager.h"
#import "HiroiaCloudManager.h"
#import "FaceApiCloudManager.h"
#import "FaceStruc.h"
#import "HwcCloudManager.h"
#import "BaseViewController.h"
#import "BaseNavigationController.h"
#import "FaceDetectionManager.h"
#import "YahooWeatherAPI.h"
#import "BOGPSManager.h"
#import "MoodRecipeProtocol.h"
#import "ChosenRecipeProtocol.h"
#import "DeviceStatusView.h"
#import "DeviceScanView.h"
#import "DeviceTableProtocol.h"
#import "DeviceDoneView.h"
#import "AlertLabelView.h"
#import "AlertTextView.h"

#define SCAN_TIMEOUT        10

#define COLOR_CECECE                    @"0xCECECE"
#define COLOR_FF15D6                    @"0xFF15D6"
#define COLOR_3E3D4A                    @"0x3E3D4A"
#define COLOR_ED5FFF                    @"0xED5FFF"
#define COLOR_RECIPE_1                  @"0xFFF840"
#define COLOR_RECIPE_2                  @"0x4DFFB4"
#define COLOR_RECIPE_3                  @"0xFFCF83"
#define COLOR_CHART                     @"0xED5FFF"
#define COLOR_CONNECT                   @"0x4AFF58"

#define KEYCHAIN_ACCESS_GROUPS          @"S4D6CM849V.com.hiroia.samantha.lite"

#define SAVE_SMT_MAC                    @"SAVE_SMT_MAC"
#define SAVE_SMT_PWD                    @"SAVE_SMT_PWD"

#define SMT_DIS_CONNECT                 @"SMT_DIS_CONNECT"
#define SMT_P5_CALLBACK                 @"SMT_P5_CALLBACK"

#define LOGIN_STATUS                    @"LOGIN_STATUS"

#define KEYCHAIN_USER_AUTH_TOKEN        @"KEYCHAIN_USER_AUTH_TOKEN"
#define KEYCHAIN_USER_UUID              @"KEYCHAIN_USER_UUID"

