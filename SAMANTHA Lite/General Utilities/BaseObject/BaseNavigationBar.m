//
//  BaseNavigationBar.m
//  12Ours
//
//  Created by Blues on 2017/6/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "BaseNavigationBar.h"

@interface BaseNavigationBar()

@property (strong, nonatomic) IBOutlet UIView *view;

@end


@implementation BaseNavigationBar
@synthesize delegate;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (void)loadXIB
{
    if ([self.view isDescendantOfView:self])
        return;
    
    self.view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    
    self.clipsToBounds = YES;
    
    [self addSubview:self.view];
}

- (void)drawRect:(CGRect)rect
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:@[
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0.0] ,
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeLeading
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeading
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeTrailing
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeTrailing
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0.0]
                           ]];
    
    [self layoutIfNeeded];
}

#pragma mark - Button Events

- (IBAction)buttonLeftClick:(id)sender
{
//    if (self.imageLeftBar.hidden)
//    {
//        return;
//    }
    
    if ([delegate respondsToSelector:@selector(baseNavigationBarLeftBtnClick)])
        [delegate baseNavigationBarLeftBtnClick];
}

- (IBAction)buttonRightClick:(id)sender
{
//    if (self.imageRightBar.hidden)
//    {
//        return;
//    }
    
    if ([delegate respondsToSelector:@selector(baseNavigationBarRightBtnClick)])
        [delegate baseNavigationBarRightBtnClick];
}

@end
