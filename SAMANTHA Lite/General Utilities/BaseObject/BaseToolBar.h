//
//  BaseToolBar.h
//  HWC
//
//  Created by Blues on 2017/6/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LAB_TAG_INIT_VALUE 40
#define IMG_TAG_INIT_VALUE 10
#define BTN_TAG_INIT_VALUE 20
#define VIEW_TAG_INIT_VALUE 30

@protocol BaseToolBarDelegate <NSObject>

- (void)baseToolBarBtnClick:(NSInteger)tag;

@end

@interface BaseToolBar : UIView

- (void)setTabLabelWithTag:(NSInteger)tag Text:(NSString *)text;
- (void)setTabImgWithTag:(NSInteger)tag ImageName:(NSString *)imageName;
//- (void)setTabBtnWithTag:(NSInteger)tag Action:(SEL)action Target:(id)target Events:(UIControlEvents)controlEvents;
//- (void)setEnableBtnWithTag:(NSInteger)tag Enable:(BOOL)enable;

@property (weak, nonatomic) id<BaseToolBarDelegate> delegate;

@end
