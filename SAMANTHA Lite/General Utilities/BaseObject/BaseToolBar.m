//
//  BaseNavigationBar.m
//  12Ours
//
//  Created by Blues on 2017/6/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "BaseToolBar.h"

@interface BaseToolBar()

@property (strong, nonatomic) IBOutlet UIView *view;

@end


@implementation BaseToolBar
@synthesize delegate;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self loadXIB];
    }
    
    return self;
}

- (void)loadXIB
{
    if ([self.view isDescendantOfView:self])
        return;
    
    self.view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    
    self.view.clipsToBounds = NO;
    
    self.clipsToBounds = NO;
    
    [self addSubview:self.view];
}

- (void)drawRect:(CGRect)rect
{
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:@[
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                         constant:0.0] ,
                           [NSLayoutConstraint constraintWithItem:self.view
                                                        attribute:NSLayoutAttributeLeading
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self
                                                        attribute:NSLayoutAttributeLeading
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeTrailing
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeTrailing
                                                       multiplier:1
                                                         constant:0.0],
                           [NSLayoutConstraint constraintWithItem:self
                                                        attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view
                                                        attribute:NSLayoutAttributeBottom
                                                       multiplier:1.0
                                                         constant:0.0]
                           ]];
    
    [self layoutIfNeeded];
}

#pragma mark - Utilities

- (void)setTabLabelWithTag:(NSInteger)tag Text:(NSString *)text
{
    UILabel *lab = [self viewWithTag:(LAB_TAG_INIT_VALUE + tag)];

    [lab setText:text];
    
    [lab setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 6) weight:UIFontWeightMedium]];

    [lab setTextColor:[UIColor colorWithHexString:COLOR_CECECE]];
}

//- (void)setTabImgWithTag:(NSInteger)tag Image:(UIImage *)image
//{
//    UIButton *btn = [self viewWithTag:(BTN_TAG_INIT_VALUE + tag)];
//
//    [btn setImage:image forState:UIControlStateNormal];
//
//    CGFloat btnHeight = CGRectGetHeight(btn.frame);
//
//    [btn setImageEdgeInsets:UIEdgeInsetsMake(- (btnHeight / 2.5), 0, 0, -btn.titleLabel.intrinsicContentSize.width)];
//
//    [btn setTitleEdgeInsets:UIEdgeInsetsMake((btnHeight / 1.5), -CGRectGetWidth(btn.imageView.frame), 0, 0)];
//}

- (void)setTabImgWithTag:(NSInteger)tag ImageName:(NSString *)imageName
{
    UIImageView *imageView = [self viewWithTag:(IMG_TAG_INIT_VALUE + tag)];
    
    [imageView setImage:[UIImage imageNamed:imageName]];
    
    [imageView setHighlightedImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_hover", imageName]]];
}

//- (void)setTabBtnWithTag:(NSInteger)tag Action:(SEL)action Target:(id)target Events:(UIControlEvents)controlEvents
//{
//    UIButton *btn = [self viewWithTag:(BTN_TAG_INIT_VALUE + tag)];
//
//    [btn removeTarget:target action:action forControlEvents:controlEvents];
//
//    [btn addTarget:target action:action forControlEvents:controlEvents];
//}
//
//- (void)setEnableBtnWithTag:(NSInteger)tag Enable:(BOOL)enable
//{
//    UIButton *btn = [self viewWithTag:(BTN_TAG_INIT_VALUE + tag)];
//
////    UILabel *lab = [self viewWithTag:(LAB_TAG_INIT_VALUE + tag)];
//
//    btn.enabled = enable;
//
//    if (enable)
//    {
////        [lab setTextColor:[UIColor colorWithHexString:COLOR_333333]];
//    }
//    else
//    {
////        [lab setTextColor:[UIColor colorWithHexString:COLOR_B3B3B3]];
//    }
//}

#pragma mark - Button Events

- (IBAction)buttonClick:(id)sender
{
    UIButton *tempBtn = sender;
    
    NSInteger tag = (tempBtn.tag - BTN_TAG_INIT_VALUE);
    
    for (int i = 0 ; i < 5 ; i++)
    {
        UIImageView *imageView = [self viewWithTag:(i + IMG_TAG_INIT_VALUE)];
        
        UILabel *lab = [self viewWithTag:(i + LAB_TAG_INIT_VALUE)];
        
        UIButton *btn = [self viewWithTag:(BTN_TAG_INIT_VALUE + i)];
        
        if ((tag + IMG_TAG_INIT_VALUE) == imageView.tag)
        {
            imageView.highlighted = YES;
            
            [lab setTextColor:[UIColor colorWithHexString:COLOR_FF15D6]];
        }
        else
        {
            imageView.highlighted = NO;
            
//            if (btn.enabled)
//            {
                [lab setTextColor:[UIColor colorWithHexString:COLOR_CECECE]];
//            }
//            else
//            {
//                [lab setTextColor:[UIColor colorWithHexString:COLOR_B3B3B3]];
//            }
        }
    }
    
    if ([delegate respondsToSelector:@selector(baseToolBarBtnClick:)])
        [delegate baseToolBarBtnClick:tag];
}

@end
