//
//  BaseNavigationBar.h
//  12Ours
//
//  Created by Blues on 2017/6/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BaseNavigationBarDelegate <NSObject>
@optional
- (void)baseNavigationBarLeftBtnClick;
- (void)baseNavigationBarRightBtnClick;

@end

@interface BaseNavigationBar : UIView

@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
//@property (weak, nonatomic) IBOutlet UIImageView *imageLeftBar;
//@property (weak, nonatomic) IBOutlet UIImageView *imageRightBar;
@property (weak, nonatomic) IBOutlet UIButton *btnLeftBar;
@property (weak, nonatomic) IBOutlet UIButton *btnRightBar;

@property (weak, nonatomic) id<BaseNavigationBarDelegate> delegate;

@end
