//
//  YahooWeatherAPI.h
//  weather-api-demo-objc
//
//  Copyright 2019 Oath Inc. Licensed under the terms of the zLib license see https://opensource.org/licenses/Zlib for terms.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YahooWeatherAPI : NSObject
-(nonnull instancetype) init NS_UNAVAILABLE;

/**
 Shared instance of the YahooWeatherAPI class. Make your calls on this object: [YahooWeatherAPI.shared weatherForLocation ...]

 @return Shared instance of YahooWeatherAPI
 */
+ (YahooWeatherAPI*)shared;

/**
 Returns weather data by location in a format and unit you specify.

 @param location The location you want weather for, i.e. 'sunnyvale,ca'
 @param format Response format. json or xml, default is json
 @param unit Units returned. imperial or metric, default is imperial
 @param failure Failure callback
 @param success Success callback
 */
-(void)weatherForLocation:(NSString *) location format:(NSString* _Nullable)format unit:(NSString* _Nullable) unit failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData* _Nullable data)) success;

/**
 Returns weather data by woeid (Where on Earth ID) in a format and unit you specify

 @param woeid The woeid for the location
 @param format Response format. json or xml, default is json
 @param unit Units returned. imperial or metric, default is imperial
 @param failure Failure callback
 @param success Success callback
 */
-(void)weatherForWoeId:(NSString* _Nonnull) woeid format:(NSString* _Nullable)format unit:(NSString* _Nullable) unit failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData* _Nullable data)) success;



/**
 Returns weather data for a given latitude and longitude

 @param lat Latitude of the location
 @param lon Longitude of the location
 @param format Response format. json or xml, default is json
 @param unit Units returned. imperial or metric, default is imperial
 @param failure Failure callback
 @param success Success callback
 */
-(void)weatherForLat:(NSString* _Nonnull) lat lon:(NSString* _Nonnull) lon format:(NSString* _Nullable)format unit:(NSString* _Nullable) unit failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData* _Nullable data)) success;

@end

NS_ASSUME_NONNULL_END