//
//  YahooWeatherAPI.m
//  weather-api-demo-objc
//
//  Copyright 2019 Oath Inc. Licensed under the terms of the zLib license see https://opensource.org/licenses/Zlib for terms.
//

#import "YahooWeatherAPI.h"
/*
See https://github.com/yahoo/TDOAuth for information on adding this library to your project.
*/
#import <TDOAuth/TDOAuth.h>

@interface YahooWeatherAPI()

@property (nonatomic) NSString *appId;
@property (nonatomic) NSString *consumerKey;
@property (nonatomic) NSString *consumerSecret;

@end

@implementation YahooWeatherAPI

+ (YahooWeatherAPI*)shared
{
    static YahooWeatherAPI *_shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [YahooWeatherAPI new];
    });
    
    return _shared;
}

-(instancetype)init
{
    self = [super init];
    if(self) {
        // Fill these with your own values.
        self.appId = @"gzSCQurw";
        self.consumerKey = @"dj0yJmk9NDZZYnhxTEFXVEdvJmQ9WVdrOVozcFRRMUYxY25jbWNHbzlNQT09JnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTM0";
        self.consumerSecret = @"9d1e04351795122bf073bdfdc05a543cc6cab867";
    }
    return self;
}

-(void)weatherForLocation:(NSString* _Nonnull) location format:(NSString* _Nullable)format unit:(NSString* _Nullable) unit failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData*  _Nullable data)) success
{
    NSMutableURLRequest *request = [self requestWithParameters:@{@"location":location, @"format":format?:@"json", @"u":unit?:@"f"}];
    [self makeRequest:request failure:failure success:success];
}

-(void)weatherForWoeId:(NSString* _Nonnull) woeid format:(NSString* _Nullable)format unit:(NSString* _Nullable) unit failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData*  _Nullable data)) success
{
    NSMutableURLRequest *request = [self requestWithParameters:@{@"woeid":woeid, @"format":format?:@"json", @"u":unit?:@"f"}];
    [self makeRequest:request failure:failure success:success];
}

-(void)weatherForLat:(NSString* _Nonnull) lat lon:(NSString* _Nonnull) lon format:(NSString* _Nullable)format unit:(NSString* _Nullable) unit failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData*  _Nullable data)) success
{
    NSMutableURLRequest *request = [self requestWithParameters:@{@"lat":lat, @"lon":lon, @"format":format?:@"json", @"u":unit?:@"f"}];
    [self makeRequest:request failure:failure success:success];
}

-(NSMutableURLRequest *)requestWithParameters:(NSDictionary *)parameters
{
    NSMutableURLRequest *request = [[TDOAuth URLRequestForPath:@"/forecastrss"
                                                 GETParameters:parameters
                                                        scheme:@"https"
                                                          host:@"weather-ydn-yql.media.yahoo.com"
                                                   consumerKey:self.consumerKey
                                                consumerSecret:self.consumerSecret
                                                   accessToken:nil
                                                   tokenSecret:nil] mutableCopy];
    [request addValue:self.appId forHTTPHeaderField:self.appId];
    
    return request;
}

-(void)makeRequest:(NSMutableURLRequest*) request failure:(void(^)(NSError * _Nullable error)) failure success:(void(^)(NSData* data)) success
{
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error != nil) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if(failure != nil) {
                    failure(error);
                }
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if(success != nil) {
                    success(data);
                }
            });
        }
    }] resume];
}

@end
