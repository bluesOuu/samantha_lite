//
//  BOGPSManager.h
//  BO GPS Manager
//
//  Created by Blues on 2017/7/20.
//  Copyright © 2017年 BOGPSManager. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define GPS_AUTHORIZATION @"GPS_AUTHORIZATION"

//#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
//if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))

typedef enum
{
    GPS_CODE_SUCESS,
    GPS_CODE_ERROR_AUTH_NOT_YET,
    GPS_CODE_ERROR_SYS_OFF,
    GPS_CODE_ERROR_AUTH_OFF,
    
} _GPS_RETURN_CODE;

@protocol BOGPSManagerDelegate <NSObject>

- (void)updateGPSInfo:(CLLocation *)cLLocation;

@optional
- (void)gpsAuthorizationDenied;

@end

@interface BOGPSManager : NSObject<CLLocationManagerDelegate>
{
    _GPS_RETURN_CODE returnCode;
}

@property (strong, nonatomic) id<BOGPSManagerDelegate> delegate;

@property (strong, nonatomic) CLLocationManager *mLocationManager;

+ (id)sharedInstance;

- (_GPS_RETURN_CODE)startLocationUpdate;

- (void)stopLocationUpdate;

@end
