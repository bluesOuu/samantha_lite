//
//  BOGPSManager.m
//  BO GPS Manager
//
//  Created by Blues on 2017/7/20.
//  Copyright © 2017年 BOGPSManager. All rights reserved.
//

#import "BOGPSManager.h"

@implementation BOGPSManager
@synthesize mLocationManager;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        mLocationManager = [[CLLocationManager alloc] init];
        
        mLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        mLocationManager.distanceFilter = 10;
        
        mLocationManager.delegate = self;
    }
    
    return self;
}

+ (id)sharedInstance
{
    static BOGPSManager *gpsManager;
    
    static dispatch_once_t singletonToken;
    
    dispatch_once(&singletonToken,^
    {
        gpsManager = [[self alloc] init];
    });
    
    return gpsManager;
}

- (_GPS_RETURN_CODE)startLocationUpdate
{
    if ([CLLocationManager locationServicesEnabled])
    {
        [mLocationManager requestAlwaysAuthorization];
        
        [mLocationManager requestWhenInUseAuthorization];
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
        {
            return GPS_CODE_ERROR_AUTH_NOT_YET;
        }
        else if ((kCLAuthorizationStatusAuthorizedWhenInUse == [CLLocationManager authorizationStatus]) || (kCLAuthorizationStatusAuthorizedAlways == [CLLocationManager authorizationStatus]))
        {
            [mLocationManager setAllowsBackgroundLocationUpdates:NO];
            
            [mLocationManager startUpdatingLocation];

            return GPS_CODE_SUCESS;
        }
        else
        {
            return GPS_CODE_ERROR_AUTH_OFF;
        }
    }
    else
    {
        return GPS_CODE_ERROR_SYS_OFF;
    }
}

- (void)stopLocationUpdate
{
    [mLocationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *latestLocation = (CLLocation *) [locations lastObject];
    
    if ([self.delegate respondsToSelector:@selector(updateGPSInfo:)])
    {
        [self.delegate updateGPSInfo:latestLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [mLocationManager stopUpdatingLocation];

    [mLocationManager performSelector:@selector(startUpdatingLocation) withObject:nil afterDelay:1];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ((kCLAuthorizationStatusAuthorizedWhenInUse == status) || (kCLAuthorizationStatusAuthorizedAlways == status))
    {
        [mLocationManager startUpdatingLocation];
    }
    else if (status == kCLAuthorizationStatusNotDetermined)
    {
        [mLocationManager requestAlwaysAuthorization];
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(gpsAuthorizationDenied)])
            [self.delegate gpsAuthorizationDenied];
    }
}

@end
