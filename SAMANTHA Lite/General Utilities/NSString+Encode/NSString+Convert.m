//
//  NSString+DateSort.m
//  12Ours
//
//  Created by Blues on 2018/6/26.
//  Copyright © 2018年 12Ours. All rights reserved.
//

#import "NSString+Convert.h"

@implementation NSString (Convert)

- (NSInteger)convertToDecWithHex
{
    NSMutableArray *ary = [[NSMutableArray alloc]init];
    
    for(int i = 0; i < [self length]; i++)
    {
        unichar hex_char1 = [self characterAtIndex:i];
        
        int int_ch1;
        
        if(hex_char1 >= '0' && hex_char1 <='9')
            int_ch1 = (hex_char1 - 48);                   // 0 Ascll - 48
        else if(hex_char1 >= 'A' && hex_char1 <='F')
            int_ch1 = (hex_char1 - 55);                   // A Ascll - 65
        else
            int_ch1 = (hex_char1 - 87);                   // a Ascll - 97
        
        [ary addObject:[NSString stringWithFormat:@"%d",int_ch1]];
    }
    
    NSInteger ten = 0;
    
    for (int i = 0; i < [ary count]; i++)
    {
        ten += [[ary objectAtIndex:i] intValue] * pow(16,([ary count]-i)-1);
    }
    
    return ten;
}

- (NSInteger)convertToDecWithBinary
{
    NSInteger decInteger = 0;
    
    for (NSInteger i = 0; i < self.length; i++)
    {
        NSString *str = [self substringWithRange:NSMakeRange(i, 1)];
        
        decInteger += ((NSInteger)pow(2, ((self.length - i) - 1)) * [str intValue]);
    }
    
    return decInteger;
}

+ (NSString *)convertToBinaryWithDecimal:(NSInteger)decValue FillToLength:(NSInteger)length
{
    NSMutableString *binStr = [[NSMutableString alloc] init];
    
    for( ; (decValue > 1); (decValue /= 2))
    {
        [binStr insertString:[NSString stringWithFormat:@"%ld",(decValue % 2)] atIndex:0];
    }
    
    [binStr insertString:[NSString stringWithFormat:@"%ld",decValue] atIndex:0];
    
    if (length > 0 && length > binStr.length)
    {
        for(NSInteger i = (length - binStr.length); i > 0; i--)
        {
            [binStr insertString:[NSString stringWithFormat:@"0"] atIndex:0];
        }
    }
    
    return binStr;
}

+ (NSString *)convertToHexWithDecimal:(NSInteger)decValue
{
    NSMutableString *binStr = [[NSMutableString alloc] init];
    
    for( ; (decValue > 16); (decValue /= 16))
    {
        [binStr insertString:[self transfer:(decValue % 16)] atIndex:0];
    }
    
    [binStr insertString:[self transfer:decValue] atIndex:0];
    
    return binStr;
}

+ (NSString *)transfer:(NSInteger)decValue
{
    if (decValue == 10)
    {
        return @"A";
    }
    else if (decValue == 11)
    {
        return @"B";
    }
    else if (decValue == 12)
    {
        return @"C";
    }
    else if (decValue == 13)
    {
        return @"D";
    }
    else if (decValue == 14)
    {
        return @"E";
    }
    else if (decValue == 15)
    {
        return @"F";
    }
    else
    {
        return [NSString stringWithFormat:@"%ld",decValue];
    }
}

- (NSMutableAttributedString *)convertToAttrString
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:self];
    
    return attStr;
}

- (NSString *)stringToBigEndString
{
    if (self.length == 0)
        return self;
    
    NSString *firstStr = [self substringToIndex:1];
    
    NSString *secondStr = [self substringFromIndex:1];
    
    return [NSString stringWithFormat:@"%@%@",[firstStr uppercaseString],[secondStr lowercaseString]];
}

@end
