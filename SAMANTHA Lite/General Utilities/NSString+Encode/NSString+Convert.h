//
//  NSString+DateSort.h
//  12Ours
//
//  Created by Blues on 2018/6/26.
//  Copyright © 2018年 12Ours. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Convert)

- (NSInteger)convertToDecWithHex;
- (NSInteger)convertToDecWithBinary;

- (NSMutableAttributedString *)convertToAttrString;

+ (NSString *)convertToBinaryWithDecimal:(NSInteger)decValue FillToLength:(NSInteger)length;
+ (NSString *)convertToHexWithDecimal:(NSInteger)decValue;

- (NSString *)stringToBigEndString;

@end
