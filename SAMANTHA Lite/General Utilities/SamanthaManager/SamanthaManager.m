//
//  SamanthaManager.m
//  HWC Roasters
//
//  Created by Blues on 2020/8/10.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "SamanthaManager.h"

@implementation SamanthaManager
@synthesize delegate;
@synthesize autoConnFlag;

#pragma mark - Init

+ (instancetype)sharedInstance
{
    static SamanthaManager *pSelf;
    
    static dispatch_once_t singletonToken;
    
    dispatch_once(&singletonToken,^
                  {
                      pSelf = [[self alloc] init];
                  });
    
    return pSelf;
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        [HiroiaManager sharedInstance].delegate = self;
        
        tempRecipeStruc = [BaseRecipeStruc new];

        retryCount = 0;
        
        autoConnFlag = YES;
        
        timerDic = [NSMutableDictionary new];
        
        [self sendCmdStartScan];
    }
    
    return self;
}

#pragma mark - Check

- (BOOL)smtCheckConnectDeviceStatus
{
    return ([HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues.count > 0);
}

- (BOOL)smtCheckTemperatureWithRecipe:(SamanthaRecipeStruc *)recipe
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    if (device.temperature > recipe.temperature)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (BOOL)smtCheckWater250ml
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    if (device.remainWater >= 250)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)smtCheckRemainingWithRecipe:(SamanthaRecipeStruc *)recipe
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    NSInteger totalWater = 0;

    for (int i = 0 ; i < 11 ; i++)
    {
        if (i < recipe.brewStep.count)
        {
            StepStruc *brewStep = recipe.brewStep[i];

            totalWater += brewStep.water;
        }
    }

    if (device.remainWater >= (totalWater + 60))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - Set/Get

- (NSString *)smtGetConnectName
{
    if ([HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues > 0)
    {
        HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

        return device.name;
    }
    else
    {
        return @"";
    }
}

#pragma mark - Do

- (void)smtDoDisConnectAllDevice
{
    pwdErrorFlag = NO;
    
    for (NSTimer *timer in timerDic.allValues)
    {
        [timer invalidate];
    }
    
    [timerDic removeAllObjects];
    
    [[HiroiaManager sharedInstance] stopScanDevice];
    
    for (HiroiaDevice *device in [HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues)
    {
        [[HiroiaManager sharedInstance] stopGetStatusTimerWith:device];
        
        [[HiroiaManager sharedInstance] sendStopConnect:device];
    }
}

#pragma mark - Brew

- (void)smtBrewWithRecipe:(SamanthaRecipeStruc *)recipe
{
    tempRecipeStruc.temperature = recipe.temperature;

    [tempRecipeStruc.brewStep removeAllObjects];

    for (int i = 0 ; i < 11 ; i++)
    {
        if (i < recipe.brewStep.count)
        {
            StepStruc *stepStruc = recipe.brewStep[i];

            stepStruc.step = (i + 1);

            BrewStepStruc *brewStep = (BrewStepStruc *)stepStruc;
            
            [tempRecipeStruc.brewStep addObject:brewStep];
        }
        else
        {
            BrewStepStruc *brewStep = [BrewStepStruc new];

            brewStep.step = (i + 1);

            [tempRecipeStruc.brewStep addObject:brewStep];
        }
    }

    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    [[HiroiaManager sharedInstance] sendSamanthaCmdP1SetDripTemperature:tempRecipeStruc.temperature With:device];
}

- (void)smtBrewStart
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    [[HiroiaManager sharedInstance] sendSamanthaCmdP3RunDripping:SMT_RUN_CMD_START With:device];
}

- (void)smtBrewPause
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    [[HiroiaManager sharedInstance] sendSamanthaCmdP3RunDripping:SMT_RUN_CMD_PAUSE With:device];
}

- (void)smtBrewStop
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

    [[HiroiaManager sharedInstance] sendSamanthaCmdP3RunDripping:SMT_RUN_CMD_QUIT With:device];
}

#pragma mark - Public API

- (void)sendCmdStartScan
{
    __weak SamanthaManager *weakSelf = self;

    if ([HiroiaManager sharedInstance].status == CBManagerStatePoweredOn)
    {
        [[HiroiaManager sharedInstance] stopScanDevice];

        [[HiroiaManager sharedInstance] sendStartScanDeviceWithService:[NSArray arrayWithObjects:[CBUUID UUIDWithString:BLE_SAMANTHA_SERVICE], nil] ScanTime:0];
    }
    else if ([HiroiaManager sharedInstance].status != CBManagerStateUnknown)
    {
        if ([delegate respondsToSelector:@selector(onSendCmdStartScanBLENotReady)])
            [delegate onSendCmdStartScanBLENotReady];
    }
    else
    {
        [Utilities delayBlockWith:0.5 Queue:dispatch_get_main_queue() Block:^
        {
            [weakSelf sendCmdStartScan];
        }];
    }
}

- (void)sendCmdStopScan
{
    [[HiroiaManager sharedInstance] stopScanDevice];
}

- (void)sendCmdConnectWithDevice:(HiroiaDevice *)device
{
     if ([[HiroiaManager sharedInstance] findHiroiaDeviceWithMac:device.mac] != nil)
     {
         if ([self->delegate respondsToSelector:@selector(onSendCmdConnectWithDeviceSuccess:)])
             [self->delegate onSendCmdConnectWithDeviceSuccess:device];
         
         return;
     }

    for (HiroiaDevice *connDevice in [HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues)
    {
        [[HiroiaManager sharedInstance] stopGetStatusTimerWith:connDevice];

        [[HiroiaManager sharedInstance] sendStopConnect:connDevice];
    }

    [Utilities delayBlockWith:0.5 Queue:dispatch_get_main_queue() Block:^
    {
        [[HiroiaManager sharedInstance].hiroiaDeviceDictionary removeAllObjects];

        [[HiroiaManager sharedInstance] sendStartConnectPeripheral:device Password:device.password Timeout:10];
    }];
}

- (void)sendCmdGetRecipe:(NSInteger)recipeIndex
{
    HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];
        
    [[HiroiaManager sharedInstance] sendSamanthaCmdP8GetNumberOfRecipe:recipeIndex With:device];
}

#pragma mark - BLEHiroiaManagerDelegate

- (void)onHRIManagerReadyResponse:(CBManagerState)state
{
    NSLog(@"onHRIManagerReadyResponse = %@",[self managerStateChange:state]);
}

- (void)onHRIManagerStartScanDeviceWithService:(NSArray<HiroiaDevice *> *)scanDeviceArray
{
    NSLog(@"onHRIManagerStartScanDeviceWithService = %lu", (unsigned long)scanDeviceArray.count);

    scanArray = scanDeviceArray;

    NSString *curSmtMac = [BOUserInfo getStringForKey:SAVE_SMT_MAC];
    
    if (curSmtMac != nil && self.autoConnFlag)
    {
        for (HiroiaDevice *device in scanArray)
        {
            if ([device.mac isEqualToString:curSmtMac])
            {
                NSString *curSmtPwd = [BOUserInfo getStringForKey:SAVE_SMT_PWD];
                
                if (device.lock && curSmtPwd != nil)
                {
                    device.password = curSmtPwd;
                }
                
                [self sendCmdConnectWithDevice:device];
                
                break;
            }
        }
    }
    
    [Utilities runInMainThread:^
    {
        if ([self->delegate respondsToSelector:@selector(onSendCmdStartScanResult:)])
            [self->delegate onSendCmdStartScanResult:self->scanArray];
    }];
}

- (void)onHRIManagerStartConnectPeripheralResponse:(HiroiaDevice *)hiroiaDevice State:(BASE_BLE_CONNECT_STATUS)state Error:(NSError *)error
{
    NSLog(@"onStartConnectPeripheralResponse = %@", state == BASE_BLE_CONNECT_STATUS_SUCCESS ? @"BASE_BLE_CONNECT_STATUS_SUCCESS" : @"BASE_BLE_CONNECT_STATUS_FAILED");

    [[HiroiaManager sharedInstance] stopScanDevice];

    if (state == BASE_BLE_CONNECT_STATUS_SUCCESS)
    {
        if (hiroiaDevice.deviceType == DEVICE_SMT)
        {
            [[HiroiaManager sharedInstance] sendSamanthaCmdP23SendPwdWith:hiroiaDevice];
        }
    }
    else
    {
        [Utilities runInMainThread:^
        {
            if ([self->delegate respondsToSelector:@selector(onSendCmdConnectWithDeviceFailed:)])
                [self->delegate onSendCmdConnectWithDeviceFailed:hiroiaDevice];
        }];
    }
}

- (void)onHRIManagerDisConnectPeripheralResponse:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onDisConnectPeripheralResponse");

    [[HiroiaManager sharedInstance] stopGetStatusTimerWith:hiroiaDevice];

    [Utilities runInMainThread:^
    {
        if (self->pwdErrorFlag)
        {
            NSTimer *timer = [self->timerDic objectForKey:hiroiaDevice.peripheral];
            
            [timer invalidate];
            
            [self->timerDic removeAllObjects];
            
            if ([self->delegate respondsToSelector:@selector(onSendCmdConnectWithDevicePwdError:)])
                [self->delegate onSendCmdConnectWithDevicePwdError:hiroiaDevice];
        }
        else
        {
            if ([self->delegate respondsToSelector:@selector(smtResponseDisConnected)])
                [self->delegate smtResponseDisConnected];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SMT_DIS_CONNECT object:nil];
    }];
}

#pragma mark - Utilities

- (NSString *)managerStateChange:(CBManagerState)state
{
    NSString *status = @"Unknown";

    switch (state)
    {
        case CBManagerStateUnknown:
        {
            status = @"Unknown";

            break;
        }
        case CBManagerStateResetting:
        {
            status = @"Resetting";

            break;
        }
        case CBManagerStateUnsupported:
        {
            status = @"Unsupported";

            break;
        }
        case CBManagerStateUnauthorized:
        {
            status = @"Unauthorized";

            break;
        }
        case CBManagerStatePoweredOff:
        {
            status = @"PoweredOff";

            break;
        }
        case CBManagerStatePoweredOn:
        {
            status = @"PoweredOn";

            break;
        }
        default:
            break;
    }

    return status;
}

#pragma mark --- SAMANTHA

- (void)onSamanthaCmdP1DripTemperatureResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP1DripTemperatureResponse = %ld",errorCode);

    if (errorCode == SMT_ERROR_NO_ERROR)
    {
        HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

        [[HiroiaManager sharedInstance] sendSamanthaCmdP2SetDripSetp:tempRecipeStruc With:device];

        retryCount = 0;
    }
    else
    {
        if (retryCount > 3)
        {
            [Utilities runInMainThread:^
            {
                self->retryCount = 0;

                if ([self->delegate respondsToSelector:@selector(smtResponseBrewFailed:)])
                    [self->delegate smtResponseBrewFailed:hiroiaDevice];
            }];
        }
        else
        {
            [Utilities delayBlockWith:0.5 Queue:dispatch_get_main_queue() Block:^
            {
                self->retryCount++;

                [[HiroiaManager sharedInstance] sendSamanthaCmdP1SetDripTemperature:self->tempRecipeStruc.temperature With:hiroiaDevice];
            }];
        }
    }
}

- (void)onSamanthaCmdP2SetDripSetpResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP2SetDripSetpResponse = %ld",errorCode);

    if (errorCode == SMT_ERROR_NO_ERROR)
    {
        HiroiaDevice *device = [[HiroiaManager sharedInstance].hiroiaDeviceDictionary.allValues firstObject];

        [[HiroiaManager sharedInstance] sendSamanthaCmdP3RunDripping:SMT_RUN_CMD_START With:device];

        retryCount = 0;
    }
    else
    {
        if (retryCount > 3)
        {
            [Utilities runInMainThread:^
            {
                self->retryCount = 0;

                if ([self->delegate respondsToSelector:@selector(smtResponseBrewFailed:)])
                    [self->delegate smtResponseBrewFailed:hiroiaDevice];
            }];
        }
        else
        {
            [Utilities delayBlockWith:0.5 Queue:dispatch_get_main_queue() Block:^
            {
                self->retryCount++;

                [[HiroiaManager sharedInstance] sendSamanthaCmdP2SetDripSetp:self->tempRecipeStruc With:hiroiaDevice];
            }];
        }
    }
}

- (void)onSamanthaCmdP3RunDrippingResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP3RunDrippingResponse = %ld",errorCode);

    if (errorCode == SMT_ERROR_NO_ERROR)
    {

    }
    else if (errorCode == SMT_ERROR_LACK_WATER)
    {
        [Utilities runInMainThread:^
        {
            self->retryCount = 0;

            if ([self->delegate respondsToSelector:@selector(smtResponseLackWater:)])
                [self->delegate smtResponseLackWater:hiroiaDevice];
        }];
    }
    else if (errorCode == SMT_ERROR_HOT_WATER)
    {
        [Utilities runInMainThread:^
        {
            self->retryCount = 0;

            if ([self->delegate respondsToSelector:@selector(smtResponseHotWater:)])
                [self->delegate smtResponseHotWater:hiroiaDevice];
        }];
    }
    else
    {
        if (retryCount > 3)
        {
            [Utilities runInMainThread:^
            {
                self->retryCount = 0;

                if ([self->delegate respondsToSelector:@selector(smtResponseBrewFailed:)])
                    [self->delegate smtResponseBrewFailed:hiroiaDevice];
            }];
        }
        else
        {
            [Utilities delayBlockWith:0.5 Queue:dispatch_get_main_queue() Block:^
            {
                self->retryCount++;

                [[HiroiaManager sharedInstance] sendSamanthaCmdP3RunDripping:SMT_RUN_CMD_START With:hiroiaDevice];
            }];
        }
    }
}

- (void)onSamanthaCmdP4RunDrainResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP4RunDrainResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP5GetStatusResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP5GetStatusResponse = %ld",errorCode);
    
    [Utilities runInMainThread:^
    {
        if ([self->delegate respondsToSelector:@selector(smtResponseUpdateStatus:)])
            [self->delegate smtResponseUpdateStatus:hiroiaDevice];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SMT_P5_CALLBACK object:nil];
    }];
}

- (void)onSamanthaCmdP6ClearTemperatureAndDripStepResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP6ClearTemperatureAndDripStepResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP7GetTemperatureAndRecipeStepResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP7GetTemperatureAndRecipeStepResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP8GetNumberOfRecipeResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP8GetNumberOfRecipeResponse = %ld",errorCode);
    
    [Utilities delayBlockWith:0.1 Queue:dispatch_get_main_queue() Block:^
    {
        for (BaseRecipeStruc *recipe in hiroiaDevice.machineRecipe)
        {
            if (recipe.temperature == -1)
            {
                NSInteger index = [hiroiaDevice.machineRecipe indexOfObject:recipe];
                
                [self sendCmdGetRecipe:(index + 1)];
                
                break;
            }
        }
    }];
}

- (void)onSamanthaCmdP9ReplaceNumberOfRecipeResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP9ReplaceNumberOfRecipeResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP10RestartBLEResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP10RestartBLEResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP11SetNameResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP11SetNameResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP12SetNewPasswordResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP12SetNewPasswordResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP13RunCleanResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP13RunCleanResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP14BaristaHeatResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP14BaristaHeatResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP15BaristaDripResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP15BaristaDripResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP16SetLightStrongResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP16SetLightStrongResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP17ResetToDefalutResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP17ResetToDefalutResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP18GetDeviceInfoResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP18GetDeviceInfoResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP19SelectNumberOfRecipeResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP19SelectNumberOfRecipeResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP20CheckAliveTimesOfSecResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP20CheckAliveTimesOfSecResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP21GetMACResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP21GetMACResponse = %ld",errorCode);
}

- (void)onSamanthaCmdP22GetTemperatureResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP22GetTemperatureResponse  = %ld",errorCode);
}

- (void)onSamanthaCmdP23SendPwdResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP23SendPwdResponse = %ld",errorCode);

    if (errorCode == SMT_ERROR_NO_ERROR)
    {
        [Utilities runInMainThread:^
        {
            if (hiroiaDevice.lock)
            {
                self->pwdErrorFlag = YES;
                
                NSTimer *curTimer = [self->timerDic objectForKey:hiroiaDevice.peripheral];
                
                if (curTimer != nil)
                {
                    [curTimer invalidate];
                    
                    [self->timerDic removeObjectForKey:hiroiaDevice.peripheral];
                }
                
                [self createTimerWith:hiroiaDevice];
                    
                if (self->autoConnFlag)
                {
                    if ([self->delegate respondsToSelector:@selector(onSendCmdConnectWithDeviceSuccess:)])
                        [self->delegate onSendCmdConnectWithDeviceSuccess:hiroiaDevice];

                    [[HiroiaManager sharedInstance] startGetStatusTimer:1 With:hiroiaDevice];
                    
                    [self sendCmdGetRecipe:1];
                }
            }
            else
            {
                if ([self->delegate respondsToSelector:@selector(onSendCmdConnectWithDeviceSuccess:)])
                    [self->delegate onSendCmdConnectWithDeviceSuccess:hiroiaDevice];

                [[HiroiaManager sharedInstance] startGetStatusTimer:1 With:hiroiaDevice];
                
                [self sendCmdGetRecipe:1];
            }
        }];
    }
}

- (void)onSamanthaCmdP24TotalDripTimesResponse:(SMT_ERROR_LIST)errorCode With:(HiroiaDevice *)hiroiaDevice
{
    NSLog(@"onSamanthaCmdP24TotalDripTimesResponse = %ld",errorCode);
}

#pragma mark - Timer

- (void)createTimerWith:(HiroiaDevice *)hiroiaDevice
{
    if (hiroiaDevice.peripheral == nil)
    {
        return;
    }
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:5.5 target:self selector:@selector(timerCallback:) userInfo:nil repeats:NO];

    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];

    [timerDic setObject:timer forKey:hiroiaDevice.peripheral];
}

- (void)timerCallback:(NSTimer *)timer
{
    if (timer != nil)
    {
        [timer invalidate];
    }
    
    NSArray *array = [timerDic allKeysForObject:timer];
    
    CBPeripheral *peripheral = array[0];
    
    [timerDic removeObjectForKey:peripheral];
    
    pwdErrorFlag = NO;
    
    if (!autoConnFlag)
    {
        HiroiaDevice *hiroiaDevice = [[HiroiaManager sharedInstance] findHiroiaDeviceWith:peripheral];
        
        [Utilities runInMainThread:^
        {
            if ([self->delegate respondsToSelector:@selector(onSendCmdConnectWithDeviceSuccess:)])
                [self->delegate onSendCmdConnectWithDeviceSuccess:hiroiaDevice];

            [[HiroiaManager sharedInstance] startGetStatusTimer:1 With:hiroiaDevice];
            
            [self sendCmdGetRecipe:1];
        }];
    }
}

@end
