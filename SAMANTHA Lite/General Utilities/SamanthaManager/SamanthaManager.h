//
//  SamanthaManager.h
//  HWC Roasters
//
//  Created by Blues on 2020/8/10.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SamanthaRecipeStruc.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SamanthaManagerDelegate <NSObject>

- (void)onSendCmdStartScanBLENotReady;

- (void)onSendCmdStartScanResult:(NSArray<HiroiaDevice *> *)scanArray;

- (void)onSendCmdConnectWithDeviceSuccess:(HiroiaDevice *)hiroiaDevice;

- (void)onSendCmdConnectWithDeviceFailed:(HiroiaDevice *)hiroiaDevice;

- (void)onSendCmdConnectWithDevicePwdError:(HiroiaDevice *)hiroiaDevice;

- (void)smtResponseDisConnected;

- (void)smtResponseUpdateStatus:(HiroiaDevice *)device;

- (void)smtResponseBrewFailed:(HiroiaDevice *)device;

- (void)smtResponseLackWater:(HiroiaDevice *)device;

- (void)smtResponseHotWater:(HiroiaDevice *)device;

@end

@interface SamanthaManager : NSObject<BaseHriManagerDelegate>
{
    NSArray<HiroiaDevice *> *scanArray;

    BOOL pwdErrorFlag;
    
    BaseRecipeStruc *tempRecipeStruc;

    NSInteger retryCount;
    
    NSMutableDictionary *timerDic;
}

@property (weak, nonatomic) id<SamanthaManagerDelegate> delegate;
@property (assign, nonatomic) BOOL autoConnFlag;

+ (instancetype)sharedInstance;

- (void)sendCmdStartScan;
- (void)sendCmdStopScan;
- (void)sendCmdConnectWithDevice:(HiroiaDevice *)device;
- (void)sendCmdGetRecipe:(NSInteger)recipeIndex;

- (void)smtDoDisConnectAllDevice;

- (NSString *)smtGetConnectName;

- (void)smtBrewWithRecipe:(SamanthaRecipeStruc *)recipe;
- (void)smtBrewStart;
- (void)smtBrewPause;
- (void)smtBrewStop;

- (BOOL)smtCheckConnectDeviceStatus;
- (BOOL)smtCheckTemperatureWithRecipe:(SamanthaRecipeStruc *)recipe;
- (BOOL)smtCheckWater250ml;
- (BOOL)smtCheckRemainingWithRecipe:(SamanthaRecipeStruc *)recipe;

//- (void)closePwdError;

@end

NS_ASSUME_NONNULL_END
