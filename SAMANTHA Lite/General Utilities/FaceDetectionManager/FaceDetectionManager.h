//
//  FaceDetectionManager.m
//  FaceAI
//
//  Created by Blues on 2020/12/25.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class FaceDetectionManager;

typedef NS_ENUM(NSInteger, FDM_CAMERA_POSITION)
{
    FDM_CAMERA_POSITION_FRONT = 0,
    FDM_CAMERA_POSITION_BACK,
};

@protocol FaceDetectionManagerDelegate<NSObject>

- (void)faceDetectionManagerCaptureOutput:(UIImage *)image;
- (void)FaceDetectionManagerIsDetectedFace:(FaceDetectionManager *)manager;
- (void)faceDetectionCameraError:(FaceDetectionManager *)manager error:(NSError *)error;

@end

@interface FaceDetectionManager : NSObject

@property (weak, nonatomic) id<FaceDetectionManagerDelegate> delegate;

@property (readonly, nonatomic) FDM_CAMERA_POSITION fdmCameraPosition;

@property (strong, nonatomic) NSMutableArray<CALayer *> *faceLayerArray;
@property (strong, nonatomic) NSMutableArray<CALayer *> *leftEyeLayerArray;
@property (strong, nonatomic) NSMutableArray<CALayer *> *rightEyeLayerArray;
@property (strong, nonatomic) NSMutableArray<CALayer *> *mouthLayerArray;
@property (strong, nonatomic) NSMutableArray<CALayer *> *cupLayerArray;

@property (assign, nonatomic) BOOL isShowFaceFrame;
@property (assign, nonatomic) BOOL isShowLeftEyeFrame;
@property (assign, nonatomic) BOOL isShowRightEyeFrame;
@property (assign, nonatomic) BOOL isShowMouthFrame;
@property (assign, nonatomic) BOOL isShowCupFrame;

@property (strong, nonatomic) UIImage *cupLayerImage;
@property (strong, nonatomic) UIImage *faceLayerImage;
@property (strong, nonatomic) UIImage *leftEyeLayerImage;
@property (strong, nonatomic) UIImage *rightEyeLayerImage;
@property (strong, nonatomic) UIImage *mouthLayerImage;

- (instancetype)initWithPreviewView:(UIView *)previewView UseCameraPosition:(FDM_CAMERA_POSITION)cameraPosition;

- (void)setupAVCapture;
- (void)teardownAVCapture;
- (void)captureImage;
- (void)switchRunningState;
- (void)resetFrame;

@end
