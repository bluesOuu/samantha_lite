//
//  FaceDetectionManager.m
//  FaceAI
//
//  Created by Blues on 2020/12/25.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "FaceDetectionManager.h"

#define FACE_LAYER              @"FACE_LAYER"
#define LEFT_EYE_LAYER          @"LEFT_EYE_LAYER"
#define RIGHT_EYE_LAYER         @"RIGHT_EYE_LAYER"
#define MOUTH_LAYER             @"MOUTH_LAYER"
#define CUP_LAYER               @"CUP_LAYER"
#define MAX_DETECT_COUNT        5

#define Radians_To_Degrees(radians) ((radians) * (180.0 / M_PI))


#define Degrees_To_Radians(degrees) ((degrees) * (M_PI / 180.0))


#import <AVFoundation/AVFoundation.h>

@interface FaceDetectionManager()<AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate>
{
    AVCaptureSession *session;
    
    BOOL updateFrame;
}

@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;

@property (nonatomic) dispatch_queue_t videoDataOutputQueue;

@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;

@property (nonatomic, strong) CIDetector *faceDetector;

@property (strong, nonatomic) UIView *previewView;

@property (nonatomic, strong) AVCapturePhotoOutput *imageOutput;

@end

@implementation FaceDetectionManager
@synthesize isShowFaceFrame;
@synthesize isShowLeftEyeFrame;
@synthesize isShowRightEyeFrame;
@synthesize isShowMouthFrame;
@synthesize isShowCupFrame;

#pragma mark - Init

- (instancetype)initWithPreviewView:(UIView *)previewView UseCameraPosition:(FDM_CAMERA_POSITION)cameraPosition
{
    self = [super init];
    
    if (self)
    {
        self.previewView = previewView;
        
        _fdmCameraPosition = cameraPosition;
        
        [self setupData];
    }
    
    return self;
}

#pragma mark - Setup

- (void)setupData
{
    updateFrame = YES;
    
    self.faceLayerArray = [NSMutableArray new];

    self.leftEyeLayerArray = [NSMutableArray new];

    self.rightEyeLayerArray = [NSMutableArray new];

    self.mouthLayerArray = [NSMutableArray new];

    self.cupLayerArray = [NSMutableArray new];
    
    NSDictionary *detectorOptions = [[NSDictionary alloc] initWithObjectsAndKeys:CIDetectorAccuracyLow, CIDetectorAccuracy, nil];
    
    self.faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:detectorOptions];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerCallback) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)timerCallback
{
    updateFrame = YES;
}

- (void)setupAVCapture
{
    session = [[AVCaptureSession alloc] init];
    
    [session setSessionPreset:AVCaptureSessionPreset640x480];

    [self setupVideoInput];
    
    [self setupVideoOutput];
    
    [self setupImageOutput];
    
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    
    self.previewLayer.backgroundColor = [[UIColor blackColor] CGColor];
    
    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;

    CALayer *rootLayer = [self.previewView layer];
    
    [rootLayer setMasksToBounds:YES];
    
    [self.previewLayer setFrame:[rootLayer bounds]];
    
    [rootLayer addSublayer:self.previewLayer];
    
    [session startRunning];
}

- (void)setupVideoInput
{
    __weak typeof(self) weakSelf = self;
    
    NSError *error = nil;
    
    AVCaptureDevice *device = [self findCamera];
    
    if (device == nil)
    {
        device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        _fdmCameraPosition = FDM_CAMERA_POSITION_BACK;
    }

    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    
    if (error)
    {
        session = nil;
        
        [self teardownAVCapture];
        
        if ([_delegate respondsToSelector:@selector(faceDetectionCameraError:error:)])
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                [weakSelf.delegate faceDetectionCameraError:weakSelf error:error];
            });
        }
        
        return;
    }

    if ([session canAddInput:deviceInput])
    {
        [session addInput:deviceInput];
    }
}

- (void)setupVideoOutput
{
    self.videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];

    NSDictionary *rgbOutputSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCMPixelFormat_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    
    [self.videoDataOutput setVideoSettings:rgbOutputSettings];
    
    [self.videoDataOutput setAlwaysDiscardsLateVideoFrames:YES];

    self.videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
    
    [self.videoDataOutput setSampleBufferDelegate:self queue:self.videoDataOutputQueue];
    
    [[self.videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:YES];
    
    if ([session canAddOutput:self.videoDataOutput])
    {
        [session addOutput:self.videoDataOutput];
    }
}

- (void)setupImageOutput
{
    self.imageOutput = [[AVCapturePhotoOutput alloc] init];
    
    NSDictionary *setDic = @{AVVideoCodecKey:AVVideoCodecTypeJPEG};
    
    [self.imageOutput setPhotoSettingsForSceneMonitoring:[AVCapturePhotoSettings photoSettingsWithFormat:setDic]];
    
    if ([session canAddOutput:self.imageOutput])
    {
        [session addOutput:self.imageOutput];
    }
}

#pragma mark - Utilities

- (AVCaptureDevice *)findCamera
{
    AVCaptureDevice *captureDevice = nil;
    
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera] mediaType:AVMediaTypeVideo position: (self.fdmCameraPosition == FDM_CAMERA_POSITION_FRONT ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack)];
    
    NSArray *captureDevices = [captureDeviceDiscoverySession devices];
    
    for (AVCaptureDevice *device in captureDevices)
    {
        if (device.position == (self.fdmCameraPosition == FDM_CAMERA_POSITION_FRONT ? AVCaptureDevicePositionFront : AVCaptureDevicePositionBack))
        {
            captureDevice = device;
            
            break;
        }
    }

    return captureDevice;
}

- (void)teardownAVCapture
{
    [session stopRunning];
    
    session = nil;
    
    self.videoDataOutput = nil;
    
    self.imageOutput = nil;
    
    if (self.videoDataOutputQueue)
    {
        self.videoDataOutputQueue = nil;
    }
    
    [self.previewLayer removeFromSuperlayer];

    self.previewLayer = nil;
}

- (void)captureImage
{
    NSDictionary *setDic = @{AVVideoCodecKey:AVVideoCodecTypeJPEG};
    
    [self.imageOutput capturePhotoWithSettings:[AVCapturePhotoSettings photoSettingsWithFormat:setDic] delegate:self];
}

- (void)switchRunningState
{
    if (session.isRunning)
    {
        [session stopRunning];
    }
    else
    {
        [session startRunning];
    }
}

- (void)resetFrame
{
    CALayer *rootLayer = [self.previewView layer];
    
    [self.previewLayer setFrame:[rootLayer bounds]];
}


- (NSNumber *)exifOrientation:(UIDeviceOrientation)orientation
{
    int exifOrientation;
    
    enum {
        PHOTOS_EXIF_0ROW_TOP_0COL_LEFT          = 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
        PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT         = 2, //   2  =  0th row is at the top, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
        PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
        PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
    };

    switch (orientation)
    {
        case UIDeviceOrientationPortraitUpsideDown:
        {
            exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
            
            break;
        }
        case UIDeviceOrientationLandscapeLeft:
        {
            if (self.fdmCameraPosition == FDM_CAMERA_POSITION_FRONT)
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            
            break;
        }
        case UIDeviceOrientationLandscapeRight:
        {
            if (self.fdmCameraPosition == FDM_CAMERA_POSITION_FRONT)
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            break;
        }
        case UIDeviceOrientationPortrait:
        default:
        {
            exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
            
            break;
        }
    }
    
    return [NSNumber numberWithInt:exifOrientation];
}

+ (CGRect)videoPreviewBoxForGravity:(NSString *)gravity frameSize:(CGSize)frameSize apertureSize:(CGSize)apertureSize
{
    CGFloat apertureRatio = apertureSize.height / apertureSize.width;
    
    CGFloat viewRatio = frameSize.width / frameSize.height;

    CGSize size = CGSizeZero;
    
    if ([gravity isEqualToString:AVLayerVideoGravityResizeAspectFill])
    {
        if (viewRatio > apertureRatio) // 上下裁切 左右延展
        {
            size.width = frameSize.width;
            size.height = apertureSize.width * (frameSize.width / apertureSize.height);
        }
        else // 左右才切 上下延展
        {
            size.width = apertureSize.height * (frameSize.height / apertureSize.width);
            size.height = frameSize.height;
        }
    }
    else if ([gravity isEqualToString:AVLayerVideoGravityResizeAspect])
    {
        if (viewRatio > apertureRatio)
        {
            size.width = apertureSize.height * (frameSize.height / apertureSize.width);
            size.height = frameSize.height;
        }
        else
        {
            size.width = frameSize.width;
            size.height = apertureSize.width * (frameSize.width / apertureSize.height);
        }
    }
    else if ([gravity isEqualToString:AVLayerVideoGravityResize])
    {
        size.width = frameSize.width;
        
        size.height = frameSize.height;
    }

    CGRect videoBox;
    
    videoBox.size = size;
    
//    if (size.width < frameSize.width)
//    {
        videoBox.origin.x = (frameSize.width - size.width) / 2;
//    }
//    else
//    {
//        videoBox.origin.x = (size.width - frameSize.width) / 2;
//    }
//
//    if (size.height < frameSize.height)
//    {
        videoBox.origin.y = (frameSize.height - size.height) / 2;
//    }
//    else
//    {
//        videoBox.origin.y = (size.height - frameSize.height) / 2;
//    }

    return videoBox;
}

- (CGRect)diagonalConversion:(CGRect)orignalRect
{
    CGRect convertRect = CGRectZero;

    convertRect.size.width = orignalRect.size.height;
    
    convertRect.size.height = orignalRect.size.width;
    
    convertRect.origin.x = orignalRect.origin.y;
    
    convertRect.origin.y = orignalRect.origin.x;
    
    return convertRect;
}

- (CGRect)scaleRect:(CGRect)orignalRect ScaleW:(CGFloat)scaleW ScaleH:(CGFloat)scaleH
{
    CGRect faceRect = CGRectZero;
    
    faceRect.size.width = orignalRect.size.width * scaleW;
    
    faceRect.size.height = orignalRect.size.height * scaleH;
    
    faceRect.origin.x = orignalRect.origin.x * scaleW;
    
    faceRect.origin.y = orignalRect.origin.y * scaleH;
    
    return faceRect;
}

- (CGRect)getFaceRectWithCIFaceFeature:(CIFaceFeature *)faceFeture PreviewBox:(CGRect)previewBox ScaleW:(CGFloat)scaleW ScaleH:(CGFloat)scaleH
{
    CGRect faceRect = CGRectZero;

    faceRect.size.width = faceFeture.bounds.size.height * scaleW;
    
    faceRect.size.height = faceFeture.bounds.size.width * scaleH;
    
    faceRect.origin.x = faceFeture.bounds.origin.y * scaleW;
    
    faceRect.origin.y = faceFeture.bounds.origin.x * scaleH;
    
    if ([self.previewLayer.connection isVideoMirrored])
    {
        faceRect = CGRectOffset(faceRect, previewBox.origin.x + previewBox.size.width - faceRect.size.width - (faceRect.origin.x * 2), previewBox.origin.y);
    }
    else
    {
        faceRect = CGRectOffset(faceRect, previewBox.origin.x, previewBox.origin.y);
    }
    
    return faceRect;
}

- (CGRect)getLeftEyeRectWithCIFaceFeature:(CIFaceFeature *)faceFeture FaceRect:(CGRect)faceRect PreviewBox:(CGRect)previewBox ScaleW:(CGFloat)scaleW ScaleH:(CGFloat)scaleH
{
    CGFloat eyeWidth = (faceRect.size.width / 3.5);

    CGFloat eyeHeight = ((eyeWidth / 5) * 2);

    CGRect leftRect = CGRectMake((faceFeture.leftEyePosition.y * scaleW) - (eyeWidth / 2), (faceFeture.leftEyePosition.x * scaleH) - (eyeHeight / 2), eyeWidth, eyeHeight);

    if ([self.previewLayer.connection isVideoMirrored])
    {
        leftRect = CGRectOffset(leftRect, previewBox.origin.x + previewBox.size.width - leftRect.size.width - (leftRect.origin.x * 2), previewBox.origin.y);
    }
    else
    {
        leftRect = CGRectOffset(leftRect, previewBox.origin.x, previewBox.origin.y);
    }
    
    return  leftRect;
}

- (CGRect)getRightEyeRectWithCIFaceFeature:(CIFaceFeature *)faceFeture FaceRect:(CGRect)faceRect PreviewBox:(CGRect)previewBox ScaleW:(CGFloat)scaleW ScaleH:(CGFloat)scaleH
{
    CGFloat eyeWidth = (faceRect.size.width / 3.5);

    CGFloat eyeHeight = ((eyeWidth / 5) * 2);

    CGRect rightRect = CGRectMake((faceFeture.rightEyePosition.y * scaleW) - (eyeWidth / 2), (faceFeture.rightEyePosition.x * scaleH) - (eyeHeight / 2), eyeWidth, eyeHeight);

    if ([self.previewLayer.connection isVideoMirrored])
    {
        rightRect = CGRectOffset(rightRect, previewBox.origin.x + previewBox.size.width - rightRect.size.width - (rightRect.origin.x * 2), previewBox.origin.y);
    }
    else
    {
        rightRect = CGRectOffset(rightRect, previewBox.origin.x, previewBox.origin.y);
    }
    
    return  rightRect;
}

- (CGRect)getMouthRectWithCIFaceFeature:(CIFaceFeature *)faceFeture FaceRect:(CGRect)faceRect PreviewBox:(CGRect)previewBox ScaleW:(CGFloat)scaleW ScaleH:(CGFloat)scaleH
{
    CGFloat mouthWidth = (faceRect.size.width / 2.5);
    
    CGFloat mouthHeight = ((mouthWidth / 16) * 9);

    CGRect mouthRect = CGRectMake((faceFeture.mouthPosition.y * scaleW) - (mouthWidth / 2), (faceFeture.mouthPosition.x * scaleH) - (mouthHeight / 2), mouthWidth, mouthHeight);

    if ([self.previewLayer.connection isVideoMirrored])
    {
        mouthRect = CGRectOffset(mouthRect, previewBox.origin.x + previewBox.size.width - mouthRect.size.width - (mouthRect.origin.x * 2), previewBox.origin.y);
    }
    else
    {
        mouthRect = CGRectOffset(mouthRect, previewBox.origin.x, previewBox.origin.y);
    }
    
    return  mouthRect;
}

- (CGRect)getCupRectWithCIFaceFeature:(CIFaceFeature *)faceFeture Angle:(CGFloat)angle FaceRect:(CGRect)faceRect LeftRect:(CGRect)leftRect RightRect:(CGRect)rightRect
{
    CGFloat cupWidth = (CGRectGetWidth(faceRect) / 1);
    
    CGFloat cupHeight = ((cupWidth / 100) * 100);//(CGRectGetHeight(faceRect) / 2);
    
    CGPoint p1 = CGPointMake(CGRectGetMidX(leftRect), CGRectGetMidY(leftRect));
    
    CGPoint p2 = CGPointMake(CGRectGetMidX(rightRect), CGRectGetMidY(rightRect));

    CGPoint twoEyeCenterPoint = CGPointMake(((p1.x + p2.x) / 2), ((p1.y + p2.y) / 2));

    CGFloat r = (CGRectGetHeight(faceRect) / 2);

    CGFloat radians = Degrees_To_Radians((angle - 180));

    CGFloat x1 = twoEyeCenterPoint.x + (sin(radians) * r);
    CGFloat y1 = twoEyeCenterPoint.y + (cos(radians) * r);
 
    CGRect cupRect = CGRectMake(x1 - (cupWidth / 2), y1 - (cupHeight / 2), cupWidth, cupHeight);
    
    return  cupRect;
}

- (CGFloat)getFaceAngleWithCIFaceFeature:(CIFaceFeature *)faceFeture LeftRect:(CGRect)leftRect RightRect:(CGRect)rightRect
{
    CGFloat angle = 0;

    if (faceFeture.hasFaceAngle)
    {
        angle = faceFeture.faceAngle;
    }
    else
    {
        float width = (leftRect.origin.x - rightRect.origin.x);

        float height = (rightRect.origin.y - leftRect.origin.y);

        angle = Radians_To_Degrees(atanf(height/width));
    }
    
    return angle;
}

- (UIImage *)getRotateImageWithAngle:(CGFloat)angle
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:self.cupLayerImage];

    CIFilter *filter = [CIFilter filterWithName:@"CIAffineTransform" keysAndValues:kCIInputImageKey, ciImage, nil];

    [filter setDefaults];

    if (self.fdmCameraPosition == FDM_CAMERA_POSITION_BACK)
    {
        angle *= -1;
    }
    
    CGAffineTransform transform = CGAffineTransformMakeRotation((angle * M_PI / 180.0));

    [filter setValue:[NSValue valueWithBytes:&transform objCType:@encode(CGAffineTransform)] forKey:@"inputTransform"];

    CIContext *context = [CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer : @(NO)}];

    CIImage *outputImage = [filter outputImage];

    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];

    UIImage *result = [UIImage imageWithCGImage:cgImage];

    CGImageRelease(cgImage);
    
    return result;
}

- (void)drawFaces:(NSArray *)features forVideoBox:(CGRect)clearAperture orientation:(UIDeviceOrientation)orientation CIImage:(CIImage *)ciImage
{
    int currentFace = 0;
     
    CGSize parentFrameSize = [self.previewView frame].size;
    
    NSString *gravity = [self.previewLayer videoGravity];
        
    CGRect previewBox = [FaceDetectionManager videoPreviewBoxForGravity:gravity frameSize:parentFrameSize apertureSize:clearAperture.size];

//    if (!updateFrame)
//        return;
//
//    updateFrame = NO;
    
//    [CATransaction begin];
//
//    [CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];

    for (NSInteger i = features.count; i < self.faceLayerArray.count ; i++)
    {
        CALayer *layer1 = self.faceLayerArray[i];
        CALayer *layer2 = self.leftEyeLayerArray[i];
        CALayer *layer3 = self.rightEyeLayerArray[i];
        CALayer *layer4 = self.mouthLayerArray[i];
        CALayer *layer5 = self.cupLayerArray[i];
        
        [layer1 setHidden:YES];
        [layer2 setHidden:YES];
        [layer3 setHidden:YES];
        [layer4 setHidden:YES];
        [layer5 setHidden:YES];
    }
    
    CGFloat widthScaleBy = previewBox.size.width / clearAperture.size.height;
    
    CGFloat heightScaleBy = previewBox.size.height / clearAperture.size.width;
    
    NSMutableArray *featureArray = [NSMutableArray new];
    
    for (CIFaceFeature *ff in features)
    {
        CGRect faceRect = [self getFaceRectWithCIFaceFeature:ff PreviewBox:previewBox ScaleW:widthScaleBy ScaleH:heightScaleBy];
        
        NSMutableDictionary *dic = [NSMutableDictionary new];

        [dic setObject:[NSNumber numberWithFloat:faceRect.origin.x] forKey:@"faceRect"];
        
        [dic setObject:ff forKey:@"feature"];
        
        [featureArray addObject:dic];
    }
    
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"faceRect" ascending:YES selector:@selector(compare:)];
       
    [featureArray sortUsingDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    
    for (NSDictionary *dic in featureArray)
    {
        CIFaceFeature *ff = dic[@"feature"];
        
        CGRect faceRect = [self getFaceRectWithCIFaceFeature:ff PreviewBox:previewBox ScaleW:widthScaleBy ScaleH:heightScaleBy];
        
        CGRect leftRect = [self getLeftEyeRectWithCIFaceFeature:ff FaceRect:faceRect PreviewBox:previewBox ScaleW:widthScaleBy ScaleH:heightScaleBy];
        
        CGRect rightRect = [self getRightEyeRectWithCIFaceFeature:ff FaceRect:faceRect PreviewBox:previewBox ScaleW:widthScaleBy ScaleH:heightScaleBy];
    
        CGRect mouthRect = [self getMouthRectWithCIFaceFeature:ff FaceRect:faceRect PreviewBox:previewBox ScaleW:widthScaleBy ScaleH:heightScaleBy];

        CGFloat angle = [self getFaceAngleWithCIFaceFeature:ff LeftRect:leftRect RightRect:rightRect];

        CGRect cupRect = [self getCupRectWithCIFaceFeature:ff Angle:angle FaceRect:faceRect LeftRect:leftRect RightRect:rightRect];

        NSInteger index = [featureArray indexOfObject:dic];

        if (![self checkNan:cupRect])
        {
            if (index >= self.cupLayerArray.count)
            {
                if (index < MAX_DETECT_COUNT)
                {
                    CALayer *layer = [[CALayer alloc] init];

                    layer.contents = (__bridge id)self.cupLayerImage.CGImage;

                    layer.name = [NSString stringWithFormat:@"%@_%ld", CUP_LAYER, index];

                    layer.transform = CATransform3DIdentity;
                    
                    layer.frame = cupRect;
                    
                    CATransform3D transform = CATransform3DIdentity;
                    
                    layer.transform = CATransform3DRotate(transform, Degrees_To_Radians(-angle * 2), 0, 0, 1);
                    
                    [self.cupLayerArray addObject:layer];

                    [self.previewLayer addSublayer:layer];

                    if (self.cupLayerImage == nil)
                    {
                        layer.borderWidth = 1;

                        layer.borderColor = [UIColor greenColor].CGColor;
                    }

                    layer.hidden = !isShowCupFrame;
                }
            }
            else
            {
                CALayer *layer = self.cupLayerArray[index];

                layer.contents = (__bridge id)self.cupLayerImage.CGImage;
                
                layer.transform = CATransform3DIdentity;
                
                layer.frame = cupRect;
                
                layer.transform = CATransform3DMakeRotation(Degrees_To_Radians(-angle * 2), 0, 0, 1);
                
                layer.hidden = !isShowCupFrame;
            }
        }
        
        if (![self checkNan:leftRect])
        {
            if (index >= self.leftEyeLayerArray.count)
            {
                if (index < MAX_DETECT_COUNT)
                {
                    CALayer *layer = [[CALayer alloc] init];

                    layer.contents = (__bridge id)self.leftEyeLayerImage.CGImage;

                    layer.name = [NSString stringWithFormat:@"%@_%ld", LEFT_EYE_LAYER, index];

                    layer.frame = leftRect;

                    [self.leftEyeLayerArray addObject:layer];

                    [self.previewLayer addSublayer:layer];

                    if (self.leftEyeLayerImage == nil)
                    {
                        layer.borderWidth = 1;

                        layer.borderColor = [UIColor brownColor].CGColor;
                    }

                    layer.hidden = !isShowLeftEyeFrame;
                }
            }
            else
            {
                CALayer *layer = self.leftEyeLayerArray[index];

                layer.frame = leftRect;

                layer.hidden = !isShowLeftEyeFrame;
            }
        }
        
        if (![self checkNan:rightRect])
        {
            if (index >= self.rightEyeLayerArray.count)
            {
                if (index < MAX_DETECT_COUNT)
                {
                    CALayer *layer = [[CALayer alloc] init];

                    layer.contents = (__bridge id)self.rightEyeLayerImage.CGImage;

                    layer.name = [NSString stringWithFormat:@"%@_%ld", RIGHT_EYE_LAYER, index];

                    layer.frame = rightRect;

                    [self.rightEyeLayerArray addObject:layer];

                    [self.previewLayer addSublayer:layer];

                    if (self.rightEyeLayerImage == nil)
                    {
                        layer.borderWidth = 1;

                        layer.borderColor = [UIColor blueColor].CGColor;
                    }

                    layer.hidden = !isShowRightEyeFrame;
                }
            }
            else
            {
                CALayer *layer = self.rightEyeLayerArray[index];

                layer.frame = rightRect;

                layer.hidden = !isShowRightEyeFrame;
            }
        }

        if (![self checkNan:mouthRect])
        {
            if (index >= self.mouthLayerArray.count)
            {
                if (index < MAX_DETECT_COUNT)
                {
                    CALayer *layer = [[CALayer alloc] init];

                    layer.contents = (__bridge id)self.mouthLayerImage.CGImage;

                    layer.name = [NSString stringWithFormat:@"%@_%ld", MOUTH_LAYER, index];

                    layer.frame = mouthRect;

                    [self.mouthLayerArray addObject:layer];

                    [self.previewLayer addSublayer:layer];

                    if (self.mouthLayerImage == nil)
                    {
                        layer.borderWidth = 1;

                        layer.borderColor = [UIColor greenColor].CGColor;
                    }

                    layer.hidden = !isShowMouthFrame;
                }
            }
            else
            {
                CALayer *layer = self.mouthLayerArray[index];

                layer.frame = mouthRect;

                layer.hidden = !isShowMouthFrame;
            }
        }
        
        if (![self checkNan:faceRect])
        {
            if (index >= self.faceLayerArray.count)
            {
                if (index < MAX_DETECT_COUNT)
                {
                    CALayer *layer = [[CALayer alloc] init];

                    layer.contents = (__bridge id)self.faceLayerImage.CGImage;

                    layer.name = [NSString stringWithFormat:@"%@_%ld", FACE_LAYER, index];

                    layer.frame = faceRect;

                    [self.faceLayerArray addObject:layer];

                    [self.previewLayer addSublayer:layer];

                    if (self.faceLayerImage == nil)
                    {
                        layer.borderWidth = 1;

                        layer.borderColor = [UIColor greenColor].CGColor;
                    }

                    layer.hidden = !isShowFaceFrame;
                }
            }
            else
            {
                CALayer *layer = self.faceLayerArray[index];

                if (self.faceLayerImage == nil)
                {
                    layer.borderWidth = 1;

                    layer.borderColor = [UIColor greenColor].CGColor;
                }
                else
                {
                    layer.borderWidth = 0;
                    
                    layer.contents = (__bridge id)self.faceLayerImage.CGImage;
                }
                
                layer.frame = faceRect;

                layer.hidden = !isShowFaceFrame;
            }
        }

//        for (CALayer *layer in self.previewLayer.sublayers)
//        {
//            if ([[layer name] hasPrefix:FACE_LAYER] || [[layer name] hasPrefix:LEFT_EYE_LAYER] || [[layer name] hasPrefix:RIGHT_EYE_LAYER] || [[layer name] hasPrefix:MOUTH_LAYER] || [[layer name] hasPrefix:CUP_LAYER])
//            {
//                switch (orientation)
//                {
//                    case UIDeviceOrientationPortrait:
//                        [layer setAffineTransform:CGAffineTransformMakeRotation(0 * M_PI / 180)];
//                        break;
//                    case UIDeviceOrientationPortraitUpsideDown:
//                        [layer setAffineTransform:CGAffineTransformMakeRotation(180 * M_PI / 180)];
//                        break;
//                    case UIDeviceOrientationLandscapeLeft:
//                        [layer setAffineTransform:CGAffineTransformMakeRotation(90 * M_PI / 180)];
//                        break;
//                    case UIDeviceOrientationLandscapeRight:
//                        [layer setAffineTransform:CGAffineTransformMakeRotation(-90 * M_PI / 180)];
//                        break;
//                    case UIDeviceOrientationFaceUp:
//                    case UIDeviceOrientationFaceDown:
//                    default:
//                        break;
//                }
//            }
//        }

//        [CATransaction commit];

        currentFace++;
    }

//    [CATransaction commit];
    
    if ([_delegate respondsToSelector:@selector(FaceDetectionManagerIsDetectedFace:)] && currentFace > 0)
    {
        __weak typeof(self) weakSelf = self;
        
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [weakSelf.delegate FaceDetectionManagerIsDetectedFace:weakSelf];
        });
    }
}

#pragma mark - Utilities

- (void)setFaceLayerImage:(UIImage *)faceLayerImage
{
    _faceLayerImage = faceLayerImage;
    
    for (CALayer *layer in _faceLayerArray)
    {
        if (_faceLayerImage == nil)
        {
            layer.borderWidth = 1;

            layer.borderColor = [UIColor greenColor].CGColor;
        }
        else
        {
            layer.borderWidth = 0;
            
            layer.contents = (__bridge id)self.faceLayerImage.CGImage;
        }
    }
}

- (BOOL)checkNan:(CGRect)rect
{
    if (!isnan(rect.origin.x) && !isnan(rect.origin.y) && !isnan(rect.size.width) && !isnan(rect.size.height))
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

#pragma mark - AVCapturePhotoCaptureDelegate

//- (void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(nullable CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(nullable CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(nullable AVCaptureBracketedStillImageSettings *)bracketSettings error:(nullable NSError *)error
//{
//    NSData *data = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
//
//    UIImage *image = [UIImage imageWithData:data];
//
//    if ([_delegate respondsToSelector:@selector(faceDetectionManagerCaptureOutput:)])
//    {
//        __weak typeof(self) weakSelf = self;
//
//        dispatch_async(dispatch_get_main_queue(), ^
//        {
//            [weakSelf.delegate faceDetectionManagerCaptureOutput:image];
//        });
//    }
//
//    NSLog(@"1111");
//}

- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(nullable NSError *)error
{
    NSData *data = [photo fileDataRepresentation];
    
    UIImage *image = [UIImage imageWithData:data];
    
    if ([_delegate respondsToSelector:@selector(faceDetectionManagerCaptureOutput:)])
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            __weak typeof(self) weakSelf = self;
            
            [weakSelf.delegate faceDetectionManagerCaptureOutput:image];
        });
    }
}

#pragma mark - AVCaptureVideoDataOutputSampleBufferDelegate function

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    
    CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(__bridge NSDictionary *)attachments];
    
    if (attachments)
    {
        CFRelease(attachments);
    }
    
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];

    NSDictionary *imageOptions = [NSDictionary dictionaryWithObject:[self exifOrientation:curDeviceOrientation] forKey:CIDetectorImageOrientation];

    NSArray *features = [self.faceDetector featuresInImage:ciImage options:imageOptions];

    CMFormatDescriptionRef fdesc = CMSampleBufferGetFormatDescription(sampleBuffer);
    
    CGRect cleanAperture = CMVideoFormatDescriptionGetCleanAperture(fdesc, false /*originIsTopLeft == false*/);
    
    dispatch_async(dispatch_get_main_queue(), ^(void)
    {
        [self drawFaces:features forVideoBox:cleanAperture orientation:curDeviceOrientation CIImage:ciImage];
    });
}

@end


