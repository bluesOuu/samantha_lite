//
//  Utilities.h
//  HiroiaBleSample
//
//  Created by BluesOu on 2019/8/8.
//

#import <Foundation/Foundation.h>
#import "Utilities_LoadMedia.h"
#import "Utilities_Device.h"
#import <UIKit/UIKit.h>

typedef enum
{
    ANI_FADE,
    ANI_PUSH,
    ANI_MOVE_IN,
    ANI_REVEAL,
    ANI_FLIP,           //上下翻轉效果
    ANI_CUBE,           //立方體效果
    ANI_PAGE_CURL,
    ANI_PAGE_UNCURL,
    ANI_RIPPLE_EFFECT,  //滴水效果
    ANI_SUCK_EFFECT,    //收縮效果，如一塊布被抽走
    ANI_CAMERA_IRIS_HOLLOW_OPEN,
    ANI_CAMERA_IRIS_HOLLOW_CLOSE,
    
}_ANI_TYPE;

typedef enum
{
    ANI_RIGHT,
    ANI_LEFT,
    ANI_TOP,
    ANI_BOTTOM,
    
}_ANI_DIRECTION;

#pragma mark - iOS Device

#define PHONE_WIDTH_FOR_INCH_3_5                    320    // iPhone 4
#define PHONE_HEIGHT_FOR_INCH_3_5                   480

#define PHONE_WIDTH_FOR_INCH_4_0                    320    // iPhone 5
#define PHONE_HEIGHT_FOR_INCH_4_0                   568
#define PHONE_SCALE_FOR_INCH_4_0                    1

#define PHONE_WIDTH_FOR_INCH_4_7                    375    // iPhone 6
#define PHONE_HEIGHT_FOR_INCH_4_7                   667
#define PHONE_SCALE_FOR_INCH_4_7                    1.17185

#define PHONE_WIDTH_FOR_INCH_5_5                    414    // iPhone 6 Plus
#define PHONE_HEIGHT_FOR_INCH_5_5                   736
#define PHONE_SCALE_FOR_INCH_5_5                    1.29375


#define PAD_WIDTH_FOR_FLOW                          768
#define PAD_HEIGHT_FOR_FLOW                         1024
#define PAD_SCALE_FOR_INCH10                        1

#define PAD_WIDTH_FOR_INCH12                        1024
#define PAD_HEIGHT_FOR_INCH12                       1366
#define PAD_SCALE_FOR_INCH12                        1.33333

#define PHONE_SEZE_S                                1
#define PHONE_SEZE_M                                2
#define PHONE_SEZE_L                                3


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_RETINA                                   ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0))


@interface Utilities : NSObject

#pragma mark - Font Size

+ (float)getFontSize;

+ (int)getPhoneSize;

#pragma mark - UI SCreen Size

+ (CGSize)getPadSizeFromFlowSize:(CGSize)size;

#pragma mark - Add Shadow

+ (void)setShadow:(CALayer *)layer Opacity:(float)opacity Radius:(float)radius Offset:(CGSize)size Color:(UIColor *)color;

#pragma mark - Animation

+ (void)addAnimation:(_ANI_TYPE)aniType Direction:(_ANI_DIRECTION)aniDirect Duration:(double)duration onTheView:(id)object Completion:(void(^)(void))completion;

+ (BOOL)isNullOrNil:(id)object;

#pragma mark - Delay Queue

+ (void)delayBlockWith:(float)delayTime Queue:(dispatch_queue_t)queue Block:(void(^)(void))delayBlock;

+ (void)runInMainThread:(void(^)(void))delayBlock;

#pragma mark - Change

+ (NSDate *)stringChangeToDate:(NSString *)strDate;

+ (NSString *)dateChangeToString:(NSDate *)date;

+ (NSString *)productNameToImageImage:(NSString *)productName;

+ (BOOL)checkInputChar:(NSString *)string withThisCase:(NSString *)caseString;

+ (BOOL)isToday:(NSDate *)checkDate;

+ (BOOL)isToYear:(NSDate *)checkDate;

+ (NSMutableAttributedString *)createAttrStrWith:(NSString *)string Font:(UIFont *)font Color:(UIColor  *)color;



@end
