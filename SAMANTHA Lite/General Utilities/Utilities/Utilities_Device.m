//
//  Utilities_Device.m
//  12Ours
//
//  Created by Blues on 2018/7/20.
//  Copyright © 2018年 12Ours. All rights reserved.
//

#import "Utilities_Device.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <sys/utsname.h>

@implementation Utilities_Device

#pragma mark - Get App Info

+ (NSString *)getAppVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+ (NSString *)getBuildVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+ (NSString *)getAppName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
}

#pragma mark - SSID Name

+ (NSString *)getSSIDName
{
    NSString *wifiName = nil;
    
    NSArray *interFaceNames = (__bridge_transfer id)CNCopySupportedInterfaces();
    
    for (NSString *name in interFaceNames)
    {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)name);
        
        NSLog(@"info = %@",info);
        
        if (info[@"SSID"])
        {
            wifiName = info[@"SSID"];
        }
    }
    
    return wifiName;
}

#pragma mark - iDevice Name

+ (NSString *)getDeviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    static NSDictionary *deviceNamesByCode = nil;
    
    if (!deviceNamesByCode)
    {
        deviceNamesByCode = @
        {
            // iphone
            @"iPhone1,1"      :@"iPhone 1G",
            @"iPhone1,2"      :@"iPhone 3G",
            
            @"iPhone2,1"      :@"iPhone 3GS",
            
            @"iPhone3,1"      :@"iPhone 4 (GSM)",
            @"iPhone3,2"      :@"iPhone 4 (GSM)",
            @"iPhone3,3"      :@"iPhone 4 (CDMA",
            
            @"iPhone4,1"      :@"iPhone 4S",
            
            @"iPhone5,1"      :@"iPhone 5 (GSM)",
            @"iPhone5,2"      :@"iPhone 5 (GSM+CDMA)",
            @"iPhone5,3"      :@"iPhone 5c (GSM)",
            @"iPhone5,4"      :@"iPhone 5c (GSM+CDMA)",
            
            @"iPhone6,1"      :@"iPhone 5s (GSM)",
            @"iPhone6,2"      :@"iPhone 5s (GSM+CDMA)",
            
            @"iPhone7,1"      :@"iPhone 6 Plus",
            @"iPhone7,2"      :@"iPhone 6",
            
            @"iPhone8,1"      :@"iPhone 6s",
            @"iPhone8,2"      :@"iPhone 6s Plus",
            @"iPhone8,4"      :@"iPhone SE",
            
            @"iPhone9,1"      :@"iPhone 7 (CDMA+GSM/LTE)",
            @"iPhone9,2"      :@"iPhone 7 Plus (CDMA+GSM/LTE)",
            @"iPhone9,3"      :@"iPhone 7 (GSM/LTE)",
            @"iPhone9,4"      :@"iPhone 7 Plus  (GSM/LTE)",
            
            @"iPhone10,1"      :@"iPhone 8 (CDMA+GSM/LTE)",
            @"iPhone10,2"      :@"iPhone 8 Plus (CDMA+GSM/LTE)",
            @"iPhone10,3"      :@"iPhone X (CDMA+GSM/LTE)",
            @"iPhone10,4"      :@"iPhone 8 (GSM/LTE)",
            @"iPhone10,5"      :@"iPhone 8 Plus (GSM/LTE)",
            @"iPhone10,6"      :@"iPhone X (GSM/LTE)",
            
            @"iPhone11,2"      :@"iPhone XS",
            @"iPhone11,4"      :@"iPhone XS Max",
            @"iPhone11,6"      :@"iPhone XS Max",
            @"iPhone11,8"      :@"iPhone XR",
            
            @"iPhone12,1"      :@"iPhone 11",
            @"iPhone12,3"      :@"iPhone 11 Pro",
            @"iPhone12,5"      :@"iPhone 11 Pro Max",
            @"iPhone12,8"      :@"iPhone SE 2nd Gen",
            
            @"iPhone13,1"      :@"iPhone 12 mini",
            @"iPhone13,2"      :@"iPhone 12",
            @"iPhone13,3"      :@"iPhone 12 Pro",
            @"iPhone13,4"      :@"iPhone 12 Pro Maxx",
            
            // ipod
            @"iPod1,1"        :@"iPod Touch 1G",
            @"iPod2,1"        :@"iPod Touch 2G",
            @"iPod3,1"        :@"iPod Touch 3G",
            @"iPod4,1"        :@"iPod Touch 4G",
            @"iPod5,1"        :@"iPod Touch 5G",
            @"iPod7,1"        :@"iPod Touch 6G",
            @"iPod9,1"        :@"iPod Touch7G",
            
            // ipad
            @"iPad1,1"        :@"iPad",
            @"iPad1,2"        :@"iPad (GSM)",
            
            @"iPad2,1"        :@"iPad 2 (WiFi)",
            @"iPad2,2"        :@"iPad 2 (GSM)",
            @"iPad2,3"        :@"iPad 2 (CDMA)",
            @"iPad2,4"        :@"iPad 2 (WiFi)",
            @"iPad2,5"        :@"iPad Mini (WiFi)",
            @"iPad2,6"        :@"iPad Mini (GSM)",
            @"iPad2,7"        :@"iPad Mini (GSM+CDMA)",
            
            @"iPad3,1"        :@"iPad 3 (WiFi)",
            @"iPad3,2"        :@"iPad 3 (GSM+CDMA)",
            @"iPad3,3"        :@"iPad 3 (GSM)",
            @"iPad3,4"        :@"iPad 4 (WiFi)",
            @"iPad3,5"        :@"iPad 4 (GSM)",
            @"iPad3,6"        :@"iPad 4 (GSM+CDMA)",
            
            @"iPad4,1"        :@"iPad Air (WiFi)",
            @"iPad4,2"        :@"iPad Air (Cellular)",
            @"iPad4,4"        :@"iPad mini 2 (WiFi)",
            @"iPad4,5"        :@"iPad mini 2 (Cellular)",
            @"iPad4,6"        :@"iPad mini 2 (China)",
            @"iPad4,7"        :@"iPad mini 3 (WiFi)",
            @"iPad4,8"        :@"iPad mini 3 (LTE)",
            @"iPad4,9"        :@"iPad mini 3 (china)",
            
            @"iPad5,1"        :@"iPad mini 4 (WiFi)",
            @"iPad5,2"        :@"iPad mini 4 (Cellular)",
            @"iPad5,3"        :@"iPad Air 2 (WiFi)",
            @"iPad5,4"        :@"iPad Air 2 (Cellular)",
            
            @"iPad6,3"        :@"iPad Pro 9.7 (Wi‑Fi)",
            @"iPad6,4"        :@"iPad Pro 9.7 (LTE)",
            @"iPad6,7"        :@"iPad Pro 12.9 (Wi‑Fi)",
            @"iPad6,8"        :@"iPad Pro 12.9 (LTE)",
            @"iPad6,11"       :@"iPad 5 (WiFi)",
            @"iPad6,12"       :@"iPad 5 (Cellular)",
            
            @"iPad7,1"        :@"iPad Pro 12.9 (2nd Gen)(Wi‑Fi)",
            @"iPad7,2"        :@"iPad Pro 12.9 (2nd Gen)(LTE)",
            @"iPad7,3"        :@"iPad Pro 10.5 (Wi‑Fi)",
            @"iPad7,4"        :@"iPad Pro 10.5 (LTE)",
            @"iPad7,5"        :@"iPad 6 (WiFi)",
            @"iPad7,6"        :@"iPad 6 (Cellular)",
            @"iPad7,11"       :@"iPad 7 (WiFi)",
            @"iPad7,12"       :@"iPad 7 (Cellular)",
            
            @"iPad8,1"        :@"iPad Pro 11",
            @"iPad8,2"        :@"iPad Pro 11",
            @"iPad8,3"        :@"iPad Pro 11",
            @"iPad8,4"        :@"iPad Pro 11",
            @"iPad8,5"        :@"iPad Pro 12.9 (3nd Gen)",
            @"iPad8,6"        :@"iPad Pro 12.9 (3nd Gen)",
            @"iPad8,7"        :@"iPad Pro 12.9 (3nd Gen)",
            @"iPad8,8"        :@"iPad Pro 12.9 (3nd Gen)",
            
            @"iPad11,1"       :@"iPad mini 5 (WiFi)",
            @"iPad11,2"       :@"iPad mini 5 (Cellular)",
            @"iPad11,3"       :@"iPad Air 3 (WiFi)",
            @"iPad11,4"       :@"iPad Air 3 (Cellular)",
            
            
            // simulator
            @"i386"           :@"Simulator i386",
            @"x86_64"         :@"Simulator x86_64"
        };
    }
    
    NSString *deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName)
    {
        if ([code rangeOfString:@"iPod"].location != NSNotFound)
        {
            deviceName = @"iPod";
        }
        else if ([code rangeOfString:@"iPad"].location != NSNotFound)
        {
            deviceName = @"iPad";
        
        } else if ([code rangeOfString:@"iPhone"].location != NSNotFound)
        {
            deviceName = @"iPhone";
        }
        else
        {
            deviceName = [NSString stringWithFormat:@"iDevice"];
        }
    }
    
    NSLog(@"deviceCode = %@", code);
    
    NSLog(@"deviceName = %@", deviceName);
    
    return deviceName;
}

@end
