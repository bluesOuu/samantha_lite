//
//  Utilities_LoadImage.m
//  12Ours
//
//  Created by Blues on 2018/7/20.
//  Copyright © 2018年 12Ours. All rights reserved.
//

#import "Utilities_LoadMedia.h"
#import <AVFoundation/AVFoundation.h>

@implementation Utilities_LoadMedia

+ (void)loadFromAlbumType:(_LOAD_MEDIA_TYPE)mediaType CanEdit:(BOOL)canEdit Delegate:(id)delegate
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = delegate;
    
    imagePicker.allowsEditing = canEdit;
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (mediaType == LOAD_MEDIA_TYPE_IMAGE)
    {
        imagePicker.mediaTypes = [NSArray arrayWithObjects:@"public.image", nil];
    }
    else if (mediaType == LOAD_MEDIA_TYPE_VIDEO)
    {
        imagePicker.mediaTypes = [NSArray arrayWithObjects:@"public.movie", nil];
    }
    
    [delegate presentViewController:imagePicker animated:YES completion:nil];
}

+ (void)loadFromCamType:(_LOAD_MEDIA_TYPE)mediaType CanEdit:(BOOL)canEdit Delegate:(id)delegate
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(authStatus == AVAuthorizationStatusAuthorized || authStatus == AVAuthorizationStatusNotDetermined)
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        imagePicker.delegate = delegate;
        
        imagePicker.allowsEditing = canEdit;
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if (mediaType == LOAD_MEDIA_TYPE_IMAGE)
        {
            imagePicker.mediaTypes = [NSArray arrayWithObjects:@"public.image", nil];
        }
        else if (mediaType == LOAD_MEDIA_TYPE_VIDEO)
        {
            imagePicker.mediaTypes = [NSArray arrayWithObjects:@"public.movie", nil];
        }
        
        [delegate presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        void (^alertHander)(UIAlertAction *) = ^void(UIAlertAction *action)
        {
            if ([action.title isEqualToString:@"Ok"])
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                
                if ([[UIApplication sharedApplication] canOpenURL:url])
                {
                    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                }
            }
        };
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera permission is not allow" message:nil preferredStyle:UIAlertControllerStyleAlert];
        
        NSArray *buttonArray = [NSArray arrayWithObjects:@"Ok", @"Cancel", nil];
        
        for(NSString *buttonTitle in buttonArray)
        {
            UIAlertActionStyle style = UIAlertActionStyleDefault;
            
            if ([[buttonArray lastObject] isEqualToString:buttonTitle])
            {
                style = UIAlertActionStyleCancel;
            }
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:buttonTitle style:style handler:alertHander];
            
            [alertController addAction:action];
        }

        [delegate presentViewController:alertController animated:YES completion:nil];
    }
}

@end
