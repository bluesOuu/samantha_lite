//
//  GeneralUtilities.m
//  AO Common App
//
//  Created by Admin on 2015/12/29.
//  Copyright (c) 2015年 ASIA OPTICAL CO., INC. All rights reserved.

#import "Utilities.h"
#import <AVFoundation/AVFoundation.h>

@implementation Utilities

#pragma mark - Font Size

+ (float)getFontSize
{
    if ([[UIScreen mainScreen] bounds].size.width == PHONE_WIDTH_FOR_INCH_4_0)
        return 14;
    else if ([[UIScreen mainScreen] bounds].size.width == PHONE_WIDTH_FOR_INCH_4_7)
        return 15;
    else if ([[UIScreen mainScreen] bounds].size.width == PHONE_WIDTH_FOR_INCH_5_5)
        return 16;
    else
        return 16;
}

+ (int)getPhoneSize
{
    if ([[UIScreen mainScreen] bounds].size.width == PHONE_WIDTH_FOR_INCH_4_0)
        return PHONE_SEZE_S;
    else if ([[UIScreen mainScreen] bounds].size.width == PHONE_WIDTH_FOR_INCH_4_7)
        return PHONE_SEZE_M;
    else if ([[UIScreen mainScreen] bounds].size.width == PHONE_WIDTH_FOR_INCH_5_5)
        return PHONE_SEZE_L;
    else
        return PHONE_SEZE_M;
}

#pragma mark - UI SCreen Size

+ (CGSize)getPadSizeFromFlowSize:(CGSize)size
{
    CGFloat ratio = 0;
    
    if([[UIScreen mainScreen] bounds].size.width < [[UIScreen mainScreen] bounds].size.height)
    {
        ratio = ([[UIScreen mainScreen] bounds].size.width / PAD_WIDTH_FOR_FLOW);
    }
    else
    {
        ratio = ([[UIScreen mainScreen] bounds].size.height / PAD_WIDTH_FOR_FLOW);
    }
    
    CGFloat width = (size.width * ratio) + 0.5;
    
    CGFloat height = (size.height * ratio) + 0.5;
    
    int w = width;
    
    int h = height;
    
    return CGSizeMake(w, h);
}

#pragma mark - Add Shadow

+ (void)setShadow:(CALayer *)layer Opacity:(float)opacity Radius:(float)radius Offset:(CGSize)size Color:(UIColor *)color
{
    layer.shadowOpacity = opacity;
    
    layer.shadowRadius = radius;
    
    layer.shadowOffset = size;
    
    layer.shadowColor = color.CGColor;
}

#pragma mark - Animation

+ (void)addAnimation:(_ANI_TYPE)aniType Direction:(_ANI_DIRECTION)aniDirect Duration:(double)duration onTheView:(id)object Completion:(void(^)(void))completion
{
    CATransition* transition = [CATransition animation];
    
    [transition setDuration:duration];
    
    switch (aniDirect)
    {
        case ANI_RIGHT:
        {
            transition.subtype = kCATransitionFromRight;
            break;
        }
        case ANI_LEFT:
        {
            transition.subtype = kCATransitionFromLeft;
            break;
        }
        case ANI_TOP:
        {
            transition.subtype = kCATransitionFromTop;
            break;
        }
        case ANI_BOTTOM:
        {
            transition.subtype = kCATransitionFromBottom;
            break;
        }
    }
    
    switch (aniType)
    {
        case ANI_FADE:
        {
            transition.type = kCATransitionFade;
            break;
        }
        case ANI_PUSH:
        {
            transition.type = kCATransitionPush;
            break;
        }
        case ANI_MOVE_IN:
        {
            transition.type = kCATransitionMoveIn;
            break;
        }
        case ANI_REVEAL:
        {
            transition.type = kCATransitionReveal;
            break;
        }
        case ANI_FLIP:
        {
            transition.type = @"oglFlip";
            break;
        }
        case ANI_CUBE:
        {
            transition.type = @"cube";
            break;
        }
        case ANI_PAGE_CURL:
        {
            transition.type = @"pageCurl";
            break;
        }
        case ANI_PAGE_UNCURL:
        {
            transition.type = @"pageUnCurl";
            break;
        }
        case ANI_RIPPLE_EFFECT:
        {
            transition.type = @"rippleEffect";
            break;
        }
        case ANI_SUCK_EFFECT:
        {
            transition.type = @"suckEffect";
            break;
        }
        case ANI_CAMERA_IRIS_HOLLOW_OPEN:
        {
            transition.type = @"cameraIrisHollowOpen";
            break;
        }
        case ANI_CAMERA_IRIS_HOLLOW_CLOSE:
        {
            transition.type = @"cameraIrisHollowClose";
            break;
        }
        default:
            break;
    }
    
    [transition setFillMode:kCAFillModeBoth];
    
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [CATransaction setCompletionBlock:completion];
    
    if ([object isKindOfClass:[UIView class]])
    {
        UIView *view = object;
        
        [view.layer addAnimation:transition forKey:kCATransition];
    }
    else
    {
        [[UIApplication sharedApplication].delegate.window.layer addAnimation:transition forKey:kCATransition];
    }
}

+ (BOOL)isNullOrNil:(id)object
{
    return object == nil || object == (id)[NSNull null] || object == NULL || [object isEqualToString:@""] || [object isKindOfClass:[NSNull class]];
}

+ (void)delayBlockWith:(float)delayTime Queue:(dispatch_queue_t)queue Block:(void(^)(void))delayBlock
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), queue, delayBlock);
}

+ (void)runInMainThread:(void(^)(void))delayBlock
{
    dispatch_async(dispatch_get_main_queue(), delayBlock);
}

+ (NSDate *)stringChangeToDate:(NSString *)strDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    return [dateFormatter dateFromString:strDate];
}

+ (NSString *)dateChangeToString:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)productNameToImageImage:(NSString *)productName
{
    if ([productName isEqualToString:@"DRAKE 193"])
    {
        return @"bean1";
    }
    else if ([productName isEqualToString:@"Mexican Special Medium Roast"])
    {
        return @"bean2";
    }
    else if ([productName isEqualToString:@"Organic Shade-Grown Coffee"])
    {
        return @"bean3";
    }
    else if ([productName isEqualToString:@"MISTER COFFEE"])
    {
        return @"bean4";
    }
    else if ([productName isEqualToString:@"Mcnulty MINI"])
    {
        return @"bean5";
    }
    else if ([productName isEqualToString:@"Yirgacheffe"])
    {
        return @"bean6";
    }
    else if ([productName isEqualToString:@"Chocolate Cake"])
    {
        return @"cake1";
    }
    else if ([productName isEqualToString:@"Strawberry Cake"])
    {
        return @"cake2";
    }
    else //if ([productName isEqualToString:@"Yirgacheffe"])
    {
        return @"cake3";
    }
}

+ (BOOL)checkInputChar:(NSString *)string withThisCase:(NSString *)caseString
{
    const char *keyChar = [string cStringUsingEncoding:NSUTF8StringEncoding];
    
    int isBackSpace = strcmp(keyChar, "\b");
    
    if (isBackSpace == -8)
    {
        return YES;
    }
    
    NSCharacterSet *specialCharacterSet = [NSCharacterSet characterSetWithCharactersInString:caseString];
    
    if ([string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (BOOL)isToday:(NSDate *)checkDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[NSDate date]];
    
    NSDateComponents *selfCmps = [calendar components:unit fromDate:checkDate];
    
    return (selfCmps.year == nowCmps.year) && (selfCmps.month == nowCmps.month) && (selfCmps.day == nowCmps.day);
}

+ (BOOL)isToYear:(NSDate *)checkDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    int unit = NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    NSDateComponents *nowCmps = [calendar components:unit fromDate:[NSDate date]];
    
    NSDateComponents *selfCmps = [calendar components:unit fromDate:checkDate];
    
    return (selfCmps.year == nowCmps.year);
}

+ (NSMutableAttributedString *)createAttrStrWith:(NSString *)string Font:(UIFont *)font Color:(UIColor  *)color
{
    NSMutableAttributedString *attrStr = [string convertToAttrString];
    
    [attrStr setFont:font fontWithRange:NSMakeRange(0, attrStr.length)];
    
    [attrStr setTextColor:color colorWithRange:NSMakeRange(0, attrStr.length)];
    
    return attrStr;
}

@end
