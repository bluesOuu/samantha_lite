//
//  Utilities_Device.h
//  12Ours
//
//  Created by Blues on 2018/7/20.
//  Copyright © 2018年 12Ours. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities_Device : NSObject

#pragma mark - Get App Info
+ (NSString *)getAppVersion;
+ (NSString *)getBuildVersion;
+ (NSString *)getAppName;

#pragma mark - SSID Name
+ (NSString *)getSSIDName;

#pragma mark - iDevice Name
+ (NSString *)getDeviceName;

@end
