//
//  Utilities_LoadImage.h
//  12Ours
//
//  Created by Blues on 2018/7/20.
//  Copyright © 2018年 12Ours. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, _LOAD_MEDIA_TYPE)
{
    LOAD_MEDIA_TYPE_ALL,
    LOAD_MEDIA_TYPE_IMAGE,
    LOAD_MEDIA_TYPE_VIDEO,
};

@interface Utilities_LoadMedia : NSObject

+ (void)loadFromAlbumType:(_LOAD_MEDIA_TYPE)mediaType CanEdit:(BOOL)canEdit Delegate:(id)delegate;

+ (void)loadFromCamType:(_LOAD_MEDIA_TYPE)mediaType CanEdit:(BOOL)canEdit Delegate:(id)delegate;

@end
