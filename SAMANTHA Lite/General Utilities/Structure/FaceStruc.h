//
//  FaceStruc.h
//  HiroiaSDK
//
//  Created by Blues on 2020/3/23.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AutoBindObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface FaceStruc : AutoBindObject

@property (nonatomic, strong) NSString *faceID;
@property (nonatomic, assign) NSInteger age;
@property (nonatomic, assign) NSInteger temperature;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSDictionary *emotion;
@property (nonatomic, strong) NSData *imageData;

//@property (nonatomic, assign) float confidence1;
//@property (nonatomic, assign) float confidence2;
//@property (nonatomic, assign) float confidence3;
//
//@property (nonatomic, strong) NSString *name2;
//@property (nonatomic, strong) NSString *name3;

@property (nonatomic, strong) NSDictionary *faceRectangle;

@end

NS_ASSUME_NONNULL_END

