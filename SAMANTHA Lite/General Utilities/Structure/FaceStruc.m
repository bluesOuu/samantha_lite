//
//  FaceStruc.m
//  HiroiaSDK
//
//  Created by Blues on 2020/3/23.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "FaceStruc.h"

@implementation FaceStruc
@synthesize age;
@synthesize name;
//@synthesize name2;
//@synthesize name3;
@synthesize temperature;
@synthesize emotion;
@synthesize gender;
@synthesize imageData;
//@synthesize confidence1;
//@synthesize confidence2;
//@synthesize confidence3;
@synthesize faceRectangle;

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self)
    {
        age = -1;
        name = nil;
        temperature = 0;
        emotion = nil;
        gender = nil;
        imageData = nil;
    }
    
    return self;
}

@end
