//
//  UserStruc.h
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/22.
//

#import "AutoBindObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserStruc : AutoBindObject

@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSData *imageData;

@end

NS_ASSUME_NONNULL_END
