//
//  UserStruc.m
//  SAMANTHA Lite
//
//  Created by Blues on 2021/1/22.
//

#import "UserStruc.h"

@implementation UserStruc
@synthesize birthday;
@synthesize userName;
@synthesize email;
@synthesize imageUrl;
@synthesize imageData;

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    if (self)
    {
        birthday = nil;
        userName = nil;
        email = nil;
        imageUrl = nil;
        imageData = nil;
    }
    
    return self;
}

#pragma mark - Public API (overridable)

- (NSString *)propertyNameForKey:(NSString*)key
{
    if ([key isEqualToString:@"fbPhoto"])
        return @"imageUrl";
    
    return key;
}

@end
