//
//  BOUserInfo.m
//  BOUserInfo
//
//  Created by Blues on 2017/8/14.
//  Copyright © 2017年 BOUserInfo. All rights reserved.
//

#import "BOUserInfo.h"

@implementation BOUserInfo

#pragma mark - GetForKey

+ (NSString*)getStringForKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *getString = [userDefaults stringForKey:key];
    
    return getString;
}

+ (id)getObjectForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    id object = [userDefaults objectForKey:key];
    
    return object;
}

+ (NSArray *)getArrayForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *array = [userDefaults arrayForKey:key];
    
    return array;
}

+ (NSDictionary *)getDictionaryForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary *dictionary = [userDefaults dictionaryForKey:key];
    
    return dictionary;
}

+ (NSData *)getDataForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *data = [userDefaults dataForKey:key];
    
    return data;
}

+ (BOOL)getBooleanForKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL getBoolean = [userDefaults boolForKey:key];
    
    return getBoolean;
}

+ (NSInteger)getIntegerForKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSInteger getInteger = [userDefaults integerForKey:key];
    
    return getInteger;
}

+ (double)getDoubleForKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    double getDouble = [userDefaults doubleForKey:key];
    
    return getDouble;
}

+ (float)getFloatForKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    float getFloat = [userDefaults floatForKey:key];
    
    return getFloat;
}

+ (id)getStructureForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *data = [userDefaults objectForKey:key];
    
    id structure = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    return structure;
}

+ (UIImage *)getImageForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *data = [userDefaults objectForKey:key];
    
    return [UIImage imageWithData:data];
}

#pragma mark - RemoveForKey

+ (BOOL)removeForKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    
    return [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - SetForKey

+ (BOOL)setString:(NSString*)value forKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setObject:(id)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setArray:(NSArray*)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setDictionary:(NSDictionary*)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setData:(NSData*)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:value forKey:key];
    return [userDefaults synchronize];
}

+ (BOOL)setBoolean:(BOOL)value forKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setBool:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setInteger:(NSInteger)value forKey:(NSString*)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setInteger:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setDouble:(double)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setDouble:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setFloat:(float)value forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setFloat:value forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setStructure:(id)value forKey:(NSString *)key
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:data forKey:key];
    
    return [userDefaults synchronize];
}

+ (BOOL)setImage:(UIImage *)image forKey:(NSString *)key
{
    NSData *data = UIImagePNGRepresentation(image);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:data forKey:key];
    
    return [userDefaults synchronize];
}

@end
