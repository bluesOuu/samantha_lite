//
//  BOUserInfo.h
//  BOUserInfo
//
//  Created by Blues on 2017/8/14.
//  Copyright © 2017年 BOUserInfo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BOUserInfo : NSObject

#pragma mark - GetForKey

+ (id)getObjectForKey:(NSString *)key;
+ (BOOL)getBooleanForKey:(NSString*)key;
+ (float)getFloatForKey:(NSString*)key;
+ (double)getDoubleForKey:(NSString*)key;
+ (NSData *)getDataForKey:(NSString *)key;
+ (NSArray *)getArrayForKey:(NSString *)key;
+ (NSInteger)getIntegerForKey:(NSString*)key;
+ (NSString *)getStringForKey:(NSString*)key;
+ (NSDictionary *)getDictionaryForKey:(NSString *)key;
+ (id)getStructureForKey:(NSString *)key;
+ (UIImage *)getImageForKey:(NSString *)key;

#pragma mark - RemoveForKey

+ (BOOL)removeForKey:(NSString*)key;

#pragma mark - SetForKey

+ (BOOL)setString:(NSString*)value forKey:(NSString*)key;
+ (BOOL)setObject:(id)value forKey:(NSString *)key;
+ (BOOL)setArray:(NSArray*)value forKey:(NSString *)key;
+ (BOOL)setDictionary:(NSDictionary*)value forKey:(NSString *)key;
+ (BOOL)setData:(NSData*)value forKey:(NSString *)key;
+ (BOOL)setBoolean:(BOOL)value forKey:(NSString*)key;
+ (BOOL)setInteger:(NSInteger)value forKey:(NSString*)key;
+ (BOOL)setDouble:(double)value forKey:(NSString *)key;
+ (BOOL)setFloat:(float)value forKey:(NSString *)key;
+ (BOOL)setStructure:(id)value forKey:(NSString *)key;
+ (BOOL)setImage:(UIImage *)image forKey:(NSString *)key;

@end
