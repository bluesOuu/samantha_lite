//
//  NSMutableAttributedString+SettingMethod.h
//  PASESA Health
//
//  Created by Blues on 2016/5/31.
//  Copyright © 2016年 PASESA Health. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableAttributedString(SettingMethod)

- (NSMutableAttributedString *)setTextColor:(UIColor *)color colorWithRange:(NSRange)range;

- (NSMutableAttributedString *)setBackgroundColor:(UIColor *)color colorWithRange:(NSRange)range;

- (NSMutableAttributedString *)setLineSpacing:(float)spacing;

- (NSMutableAttributedString *)setKerning:(int)kerning;

- (NSMutableAttributedString *)setFont:(UIFont *)font fontWithRange:(NSRange)range;

- (NSMutableAttributedString *)setUnderLine:(int)lineThickness thicknessWithRange:(NSRange)range;

- (NSMutableAttributedString *)setDeleteLine:(int)lineThickness thicknessWithRange:(NSRange)range;

- (NSMutableAttributedString *)setShadowColor:(UIColor *)color BlurRadius:(float)blurRadius Offset:(CGSize)size;

- (NSMutableAttributedString *)setStrokeWidth:(int)width Color:(UIColor *)color;

@end
