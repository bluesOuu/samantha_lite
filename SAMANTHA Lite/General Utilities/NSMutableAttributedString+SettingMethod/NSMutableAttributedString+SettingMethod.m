//
//  NSMutableAttributedString+SettingMethod.m
//  PASESA Health
//
//  Created by Blues on 2016/5/31.
//  Copyright © 2016年 PASESA Health. All rights reserved.
//

#import "NSMutableAttributedString+SettingMethod.h"

@implementation NSMutableAttributedString(SettingMethod)

- (NSMutableAttributedString *)setTextColor:(UIColor *)color colorWithRange:(NSRange)range
{
    [self addAttribute:NSForegroundColorAttributeName value:color range:range];
    
    return self;
}

- (NSMutableAttributedString *)setBackgroundColor:(UIColor *)color colorWithRange:(NSRange)range
{
    [self addAttribute:NSBackgroundColorAttributeName value:color range:range];
    
    return self;
}

- (NSMutableAttributedString *)setLineSpacing:(float)spacing
{
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    
    [style setLineSpacing:spacing];
    
    [self addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [self length])];
    
    return self;
}

- (NSMutableAttributedString *)setKerning:(int)kerning
{
    [self addAttribute:NSKernAttributeName value:[NSNumber numberWithInt:kerning] range:NSMakeRange(0, [self length])];
    
    return self;
}

- (NSMutableAttributedString *)setFont:(UIFont *)font fontWithRange:(NSRange)range
{
    [self addAttribute:NSFontAttributeName value:font range:range];
    
    return self;
}

- (NSMutableAttributedString *)setUnderLine:(int)lineThickness thicknessWithRange:(NSRange)range
{
    [self addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:lineThickness] range:range];
    
    return self;
}

- (NSMutableAttributedString *)setDeleteLine:(int)lineThickness thicknessWithRange:(NSRange)range
{
    [self addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInt:lineThickness] range:range];
    
    return self;
}

- (NSMutableAttributedString *)setShadowColor:(UIColor *)color BlurRadius:(float)blurRadius Offset:(CGSize)size
{
    NSShadow *shadowDic=[[NSShadow alloc] init];
    
    [shadowDic setShadowBlurRadius:blurRadius];
    
    [shadowDic setShadowColor:color];
    
    [shadowDic setShadowOffset:size];
    
    [self addAttribute:NSShadowAttributeName value:shadowDic range:NSMakeRange(0, [self length])];
    
    return self;
}

- (NSMutableAttributedString *)setStrokeWidth:(int)width Color:(UIColor *)color
{
    [self addAttribute:NSStrokeColorAttributeName value:color range:NSMakeRange(0, [self length])];
    
    
    [self addAttribute:NSStrokeColorAttributeName value:[NSNumber numberWithInt:width] range:NSMakeRange(0, [self length])];
    
    return self;
}

@end
