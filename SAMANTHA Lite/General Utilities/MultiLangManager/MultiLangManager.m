//
//  MultiLangManager.m
//  SamanthaCloudSample
//
//  Created by Blues on 2019/12/12.
//

#import "MultiLangManager.h"

@implementation MultiLangManager

+ (NSString *)getLangStringForKey:(NSString *)key
{
    NSString *folderName = [[NSUserDefaults standardUserDefaults] stringForKey:BO_LANGUAGE_KEY];
    
    BOLLog(@"folderName = %@", folderName);
    
    if (folderName != nil)
    {
        NSString *path = [[NSBundle mainBundle] pathForResource:folderName ofType:FOLDER_SUB_NAME];
        
        NSBundle *bundle = [NSBundle bundleWithPath:path];
        
        if (bundle != nil)
        {
            return [bundle localizedStringForKey:key value:nil table:nil];
        }
        
        return NSLocalizedString(key, nil);
    }
    else
    {
        return NSLocalizedString(key, nil);
    }
}

+ (void)setLanguageWithFolderName:(NSString *)folderName
{
    [[NSUserDefaults standardUserDefaults] setObject:folderName forKey:BO_LANGUAGE_KEY];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setLanguageWith:(BO_LANG)langKey
{
    NSString *folderName = [MultiLangManager getFolderNameFromKey:langKey];
    
    [MultiLangManager setLanguageWithFolderName:folderName];
}

+ (void)resetDefaultLanguage
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BO_LANGUAGE_KEY];
}

+ (NSArray<NSString *> *)getSupportLanguageList
{
    NSMutableArray *allLanguageList = [NSMutableArray new];
    
    for (int i = 0 ; i < BO_LANG_KEY_MAX_COUNT; i ++)
    {
        NSString *curName = [[NSUserDefaults standardUserDefaults] stringForKey:BO_LANGUAGE_KEY];
        
        NSString *folderName = [MultiLangManager getFolderNameFromKey:(BO_LANG)i];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:folderName ofType:FOLDER_SUB_NAME];
        
        NSBundle *bundle = [NSBundle bundleWithPath:path];
        
        if (bundle != nil)
        {
            if ([folderName hasSuffix:@"Hant"])
            {
                folderName = @"zh-TW";
            }
            else if ([folderName hasSuffix:@"Hans"])
            {
                folderName = @"zh-CN";
            }
            else if ([folderName hasSuffix:@"en"])
            {
                folderName = @"en-US";
            }
            
            NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:curName];
            
            NSString *countryName = [local displayNameForKey:NSLocaleIdentifier value:folderName];

            [allLanguageList addObject:countryName];
        }
    }
    
    return allLanguageList;
}

+ (NSString *)getFolderNameFromKey:(BO_LANG)langKey
{
    NSString *returnName = @"";
    
    switch (langKey)
    {
        case BO_LANG_KEY_ENGLISH_UNITED_STATES:
        {
            returnName = @"en";
            
            break;
        }
        case BO_LANG_KEY_ENGLISH_UNITED_KINGDOM:
        {
            returnName = @"en-GB";
            
            break;
        }
        case BO_LANG_KEY_ENGLISH_AUSTRALIA:
        {
            returnName = @"en-AU";
            
            break;
        }
        case BO_LANG_KEY_ENGLISH_INDIA:
        {
            returnName = @"en-IN";
            
            break;
        }
        case BO_LANG_KEY_CHINESE_TRADITIONAL:
        {
            returnName = @"zh-Hant";
            
            break;
        }
        case BO_LANG_KEY_CHINESE_SIMPLIFIED:
        {
            returnName = @"zh-Hans";
            
            break;
        }
        case BO_LANG_KEY_CHINESE_HONK_KONG:
        {
            returnName = @"zh-HK";
            
            break;
        }
        case BO_LANG_KEY_JAPANESE:
        {
            returnName = @"ja";
            
            break;
        }
        case BO_LANG_KEY_SPANISH:
        {
            returnName = @"es";
            
            break;
        }
        case BO_LANG_KEY_SPANISH_419:
        {
            returnName = @"es-419";
            
            break;
        }
        case BO_LANG_KEY_FRENCH:
        {
            returnName = @"fr";
            
            break;
        }
        case BO_LANG_KEY_FRENCH_CANADA:
        {
            returnName = @"fr-CA";
            
            break;
        }
        case BO_LANG_KEY_RUSSIAN:
        {
            returnName = @"ru";
            
            break;
        }
        case BO_LANG_KEY_GERMAN:
        {
            returnName = @"de";
            
            break;
        }
        case BO_LANG_KEY_PORTUGUESE_BRAZIL:
        {
            returnName = @"pt-BR";
            
            break;
        }
        case BO_LANG_KEY_PORTUGUESE:
        {
            returnName = @"pt-PT";
            
            break;
        }
        case BO_LANG_KEY_ITALIAN:
        {
            returnName = @"it";
            
            break;
        }
        case BO_LANG_KEY_KOREAN:
        {
            returnName = @"ko";
            
            break;
        }
        case BO_LANG_KEY_TURKISH:
        {
            returnName = @"tr";
            
            break;
        }
        case BO_LANG_KEY_DUTCH:
        {
            returnName = @"nl";
            
            break;
        }
        case BO_LANG_KEY_ARABIC:
        {
            returnName = @"ar";
            
            break;
        }
        case BO_LANG_KEY_THAI:
        {
            returnName = @"th";
            
            break;
        }
        case BO_LANG_KEY_SWEDISH:
        {
            returnName = @"sv";
            
            break;
        }
        case BO_LANG_KEY_DANISH:
        {
            returnName = @"da";
            
            break;
        }
        case BO_LANG_KEY_VIETNAMESE:
        {
            returnName = @"vi";
            
            break;
        }
        case BO_LANG_KEY_NORWEGIAN_BOKMAL:
        {
            returnName = @"nb";
            
            break;
        }
        case BO_LANG_KEY_POLISH:
        {
            returnName = @"pl";
            
            break;
        }
        case BO_LANG_KEY_FINNISH:
        {
            returnName = @"fi";
            
            break;
        }
        case BO_LANG_KEY_INDONESIAN:
        {
            returnName = @"id";
            
            break;
        }
        case BO_LANG_KEY_HEBREW:
        {
            returnName = @"hr";
            
            break;
        }
        case BO_LANG_KEY_GREEK:
        {
            returnName = @"el";
            
            break;
        }
        case BO_LANG_KEY_ROMANIAN:
        {
            returnName = @"ro";
            
            break;
        }
        case BO_LANG_KEY_HUNGARIAN:
        {
            returnName = @"hu";
            
            break;
        }
        case BO_LANG_KEY_CZECH:
        {
            returnName = @"cs";
            
            break;
        }
        case BO_LANG_KEY_CATALAN:
        {
            returnName = @"ca";
            
            break;
        }
        case BO_LANG_KEY_SLOVAK:
        {
            returnName = @"sk";
            
            break;
        }
        case BO_LANG_KEY_UKRAINIAN:
        {
            returnName = @"uk";
            
            break;
        }
        case BO_LANG_KEY_CROATIAN:
        {
            returnName = @"hr";
            
            break;
        }
        case BO_LANG_KEY_MALAY:
        {
            returnName = @"ms";
            
            break;
        }
        case BO_LANG_KEY_HINDI:
        {
            returnName = @"si";
            
            break;
        }
        default:
            break;
    }
    
    return returnName;
}

+ (NSString *)getCurLangKey
{
    NSArray *langArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    
    NSString *language = langArray[0];
    
    NSString *languageKey;
    
    if ([language.lowercaseString rangeOfString:@"en"].location != NSNotFound)
    {
        languageKey = @"en";
    }
    else if ([language.lowercaseString rangeOfString:@"hans"].location != NSNotFound || [language.lowercaseString rangeOfString:@"hans"].location != NSNotFound)
    {
        languageKey = @"zh_cn";
    }
    else
    {
        languageKey = @"zh_tw";
    }
    
    return languageKey;
}

@end
