//
//  MultiLanguageManager.h
//  SamanthaCloudSample
//
//  Created by Blues on 2019/12/12.
//

#import <Foundation/Foundation.h>

//#define BO_LANG_DEBUG
#ifdef BO_LANG_DEBUG
#   define BOLLog(fmt, ...) NSLog((@"%s [#_%d_] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#   define BOLTrace printf("#### %s [#_%d_]\n", __FUNCTION__, __LINE__);
#else
#   define BOLLog(fmt, ...)
#   define BOLTrace ;
#endif

#define BO_LANGUAGE_KEY                 @"BO_LANGUAGE"
#define FOLDER_SUB_NAME                 @"lproj"

typedef NS_ENUM(NSInteger, BO_LANG)
{
    BO_LANG_KEY_ENGLISH_UNITED_STATES,
    BO_LANG_KEY_ENGLISH_UNITED_KINGDOM,
    BO_LANG_KEY_ENGLISH_AUSTRALIA,
    BO_LANG_KEY_ENGLISH_INDIA,
    BO_LANG_KEY_CHINESE_TRADITIONAL,
    BO_LANG_KEY_CHINESE_SIMPLIFIED,
    BO_LANG_KEY_CHINESE_HONK_KONG,
    BO_LANG_KEY_JAPANESE,
    BO_LANG_KEY_SPANISH,
    BO_LANG_KEY_SPANISH_419,
    BO_LANG_KEY_FRENCH,
    BO_LANG_KEY_FRENCH_CANADA,
    BO_LANG_KEY_RUSSIAN,
    BO_LANG_KEY_GERMAN,
    BO_LANG_KEY_PORTUGUESE_BRAZIL,
    BO_LANG_KEY_PORTUGUESE,
    BO_LANG_KEY_ITALIAN,
    BO_LANG_KEY_KOREAN,
    BO_LANG_KEY_TURKISH,
    BO_LANG_KEY_DUTCH,
    BO_LANG_KEY_ARABIC,
    BO_LANG_KEY_THAI,
    BO_LANG_KEY_SWEDISH,
    BO_LANG_KEY_DANISH,
    BO_LANG_KEY_VIETNAMESE,
    BO_LANG_KEY_NORWEGIAN_BOKMAL,
    BO_LANG_KEY_POLISH,
    BO_LANG_KEY_FINNISH,
    BO_LANG_KEY_INDONESIAN,
    BO_LANG_KEY_HEBREW,
    BO_LANG_KEY_GREEK,
    BO_LANG_KEY_ROMANIAN,
    BO_LANG_KEY_HUNGARIAN,
    BO_LANG_KEY_CZECH,
    BO_LANG_KEY_CATALAN,
    BO_LANG_KEY_SLOVAK,
    BO_LANG_KEY_UKRAINIAN,
    BO_LANG_KEY_CROATIAN,
    BO_LANG_KEY_MALAY,
    BO_LANG_KEY_HINDI,
    BO_LANG_KEY_MAX_COUNT,
};

@interface MultiLangManager : NSObject

+ (NSString *)getLangStringForKey:(NSString *)key;
+ (void)setLanguageWith:(BO_LANG)langKey;
+ (void)resetDefaultLanguage;
+ (NSArray<NSString *> *)getSupportLanguageList;
+ (NSString *)getCurLangKey;

@end
