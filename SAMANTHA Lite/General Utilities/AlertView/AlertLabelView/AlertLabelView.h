//
//  AlertLabelView.h
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseAlertView.h"

typedef void (^AlertLabelViewHandler)(UIButton *button);

@interface AlertLabelView : BaseAlertView
{
    AlertLabelViewHandler tempHandler;
}

#pragma mark - UI

@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIImageView *bkImage;
@property (weak, nonatomic) IBOutlet UILabel *labMsg;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UILabel *labLeft;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *labRight;
@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (weak, nonatomic) IBOutlet UILabel *labCenter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgEmpty;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgNotEmpty;

- (id)init;
- (void)showOnView:(UIView *)view handler:(AlertLabelViewHandler)handler;
- (void)setAlertButton:(int)count;

@end
