//
//  AlertLabelView.m
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "AlertLabelView.h"

@implementation AlertLabelView

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    [self setupUILayer];
    
    [self setupFontSize];
        
    return self;
}

- (void)setupUILayer
{
    self.bkImage.layer.cornerRadius = 10;
    
    self.view.layer.cornerRadius = 10;
    self.view.layer.borderWidth = 1;
    self.view.layer.borderColor = [UIColor colorWithHexString:COLOR_ED5FFF].CGColor;
    
    [Utilities setShadow:self.view.layer Opacity:0.18 Radius:8 Offset:CGSizeMake(6, -10) Color:UIColor.blackColor];
    
    self.leftBtn.layer.cornerRadius = 10;
    self.rightBtn.layer.cornerRadius = 10;
    self.centerBtn.layer.cornerRadius = 10;
    
    [Utilities setShadow:self.leftBtn.layer Opacity:0.16 Radius:6 Offset:CGSizeMake(0, 3) Color:UIColor.blackColor];
    [Utilities setShadow:self.rightBtn.layer Opacity:0.16 Radius:6 Offset:CGSizeMake(0, 3) Color:UIColor.blackColor];
    [Utilities setShadow:self.centerBtn.layer Opacity:0.16 Radius:6 Offset:CGSizeMake(0, 3) Color:UIColor.blackColor];
}

- (void)setAlertButton:(int)count
{
    if (count >= 2)
    {
        [self.leftBtn setHidden:NO];
        
        [self.rightBtn setHidden:NO];
        
        [self.centerBtn setHidden:YES];
        
        self.labLeft.hidden = NO;
        
        self.labRight.hidden = NO;
        
        self.labCenter.hidden = YES;
    }
    else
    {
        [self.leftBtn setHidden:YES];
        
        [self.rightBtn setHidden:YES];
        
        [self.centerBtn setHidden:NO];
        
        self.labLeft.hidden = YES;
        
        self.labRight.hidden = YES;
        
        self.labCenter.hidden = NO;
    }
}

#pragma mark - Private API

- (void)showOnView:(UIView *)view handler:(AlertLabelViewHandler)handler
{
    [super showOnView:view];
    
    if ([Utilities isNullOrNil:self.labMsg.text])
    {
        self.msgEmpty.priority = UILayoutPriorityDefaultHigh;
        self.msgNotEmpty.priority = UILayoutPriorityDefaultLow;
    }
    else
    {
        self.msgEmpty.priority = UILayoutPriorityDefaultLow;
        self.msgNotEmpty.priority = UILayoutPriorityDefaultHigh;
    }
    
    [self layoutIfNeeded];
    
    tempHandler = handler;
}

- (IBAction)downButtonClick:(id)sender
{
    if (tempHandler != nil)
    {
        tempHandler(sender);
    }
    
    [self hide];
}

- (void)setupFontSize
{
    [self.labTitle setFont:[UIFont systemFontOfSize:([Utilities getFontSize] + 0) weight:UIFontWeightBold]];
    
    [self.labMsg setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightRegular]];
    
    [self.labLeft setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 1) weight:UIFontWeightBold]];
    
    [self.labRight setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 1) weight:UIFontWeightBold]];
    
    [self.labCenter setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 1) weight:UIFontWeightBold]];
    
    self.labLeft.text = [MultiLangManager getLangStringForKey:STR_CANCEL];
    
    self.labRight.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    self.labCenter.text = [MultiLangManager getLangStringForKey:STR_OK];
}

@end
