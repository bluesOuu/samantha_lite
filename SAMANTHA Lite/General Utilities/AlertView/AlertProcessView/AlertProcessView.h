//
//  AlertProcessView.h
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseAlertView.h"

@interface AlertProcessView : BaseAlertView
{
    NSMutableArray *imageArray;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

+ (id)sharedInstance;

+ (void)show;

+ (void)showOnView:(UIView *)view;

+ (void)hide;

@end
