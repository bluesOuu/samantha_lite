//
//  AlertProcessView.m
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "AlertProcessView.h"

@implementation AlertProcessView

#pragma mark - Init

+ (id)sharedInstance
{
    static AlertProcessView *processView;
    
    static dispatch_once_t singletonToken;
    
    dispatch_once(&singletonToken,^
    {
        processView = [[self alloc] init];
    });
    
    return processView;
}

- (id)init
{
    self = [super init];
    
    imageArray = [NSMutableArray new];
    
    [self removeGestureRecognizer:self.gestureRecognizers[0]];
    
    return self;
}

- (void)prepareImage
{
    [imageArray removeAllObjects];
    
    for (int i = 1 ; i < 27 ; i++)
    {
        [imageArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loading_%d", i]]];
    }
    
    self.imageView.image = nil;
    
    self.imageView.animationImages = imageArray;
    
    self.imageView.animationRepeatCount = 0;
    
    self.imageView.animationDuration = 1.5;
}

- (void)show
{
    [self prepareImage];
    
    if (!self.imageView.animating)
    {
        [super show];
        
        [self.imageView startAnimating];
    }
}

- (void)showOnView:(UIView *)view
{
    [self prepareImage];
    
    if (!self.imageView.isAnimating)
    {
        [super showOnView:view];
        
        [self.imageView startAnimating];
    }
}

- (void)hide
{
    [super hide];
    
    [self.imageView stopAnimating];
}

+ (void)show
{
    [[AlertProcessView sharedInstance] show];
}

+ (void)showOnView:(UIView *)view
{
    [[AlertProcessView sharedInstance] showOnView:view];
}

+ (void)hide
{
    [[AlertProcessView sharedInstance] hide];
}

@end
