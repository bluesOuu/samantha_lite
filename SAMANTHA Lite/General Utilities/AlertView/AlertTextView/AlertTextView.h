//
//  AlertTextView.h
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseAlertView.h"

typedef void (^AlertTextViewHandler)(UIButton *button);

@interface AlertTextView : BaseAlertView<UITextFieldDelegate>
{
    AlertTextViewHandler tempHandler;
}

#pragma mark - UI

@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UILabel *labMsg;
@property (weak, nonatomic) IBOutlet UIImageView *bkImage;
@property (weak, nonatomic) IBOutlet UITextField *text;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UILabel *labLeft;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *labRight;

- (id)init;

- (void)showOnView:(UIView *)view handler:(AlertTextViewHandler)handler;

@end

