//
//  AlertTextView.m
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "AlertTextView.h"

@implementation AlertTextView

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    [self removeGestureRecognizer:self.gestureRecognizers[0]];
    
    [self setupUILayer];
    
    [self setupFontSize];
    
    return self;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Private API

- (void)setupUILayer
{
    self.bkImage.layer.cornerRadius = 10;
    
    self.view.layer.cornerRadius = 10;
    self.view.layer.borderWidth = 1;
    self.view.layer.borderColor = [UIColor colorWithHexString:COLOR_ED5FFF].CGColor;
    
    [Utilities setShadow:self.view.layer Opacity:0.18 Radius:8 Offset:CGSizeMake(6, -10) Color:UIColor.blackColor];
    
    self.text.layer.cornerRadius = 10;
    self.text.layer.borderWidth = 1;
    self.text.layer.borderColor = [UIColor colorWithHexString:COLOR_ED5FFF].CGColor;
    
    self.leftBtn.layer.cornerRadius = 10;
    self.rightBtn.layer.cornerRadius = 10;
    
    [Utilities setShadow:self.leftBtn.layer Opacity:0.16 Radius:6 Offset:CGSizeMake(0, 3) Color:UIColor.blackColor];
    [Utilities setShadow:self.rightBtn.layer Opacity:0.16 Radius:6 Offset:CGSizeMake(0, 3) Color:UIColor.blackColor];
}

- (IBAction)downButtonClick:(id)sender
{
    [self.text resignFirstResponder];

    if (tempHandler != nil)
    {
        tempHandler(sender);
    }
    
    [self hide];
}

- (void)setupFontSize
{
    [self.labTitle setFont:[UIFont systemFontOfSize:([Utilities getFontSize] + 0) weight:UIFontWeightBold]];
    
    [self.labMsg setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightRegular]];
    
    [self.text setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 2) weight:UIFontWeightRegular]];
    
    [self.labLeft setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 1) weight:UIFontWeightBold]];
    
    [self.labRight setFont:[UIFont systemFontOfSize:([Utilities getFontSize] - 1) weight:UIFontWeightBold]];
    
    self.labLeft.text = [MultiLangManager getLangStringForKey:STR_CANCEL];
    
    self.labRight.text = [MultiLangManager getLangStringForKey:STR_OK];
}

- (void)showOnView:(UIView *)view handler:(AlertTextViewHandler)handler
{
    [super showOnView:view];
    
    tempHandler = handler;
}

@end
