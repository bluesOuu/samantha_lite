//
//  AlertScanQRCodeView.h
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseAlertView.h"
#import "BOBarCodeScanView.h"

@protocol AlertScanQRCodeViewDelegate;

@interface AlertScanQRCodeView : BaseAlertView<BOBarCodeScanViewDelegate>
{
    BOOL flag;
}
@property (weak, nonatomic) IBOutlet BOBarCodeScanView *scanView;
@property (weak, nonatomic) IBOutlet UIImageView *targetView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backPosition;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (nonatomic, weak) id<AlertScanQRCodeViewDelegate> delegate;

- (id)init;

- (id)initWithDelegate:(id<AlertScanQRCodeViewDelegate>)delegate;

@end

@protocol AlertScanQRCodeViewDelegate <NSObject>

- (void)alertScanQRCodeView:(AlertScanQRCodeView *)alertView BarCodeInfo:(NSString *)barCodeInfo;

@end
