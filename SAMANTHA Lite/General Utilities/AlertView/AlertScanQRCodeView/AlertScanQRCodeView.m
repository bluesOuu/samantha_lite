//
//  AlertScanQRCodeView.m
//  12Ours
//
//  Created by Blues on 2017/7/5.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "AlertScanQRCodeView.h"

@implementation AlertScanQRCodeView

#pragma mark - Init

- (id)init
{
    self = [super init];
    
    [self setup];
        
    return self;
}

- (id)initWithDelegate:(id<AlertScanQRCodeViewDelegate>)delegate
{
    self = [self init];
    
    self.delegate = delegate;
    
    [self removeGestureRecognizer:self.gestureRecognizers[0]];
        
    return self;
}

#pragma mark - Button Events

- (IBAction)btnClick:(id)sender
{
    [self hide];
}

#pragma mark - Setup

- (void)setup
{
    flag = NO;
    
    [self setupQRScan];
    
    [self setupFontSize];
    
    [self setupLocalized];
}

- (void)setupQRScan
{
    [self.scanView setBarCodeType:[NSArray arrayWithObjects:[NSNumber numberWithInt:BAR_CODE_TYPE_QR], nil]];

    self.scanView.delegate = self;
}

- (void)setupFontSize
{
    [self.labTitle setFont:[UIFont systemFontOfSize:[Utilities getFontSize] + 7 weight:UIFontWeightRegular]] ;
}

- (void)setupLocalized
{
    self.labTitle.text = [MultiLangManager getLangStringForKey:STR_SCAN_QR];
}

#pragma mark - Public API

- (void)showOnView:(UIView *)view
{
    if (view != nil)
    {
        [self setFrame:view.bounds];
        
        [self.view setFrame:view.bounds];
        
        [self.scanView setFrame:view.bounds];
        
        [view addSubview:self];
        
        if ((view.bounds.size.height / view.bounds.size.width) > 1.8)
        {
            self.backPosition.constant = 44;
        }
        else
        {
            self.backPosition.constant = 0;
        }
        
        self.targetView.center = self.scanView.center;
        
//        [Utilities addAnimation:ANI_CAMERA_IRIS_HOLLOW_OPEN Direction:ANI_LEFT Duration:0.5 onTheView:view Completion:^
//        {
            self.targetView.translatesAutoresizingMaskIntoConstraints = YES;
//
            [self.scanView startReadingWithCameraPosition:CAMERA_POSITION_BACK];
//        }];
    }
    else
    {
        [self setFrame:[UIApplication sharedApplication].keyWindow.bounds];
        
        [self.view setFrame:[UIApplication sharedApplication].keyWindow.bounds];
        
        [self.scanView setFrame:[UIApplication sharedApplication].keyWindow.bounds];
        
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        
        if (([UIApplication sharedApplication].keyWindow.bounds.size.height / [UIApplication sharedApplication].keyWindow.bounds.size.width) > 1.8)
        {
            self.backPosition.constant = 44;
        }
        else
        {
            self.backPosition.constant = 0;
        }
        
        self.targetView.center = self.scanView.center;
        
//        [Utilities addAnimation:ANI_CAMERA_IRIS_HOLLOW_OPEN Direction:ANI_LEFT Duration:0.5 onTheView:nil Completion:^
//        {
            self.targetView.translatesAutoresizingMaskIntoConstraints = YES;
//
            [self.scanView startReadingWithCameraPosition:CAMERA_POSITION_BACK];
//        }];
    }
}

- (void)show
{
    [self showOnView:nil];
}

- (void)hide
{
    [self.scanView stopReading];
    
//    [Utilities addAnimation:ANI_CAMERA_IRIS_HOLLOW_CLOSE Direction:ANI_LEFT Duration:0.5 onTheView:nil Completion:^
//    {
        [self removeFromSuperview];
//    }];
}

#pragma mark - BOBarCodeScanViewDelegate

- (void)BOBarCodeScanViewRead:(NSString *)barCodeInfo Bound:(CGRect)bounds
{
    __weak AlertScanQRCodeView *weakSelf = self;
    
    CGPoint newPosition = [self.scanView convertPoint:bounds.origin toView:self.view];
    
    CGRect frame = CGRectMake(newPosition.x, newPosition.y, bounds.size.width, bounds.size.height);
    
    self.targetView.frame = frame;
    
    CGPoint center = self.targetView.center;
    
    frame.size = CGSizeMake(bounds.size.width * 1.3, bounds.size.height * 1.3);
    
    self.targetView.frame = frame;
    
    self.targetView.center = center;
    
//    NSLog(@"barCodeInfo = %@", barCodeInfo);
    
    if (!flag)
    {
        flag = YES;
        
        [Utilities delayBlockWith:1 Queue:dispatch_get_main_queue() Block:^
        {
            [self.scanView stopReading];
            
            if ([self.delegate respondsToSelector:@selector(alertScanQRCodeView:BarCodeInfo:)])
            {
                [self.delegate alertScanQRCodeView:self BarCodeInfo:barCodeInfo];
            }
            
            [weakSelf hide];
        }];
    }
}

@end
