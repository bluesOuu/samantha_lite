//
//  HiroiaManager.m
//  bleTest
//
//  Created by Blues on 2019/5/9.
//

#import "HiroiaManager.h"

@implementation HiroiaManager

#pragma mark - Timer

- (void)startGetStatusTimer:(NSInteger)triggerTime With:(HiroiaDevice *)devices
{
    if (timerDic == nil)
    {
        timerDic = [NSMutableDictionary new];
    }
    
    NSTimer *curTimer = [timerDic objectForKey:devices.peripheral];
    
    if (curTimer == nil)
    {
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:triggerTime target:self selector:@selector(getStatusCallback:) userInfo:nil repeats:YES];
        
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        
        if (devices.peripheral == nil)
        {
            return;
        }
        
        [timerDic setObject:timer forKey:devices.peripheral];
        
        [self getStatusCallback:timer];
    }
}

- (void)stopGetStatusTimerWith:(HiroiaDevice *)devices
{
    NSTimer *timer = [timerDic objectForKey:devices.peripheral];

    if (timer != nil)
    {
        [timerDic removeObjectForKey:devices.peripheral];

        [timer invalidate];

        timer = nil;
    }
}

- (void)getStatusCallback:(NSTimer *)timer
{
    NSArray *array = [timerDic allKeysForObject:timer];
    
    CBPeripheral *peripheral = array[0];
    
    HiroiaDevice *hiroiaDevice = [self findHiroiaDeviceWith:peripheral];
    
    if (hiroiaDevice != nil && hiroiaDevice.peripheral.state == CBPeripheralStateConnected)
    {
        [self sendSamanthaCmdP5GetStatusWith:hiroiaDevice];
    }
}

@end
