//
//  HiroiaManager.h
//  bleTest
//
//  Created by Blues on 2019/5/9.
//

#import "BaseHriManager.h"

@interface SmtCmdStruc : NSObject

@end

@interface HiroiaManager : BaseHriManager
{
    NSMutableDictionary *timerDic;
}

- (void)startGetStatusTimer:(NSInteger)triggerTime With:(HiroiaDevice *)devices;

- (void)stopGetStatusTimerWith:(HiroiaDevice *)devices;

@end
