//
//  NSData+Conversion.m
//  PASESA Health
//
//  Created by Blues on 2016/5/31.
//  Copyright © 2016年 PASESA Health. All rights reserved.
//

#import "NSData+Conversion.h"

@implementation NSData (Conversion)

//- (NSString *)forceConvertToString
//{
//    NSString *str = [[[[self description] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""];
//
//    return str;
//}

- (NSString *)forceConvertToString//:(NSData *)data
{
    NSString *str = [[[[self description] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    if (![self isKindOfClass:[NSData class]]) return str;
    
    const unsigned *dataBytes = [self bytes];
    
    NSInteger dataInt = [self length];
    
    dataInt = dataInt/4;
    
    NSInteger lastNum;
    
    lastNum = [self length]%4;
    
    if (lastNum !=0)
    {
        dataInt +=1;
    }
    
    NSMutableString *mtbStr = [[NSMutableString alloc] initWithString:@""];
    
    for (int i = 0 ; i < dataInt ; i++)
    {
        NSString *newStr = [NSString stringWithFormat:@"%08x",ntohl(dataBytes[i])];
        
        [mtbStr appendString:newStr];
    }
    
    NSString *lastStr = [mtbStr substringWithRange:NSMakeRange(0, [self length]*2)];

    return lastStr;
}

- (NSInteger)getEoSLength
{
    unsigned char bytes[self.length];
    
    [self getBytes:bytes length:self.length];
    
    NSInteger length = 0;
    
    for (int i = 0 ; i < self.length ; i ++)
    {
        char oneChar = bytes[i];
        
        if (oneChar == '\0')
        {
            length = i;
            
            break;
        }
        NSLog(@"oneChar = %x",oneChar);
    }
    
    return length;
}

+ (NSData *)hexStringToNSData:(NSString *)hexStr
{
    NSMutableData *data = [[NSMutableData alloc] init];
    
    unsigned char whole_byte;
    
    char byte_chars[2];
    
    int i;
    
    for (i = 0 ; i < hexStr.length / 2 ; i++)
    {
        byte_chars[0] = [hexStr characterAtIndex:(i * 2)];
        
        byte_chars[1] = [hexStr characterAtIndex:((i * 2) + 1)];
        
        whole_byte = strtol(byte_chars, NULL, (int)hexStr.length);
        
        [data appendBytes:&whole_byte length:1];
    }
    
    return data;
}

@end
