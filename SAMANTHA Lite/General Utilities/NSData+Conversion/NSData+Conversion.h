//
//  NSData+Conversion.h
//  Galaxy View
//
//  Created by Blues on 2016/5/31.
//  Copyright © 2016年 PASESA Health. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Conversion)

- (NSString *)forceConvertToString;
- (NSInteger)getEoSLength;
+ (NSData *)hexStringToNSData:(NSString *)hexStr;

@end
