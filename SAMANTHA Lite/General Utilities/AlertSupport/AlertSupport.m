//
//  AlertSupport.m
//  HWC Roasters
//
//  Created by Blues on 2020/7/9.
//  Copyright © 2020 Blues. All rights reserved.
//

#import "AlertSupport.h"



@implementation AlertSupport

#pragma mark - Alert

+ (void)showAlertAccPwdError:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_ALERT_ACC_PWD_ERROR];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_CNFIRM_INFO];
    
    [alert setAlertButton:1];
    
    alert.labCenter.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        handler(alert.labCenter.text);
    }];
}

+ (void)showAlertNotActivatedError:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_ALERT_NOT_ACTIVATED];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_CHECK_EMAIL];
    
    [alert setAlertButton:2];
    
    alert.labLeft.text = [MultiLangManager getLangStringForKey:STR_RE_SEND];
    
    alert.labRight.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        if (button == alert.leftBtn)
        {
            handler(alert.labLeft.text);
        }
        else
        {
            handler(alert.labRight.text);
        }
    }];
}

+ (void)showAlertSendedDone:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_ALERT_RE_SEND_DONE];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_CHECK_EMAIL];
    
    [alert setAlertButton:1];
    
    alert.labCenter.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        handler(alert.labCenter.text);
    }];
}

+ (void)showAlertNetworkErrorWithCode:(NSInteger)code Delegate:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [NSString stringWithFormat:[MultiLangManager getLangStringForKey:STR_ALERT_ERROR_CODE], code];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_NETWORK_ERROR];
    
    [alert setAlertButton:1];
    
    alert.labCenter.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        handler(alert.labCenter.text);
    }];
}

+ (void)showAlertRegDone:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_REG_SUCCESS];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_CHECK_EMAIL];
    
    [alert setAlertButton:2];
    
    alert.labLeft.text = [MultiLangManager getLangStringForKey:STR_RE_SEND];
    
    alert.labRight.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        if (button == alert.leftBtn)
        {
            handler(alert.labLeft.text);
        }
        else
        {
            handler(alert.labRight.text);
        }
    }];
}

+ (void)showAlertAccHasUsed:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_ALERT_ACC_HAS_USED];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_HAS_USED_DESCRIPTION];
    
    [alert setAlertButton:2];
    
    alert.labLeft.text = [MultiLangManager getLangStringForKey:STR_FORGET];
    
    alert.labRight.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        if (button == alert.leftBtn)
        {
            handler(alert.labLeft.text);
        }
        else
        {
            handler(alert.labRight.text);
        }
    }];
}

+ (void)showAlertResetPwdDoneWithAcc:(NSString *)acc Delegate:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_ALERT_RESET_TITLE];
    
    alert.labMsg.text = [MultiLangManager getLangStringForKey:STR_ALERT_RESET_PWD_SUCCESS];
    
    [alert setAlertButton:1];
    
    alert.labCenter.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        handler(alert.labCenter.text);
    }];
}

//+ (void)showAlertDupAcc:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ERROR] message:[MultiLangManager getLangStringForKey:STR_ERROR_DUPLICATE] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertRegSuccess:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[MultiLangManager getLangStringForKey:STR_ERROR_REG_SUCCESS] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:handler];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertRegFailed:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ERROR] message:[MultiLangManager getLangStringForKey:STR_ERROR_REG_FAIL] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertAccLength:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ERROR] message:[MultiLangManager getLangStringForKey:STR_ERROR_ACC_LENGTH] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertPwdLength:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ERROR] message:[MultiLangManager getLangStringForKey:STR_ERROR_PWD_LENGTH] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertAppleSignInNotSupport:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ERROR] message:[MultiLangManager getLangStringForKey:STR_ERROR_APPLE_SIGN_NOT_SUP] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertCustomMsg:(id)delegate Title:(NSString *)title Msg:(NSString *)msg handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertNetError:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ALERT_INTERNET_ERROR] message:[MultiLangManager getLangStringForKey:STR_ALERT_INTERNET_CHECK] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showActionSheetUser:(id)delegate isStore:(BOOL)isStore handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    if (isStore)
//    {
//        UIAlertAction *swapAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_SWAP] style:UIAlertActionStyleDefault handler:handler];
//
//        [swapAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//        [alert addAction:swapAction];
//
//        UIAlertAction *scanAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_GAME_SOLVE_TASK] style:UIAlertActionStyleDefault handler:handler];
//
//        [scanAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//        [alert addAction:scanAction];
//    }
//
//    UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_LOGOUT] style:UIAlertActionStyleDefault handler:handler];
//
//    [logoutAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:logoutAction];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:nil];
//
//    [cancelAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showActionSheetNotifySettings:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    UIAlertAction *allReadAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_ALL_READ] style:UIAlertActionStyleDefault handler:handler];
//
//    [allReadAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:allReadAction];
//
//    UIAlertAction *allDelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_ALL_DELETE] style:UIAlertActionStyleDefault handler:handler];
//
//    [allDelAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:allDelAction];
//
//    UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_SETTINGS] style:UIAlertActionStyleDefault handler:handler];
//
//    [settingsAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:settingsAction];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:nil];
//
//    [cancelAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
+ (void)showImageSelector:(id)delegate IsDevice:(BOOL)isDevice CamHander:(void (^)(UIAlertAction *action))camHander AlbumHander:(void (^)(UIAlertAction *action))albumHander
{
    NSArray *actionArray = [NSArray arrayWithObjects:isDevice ? [MultiLangManager getLangStringForKey:STR_SCAN_CAFE_QR_CODE] : [MultiLangManager getLangStringForKey:STR_IMAGE_PICKER_CAMERA], [MultiLangManager getLangStringForKey:STR_IMAGE_PICKER_ALBUM],  [MultiLangManager getLangStringForKey:STR_CANCEL], nil];

    void (^alertHander)(UIAlertAction *) = ^void(UIAlertAction *action)
    {
        for (NSString *actionTitle in actionArray)
        {
            if ([actionTitle isEqualToString:action.title])
            {
                NSInteger index = [actionArray indexOfObject:actionTitle];

                switch (index)
                {
                    case 0:
                    {
                        camHander(action);

                        break;
                    }
                    case 1:
                    {
                        albumHander(action);

                        break;
                    }
                    default:
                        break;
                }

                break;
            }
        }
    };

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    for(NSString *actionTitle in actionArray)
    {
        UIAlertActionStyle style = UIAlertActionStyleDefault;

        if ([[actionArray lastObject] isEqualToString:actionTitle])
        {
            style = UIAlertActionStyleCancel;
        }

        UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:style handler:alertHander];

//        [action setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];

        [alertController addAction:action];
    }

    [delegate presentViewController:alertController animated:YES completion:nil];
}

+ (void)showAlertGPS:(id)delegate handler:(AlertHandler)handler
{
    AlertLabelView *alert = [AlertLabelView new];
    
    alert.labTitle.text = [MultiLangManager getLangStringForKey:STR_ALERT_GPS];
    
    alert.labMsg.text = @"";
    
    [alert setAlertButton:2];
    
    alert.labLeft.text = [MultiLangManager getLangStringForKey:STR_CANCEL];
    
    alert.labRight.text = [MultiLangManager getLangStringForKey:STR_OK];
    
    [alert showOnView:nil handler:^(UIButton *button)
    {
        if (button == alert.leftBtn)
        {
            handler(alert.labLeft.text);
        }
        else
        {
            handler(alert.labRight.text);
        }
    }];
}

//+ (void)showAlertNeedPwdWithName:(NSString *)name Selector:(SEL)aSelector Delegate:(id)delegate PWDHandler:(void (^ __nullable)(NSString *pwd))pwdHandler
//{
//    [[NSNotificationCenter defaultCenter] addObserver:delegate selector:aSelector name:UITextFieldTextDidChangeNotification object:nil];
//
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:[MultiLangManager getLangStringForKey:STR_NEED_PWD], name] message:[MultiLangManager getLangStringForKey:STR_PWD_RULE] preferredStyle:UIAlertControllerStyleAlert];
//
//    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField)
//     {
//        textField.placeholder = [MultiLangManager getLangStringForKey:STR_PWD];
//
//        textField.secureTextEntry = YES;
//
//        textField.delegate = delegate;
//     }];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//                               {
//                                    [[NSNotificationCenter defaultCenter] removeObserver:delegate name:UITextFieldTextDidChangeNotification object:nil];
//
//                                    UITextField *textField = alert.textFields[0];
//
//                                    pwdHandler(textField.text);
//                               }];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//    {
//        [[NSNotificationCenter defaultCenter] removeObserver:delegate name:UITextFieldTextDidChangeNotification object:nil];
//    }];
//
//    [alert addAction:okAction];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertSmtSettings:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
////    UIAlertAction *swapAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_SMT_CHANGE_NAME] style:UIAlertActionStyleDefault handler:handler];
////
////    [swapAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
////
////    [alert addAction:swapAction];
////
////    UIAlertAction *logoutAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_SMT_CHANGE_PWD] style:UIAlertActionStyleDefault handler:handler];
////
////    [logoutAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
////
////    [alert addAction:logoutAction];
////
//    UIAlertAction *parAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_SMT_CHANGE_PAIR] style:UIAlertActionStyleDefault handler:handler];
//
//    [parAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:parAction];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:nil];
//
//    [cancelAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertBLE:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ALERT_BLE] message:nil preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:handler];
//
//    [alert addAction:okAction];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:nil];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertUpdateApp:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_ALERT_HAS_NEW_VERSION] message:nil preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_ALERT_UPDATE] style:UIAlertActionStyleDefault handler:handler];
//
//    [alert addAction:okAction];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:handler];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertMachineWithTitle:(NSString *)title Msg:(NSString *)msg Delegate:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:handler];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertBLESettings:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_BLE_SETTINGS] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    UIAlertAction *parAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_SELECT_OTHERS_DEVICE] style:UIAlertActionStyleDefault handler:handler];
//
//    [parAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:parAction];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:nil];
//
//    [cancelAction setValue:[UIColor colorWithHexString:COLOR_333333] forKey:@"titleTextColor"];
//
//    [alert addAction:cancelAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertGameNoOpen:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_GAME_SEASON_NO_OPEN_TITLE] message:[MultiLangManager getLangStringForKey:STR_GAME_SEASON_NO_OPEN_MSG] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:handler];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertSolveTask:(id)delegate CanSolve:(BOOL)canSolve Msg:(NSString *)msg handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    NSString *title;
//
//    UIAlertAction *okAction;
//
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
//
//    if (canSolve)
//    {
//        title = [MultiLangManager getLangStringForKey:STR_GAME_SOLVE_TASK];
//
//        okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CONFIRM_SOLVE] style:UIAlertActionStyleDefault handler:handler];
//
//        [alert addAction:okAction];
//
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_CANCEL] style:UIAlertActionStyleCancel handler:nil];
//
//        [alert addAction:cancelAction];
//    }
//    else
//    {
//        title = [MultiLangManager getLangStringForKey:STR_NOT_PRIMISSION_SOLVE];
//
//        okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//        [alert addAction:okAction];
//    }
//
//    alert.title = title;
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertSolveResult:(id)delegate Solve:(BOOL)solve SeasonEnd:(BOOL)seasonEnd
//{
//    NSString *title = @"";
//
//    if (solve)
//    {
//        title = [MultiLangManager getLangStringForKey:STR_SOLVE_SUCCESS];
//    }
//    else
//    {
//        title = [MultiLangManager getLangStringForKey:STR_SOLVE_TIMEOUT];
//    }
//
//    if (seasonEnd)
//    {
//        title = [MultiLangManager getLangStringForKey:STR_SEASON_END];
//    }
//
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:@"" preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}
//
//+ (void)showAlertQRError:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler
//{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[MultiLangManager getLangStringForKey:STR_QR_CODE_CANT_READ] message:[MultiLangManager getLangStringForKey:STR_QR_CODE_RE_SCAN] preferredStyle:UIAlertControllerStyleAlert];
//
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:[MultiLangManager getLangStringForKey:STR_OK] style:UIAlertActionStyleDefault handler:handler];
//
//    [alert addAction:okAction];
//
//    [delegate presentViewController:alert animated:YES completion:nil];
//}

@end
