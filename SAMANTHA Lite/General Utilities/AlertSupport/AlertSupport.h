//
//  AlertSupport.h
//  HWC Roasters
//
//  Created by Blues on 2020/7/9.
//  Copyright © 2020 Blues. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlertLabelView.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^AlertHandler)(NSString *btnStr);

@interface AlertSupport : NSObject

#pragma mark - Alert

+ (void)showAlertAccPwdError:(id)delegate handler:(AlertHandler)handler;
+ (void)showAlertNotActivatedError:(id)delegate handler:(AlertHandler)handler;
+ (void)showAlertSendedDone:(id)delegate handler:(AlertHandler)handler;
+ (void)showAlertNetworkErrorWithCode:(NSInteger)code Delegate:(id)delegate handler:(AlertHandler)handler;
+ (void)showAlertRegDone:(id)delegate handler:(AlertHandler)handler;
+ (void)showAlertAccHasUsed:(id)delegate handler:(AlertHandler)handler;
+ (void)showAlertResetPwdDoneWithAcc:(NSString *)acc Delegate:(id)delegate handler:(AlertHandler)handler;

//+ (void)showAlertEmpty:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertCode:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertDupAcc:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertRegSuccess:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertRegFailed:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertAccLength:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertPwdLength:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertAppleSignInNotSupport:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertCustomMsg:(id)delegate Title:(NSString *)title Msg:(NSString *)msg handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertNetError:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showActionSheetUser:(id)delegate isStore:(BOOL)isStore handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showActionSheetNotifySettings:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
+ (void)showImageSelector:(id)delegate IsDevice:(BOOL)isDevice CamHander:(void (^)(UIAlertAction *action))camHander AlbumHander:(void (^)(UIAlertAction *action))albumHander;
+ (void)showAlertGPS:(id)delegate handler:(AlertHandler)handler;
//+ (void)showAlertNeedPwdWithName:(NSString *)name Selector:(SEL)aSelector Delegate:(id)delegate PWDHandler:(void (^ __nullable)(NSString *pwd))pwdHandler;
//+ (void)showAlertSmtSettings:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertBLE:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertUpdateApp:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertMachineWithTitle:(NSString *)title Msg:(NSString *)msg Delegate:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertBLESettings:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertGameNoOpen:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertSolveTask:(id)delegate CanSolve:(BOOL)canSolve Msg:(NSString *)msg handler:(void (^ __nullable)(UIAlertAction *action))handler;
//+ (void)showAlertSolveResult:(id)delegate Solve:(BOOL)solve SeasonEnd:(BOOL)seasonEnd;
//+ (void)showAlertQRError:(id)delegate handler:(void (^ __nullable)(UIAlertAction *action))handler;

@end

NS_ASSUME_NONNULL_END
