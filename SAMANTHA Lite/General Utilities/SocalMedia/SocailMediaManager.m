//
//  SocailMediaManager.m
//  12Ours
//
//  Created by Blues on 2017/9/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "SocailMediaManager.h"
#import <SafariServices/SafariServices.h>

#define SOCAIL_MEDIA_DEBUG

#ifdef SOCAIL_MEDIA_DEBUG
#   define socailMediaLog(fmt, ...) NSLog((@"@SocailMediaManager [#_%4d_] " fmt "\n"), __LINE__, ##__VA_ARGS__);
#   define socailMediaTrace printf("@SocailMediaManager [#_%4d_] %s\n", __LINE__, __FUNCTION__);
#else
# define socailMediaLog(...);
# define socailMediaTrace;
#endif

@implementation SocailMediaManager
@synthesize rootView;

#pragma mark - init

+ (SocailMediaManager *)sharedInstance
{
    static SocailMediaManager *smManager;
    
    static dispatch_once_t singletonToken;
    
    dispatch_once(&singletonToken,^
    {
        smManager = [[self alloc] init];
    });
    
    return smManager;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self initWithLine];
        
        [self initWithFB];

//        [self initWithGoogle];
        
        socailMediaLog(@"Init");
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Socail Init

- (void)initWithLine
{
    [LineSDKLogin sharedInstance].delegate = self;
    
    apiClient = [[LineSDKAPI alloc] initWithConfiguration:[LineSDKConfiguration defaultConfig]];
}

- (void)initWithFB
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(observeProfileChange:) name:FBSDKProfileDidChangeNotification object:nil];
    
    fbLogin = [[FBSDKLoginButton alloc] init];

    fbLogin.permissions = @[@"public_profile"];

    fbLogin.delegate = self;
}

//- (void)initWithGoogle
//{
//    [GIDSignIn sharedInstance].shouldFetchBasicProfile = YES;
//
//    [GIDSignIn sharedInstance].delegate = self;
//
//    [GIDSignIn sharedInstance].clientID = GOOGLE_CLIENTID;
//
////    [GIDSignIn sharedInstance].scopes = [NSArray arrayWithObjects:@"https://www.googleapis.com/auth/youtube.upload", nil];
//}

#pragma mark - Applecation

- (void)registerSocalMedia:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
//
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
//
//    [[Twitter sharedInstance] startWithConsumerKey:TWITTER_ACC consumerSecret:TWITTER_PWD];
//
    
    socailMediaLog(@"didFinishLaunchingWithOptions");
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
{
    [[LineSDKLogin sharedInstance] handleOpenURL:url];
    
//    [[GIDSignIn sharedInstance] handleURL:url];

    [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options];

//    [[Twitter sharedInstance] application:app openURL:url options:options];
    
    socailMediaLog(@"openURL = %@",url);
    
    return YES;
}

#pragma mark - Socail Func

- (void)login:(_SOCAIL_TYPE)socailType Response:(SocailMediaResponse)response
{
    socailMediaResponse = response;
    
    switch (socailType) 
    {
        case SOCAIL_TYPE_LINE:
        {
            [[LineSDKLogin sharedInstance] startLoginWithPermissions:@[@"profile"]];
            
            socailMediaLog(@"Line Login");
            
            break;
        }
        case SOCAIL_TYPE_GOOGLE:
        {
//            [GIDSignIn sharedInstance].presentingViewController = rootView;
//
//            if (![[GIDSignIn sharedInstance] hasPreviousSignIn])
//            {
//                [[GIDSignIn sharedInstance] signIn];
//
//                socailMediaLog(@"Google SignIn");
//            }
//            else
//            {
//                [[GIDSignIn sharedInstance] restorePreviousSignIn];
//
//                socailMediaLog(@"Google signInSilently");
//            }
            
            break;
        }
        case SOCAIL_TYPE_FACEBOOK:
        {
            [fbLogin sendActionsForControlEvents: UIControlEventTouchUpInside];
            
            socailMediaLog(@"FB Login");
            
            break;
        }
        case SOCAIL_TYPE_TWITTER:
        {
//            if ([Twitter sharedInstance].sessionStore.hasLoggedInUsers)
//            {
//                [self getProfile:socailType];
//
//                socailMediaLog(@"TW Login With Get Profile");
//            }
//            else
//            {
//                [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error)
//                 {
//                     if (session)
//                     {
//                         [self getProfile:socailType];
//                     }
//                     else
//                     {
//                         dispatch_async(dispatch_get_main_queue(), ^
//                         {
//                             if (socailMediaResponse != nil)
//                             {
//                                 socailMediaResponse(nil);
//                             }
//                         });
//                     }
//                 }];
//
//                socailMediaLog(@"TW Login");
//            }

            break;
        }
        case SOCAIL_TYPE_APPLE:
        {
            if (@available(iOS 13.0, *))
            {
                ASAuthorizationAppleIDProvider *provider = [ASAuthorizationAppleIDProvider new];
            
                ASAuthorizationAppleIDRequest *request = provider.createRequest;

                request.requestedScopes = @[ASAuthorizationScopeEmail, ASAuthorizationScopeFullName];
                                
                ASAuthorizationController *ascv = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
                
                ascv.delegate = self;
                
//                ascv.presentationContextProvider = self;
                
                [ascv performRequests];
                
//                ASAuthorizationPasswordProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
//
//                ASAuthorizationAppleIDRequest *idRequest = appleIDProvider.createRequest;
//
//                ASAuthorizationAppleIDProvider *appleIDPasswordProvider = [ASAuthorizationPasswordProvider new];
//
//                ASAuthorizationPasswordRequest *passwordRequest = appleIDPasswordProvider.createRequest;
//
//                ASAuthorizationController *ascv = [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[idRequest, passwordRequest]];
            }
            
            break;
        }
        default:
            break;
    }
}

- (void)logout:(_SOCAIL_TYPE)socailType Response:(SocailMediaResponse)response
{
    socailMediaResponse = response;
    
    switch (socailType) 
    {
        case SOCAIL_TYPE_LINE:
        {
            [apiClient logoutWithCompletion:^(BOOL success, NSError * error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    if (self->socailMediaResponse != nil)
                                    {
                                        self->socailMediaResponse(nil);
                                    }
                                });
             }];
            
            socailMediaLog(@"Line Logout");
            
            break;
        }
        case SOCAIL_TYPE_GOOGLE:
        {
//            [[GIDSignIn sharedInstance] signOut];
//
//            [[GIDSignIn sharedInstance] disconnect];
//
//            socailMediaLog(@"Google Logout");
            
            break;
        }
        case SOCAIL_TYPE_FACEBOOK:
        {
            FBSDKLoginManager *fbloginManager = [[FBSDKLoginManager alloc] init];
            
            [fbloginManager logOut];
            
            break;
        }
        case SOCAIL_TYPE_APPLE:
        {
            
            
            break;
        }
        case SOCAIL_TYPE_TWITTER:
        {
//            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
//
//            [[Twitter sharedInstance].sessionStore logOutUserID:client.userID];
//
//            socailMediaLog(@"TW Logout");
//
//            dispatch_async(dispatch_get_main_queue(), ^
//                           {
//                               if (socailMediaResponse != nil)
//                               {
//                                   socailMediaResponse(nil);
//                               }
//                           });
            
            break;
        }
        default:
            break;
    }
}

- (void)refreshAccessToken:(_SOCAIL_TYPE)socailType Response:(SocailMediaResponse)response
{
    socailMediaResponse = response;
    
    switch (socailType)
    {
        case SOCAIL_TYPE_LINE:
        {
            [apiClient refreshTokenWithCompletion:^(LineSDKAccessToken *accessToken, NSError *error)
             {
                SocailProfile *profile = [self getProfile:socailType];
                
                dispatch_async(dispatch_get_main_queue(), ^
                {
                    if (error)
                    {
                        self->socailMediaResponse(nil);
                    }
                    else
                    {
                        if (self->socailMediaResponse != nil)
                        {
                            self->socailMediaResponse(profile);
                        }
                    }
                });
             }];
            
            socailMediaLog(@"Line RefreshToken");
            
            break;
        }
        case SOCAIL_TYPE_GOOGLE:
        {
//            GIDGoogleUser *googleUser = [[GIDSignIn sharedInstance] currentUser];
//
//            if (googleUser.authentication)
//            {
//                [googleUser.authentication refreshTokensWithHandler:^(GIDAuthentication *authentication, NSError *error)
//                 {
//                        dispatch_async(dispatch_get_main_queue(), ^
//                                   {
//                                        if (error != nil)
//                                        {
//                                            [[GIDSignIn sharedInstance] signIn];
//
//                                            self->socailMediaResponse(nil);
//
//                                            socailMediaLog(@"Google RefreshToken Failh SignIn");
//                                        }
//                                        else
//                                        {
//                                            SocailProfile *profile = [self getProfile:socailType];
//
//                                            if (self->socailMediaResponse != nil)
//                                            {
//                                                self->socailMediaResponse(profile);
//                                            }
//
//                                            socailMediaLog(@"Google RefreshToken");
//                                        }
//                                   });
//                 }];
//            }
//            else if ([GIDSignIn sharedInstance].hasPreviousSignIn)
//            {
//                [[GIDSignIn sharedInstance] restorePreviousSignIn];
//
//                socailMediaLog(@"Google RefreshToken With SignInSilently");
//            }
//            else
//            {
//                dispatch_async(dispatch_get_main_queue(), ^
//                {
//                    self->socailMediaResponse(nil);
//                });
//            }
            
            break;
        }
        case SOCAIL_TYPE_FACEBOOK:
        {
            [FBSDKAccessToken refreshCurrentAccessToken:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
             {
                 SocailProfile *profile = [self getProfile:socailType];

                 dispatch_async(dispatch_get_main_queue(), ^
                 {
                     if (error)
                     {
                         self->socailMediaResponse(nil);
                     }
                     else
                     {
                         if (self->socailMediaResponse != nil)
                         {
                             self->socailMediaResponse(profile);
                         }
                     }
                 });
             }];

            socailMediaLog(@"FB RefreshToken");
            
            break;
        }
        case SOCAIL_TYPE_TWITTER:
        {
//            SocailProfile *profile = [self getProfile:socailType];
//
//            socailMediaLog(@"TW RefreshToken With Get Profile");
//
//            dispatch_async(dispatch_get_main_queue(), ^
//                           {
//                               if (socailMediaResponse != nil)
//                               {
//                                   socailMediaResponse(profile);
//                               }
//                           });
//
            break;
        }
        default:
            break;
    }
}

- (SocailProfile *)getProfile:(_SOCAIL_TYPE)socailType
{
    __block SocailProfile *socailProfile = nil;
    
    switch (socailType) 
    {
        case SOCAIL_TYPE_LINE:
        {
            [apiClient getProfileWithCompletion:^(LineSDKProfile * profile, NSError * error)
             {
                 if (error)
                 {
                     socailMediaLog(@"Line Get Profile Fail");
                     
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        if (self->socailMediaResponse != nil)
                                        {
                                            self->socailMediaResponse(nil);
                                        }
                                    });
                 }
                 else
                 {
                     socailMediaLog(@"Line Get Profile Success");
                     
                     LineSDKProfile * profileInformation = profile;
                     
                     socailProfile = [[SocailProfile alloc] init];
                     
                     socailProfile.socailType = SOCAIL_TYPE_LINE;
                     
                     socailProfile.name = profileInformation.displayName;
                     
                     socailProfile.userId = profileInformation.userID;
                     
                     socailProfile.imageUrl = [profileInformation.pictureURL absoluteString];
                     
                     LineSDKAccessToken * accessTokenObject = [self->apiClient currentAccessToken];
                     
                     socailProfile.accessToken = accessTokenObject.accessToken;
                     
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        if (self->socailMediaResponse != nil)
                                        {
                                            self->socailMediaResponse(socailProfile);
                                        }
                                    });
                 }
             }];
            
            break;
        }
        case SOCAIL_TYPE_GOOGLE:
        {
//            GIDGoogleUser *googleUser = [[GIDSignIn sharedInstance] currentUser];
//
//            if (googleUser.authentication)
//            {
//                socailMediaLog(@"Google Get Profile Success");
//
//                NSURL *url = [[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:150];
//
//                socailProfile = [[SocailProfile alloc] init];
//
//                socailProfile.socailType z= SOCAIL_TYPE_GOOGLE;
//
//                socailProfile.name = [GIDSignIn sharedInstance].currentUser.profile.name;
//
//                socailProfile.userId = [GIDSignIn sharedInstance].currentUser.userID;
//
//                socailProfile.imageUrl = [url absoluteString];
//
//                socailProfile.accessToken = googleUser.authentication.accessToken;
//
//                dispatch_async(dispatch_get_main_queue(), ^
//                               {
//                                   if (self->socailMediaResponse != nil)
//                                   {
//                                       self->socailMediaResponse(socailProfile);
//                                   }
//                               });
//            }
//            else
//            {
//                socailMediaLog(@"Google Get Profile Fail");
//
//                dispatch_async(dispatch_get_main_queue(), ^
//                               {
//                                   if (self->socailMediaResponse != nil)
//                                   {
//                                       self->socailMediaResponse(nil);
//                                   }
//                               });
//            }
//
            break;
        }
        case SOCAIL_TYPE_FACEBOOK:
        {
            if ([FBSDKAccessToken currentAccessToken] && [FBSDKProfile currentProfile])
            {
                socailMediaLog(@"FB Get Profile Success");

                socailProfile = [[SocailProfile alloc] init];

                socailProfile.socailType = SOCAIL_TYPE_FACEBOOK;

                socailProfile.name = [FBSDKProfile currentProfile].name;

                socailProfile.userId = [FBSDKProfile currentProfile].userID;

                socailProfile.accessToken = [FBSDKAccessToken currentAccessToken].tokenString;

                NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:@"picture.width(150).height(150)", @"fields", nil];

                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:param] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                {
                    NSDictionary *picDic = result;

                    NSString *url = picDic[@"picture"][@"data"][@"url"];

                    socailProfile.imageUrl = url;

                    dispatch_async(dispatch_get_main_queue(), ^
                                   {
                                       if (self->socailMediaResponse != nil)
                                       {
                                           self->socailMediaResponse(socailProfile);
                                       }
                                   });
                    
                    socailMediaLog(@"FB Get Profile Success");
                }];
            }
            else
            {
                socailMediaLog(@"FB Get Profile Fail");

                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   if (self->socailMediaResponse != nil)
                                   {
                                       self->socailMediaResponse(nil);
                                   }
                               });
            }
            
            break;
        }
        case SOCAIL_TYPE_TWITTER:
        {
//            if ([Twitter sharedInstance].sessionStore.hasLoggedInUsers)
//            {
//                TWTRSession *session = [Twitter sharedInstance].sessionStore.session;
//
//                if (session)
//                {
//                    socailMediaLog(@"TW Get Profile Success");
//
//                    socailProfile = [[SocailProfile alloc] init];
//
//                    socailProfile.socailType = SOCAIL_TYPE_TWITTER;
//
//                    socailProfile.name = [session userName];
//
//                    socailProfile.userId = [session userID];
//
//                    socailProfile.imageUrl = @"";
//
//                    socailProfile.view = nil;
//
//                    socailProfile.accessToken = [session authToken];
//
//                    socailProfile.providerSecret = [session authTokenSecret];
//
//                    TWTRAPIClient *apiclient = [[TWTRAPIClient alloc] initWithUserID:session.userID];
//
//                    [apiclient loadUserWithID:session.userID completion:^(TWTRUser * _Nullable user, NSError * _Nullable error)
//                    {
//                        socailProfile.imageUrl = user.profileImageLargeURL;
//
//                        dispatch_async(dispatch_get_main_queue(), ^
//                                       {
//                                           if (socailMediaResponse != nil)
//                                           {
//                                               socailMediaResponse(socailProfile);
//                                           }
//                                       });
//                    }];
//
//
//                }
//                else
//                {
//                    socailMediaLog(@"TW Get Profile Fail");
//
//                    dispatch_async(dispatch_get_main_queue(), ^
//                                   {
//                                       if (socailMediaResponse != nil)
//                                       {
//                                           socailMediaResponse(nil);
//                                       }
//                                   });
//                }
//            }
//            else
//            {
//                socailMediaLog(@"TW Get Profile Fail");
//
//                dispatch_async(dispatch_get_main_queue(), ^
//                               {
//                                   if (socailMediaResponse != nil)
//                                   {
//                                       socailMediaResponse(nil);
//                                   }
//                               });
//            }
            
            break;
        }
        default:
            break;
    }
    
    return socailProfile;
}

#pragma mark LineSDKLoginDelegate

- (void)didLogin:(LineSDKLogin *)login credential:(LineSDKCredential *)credential profile:(LineSDKProfile *)profile error:(NSError *)error
{
    if (error)
    {
        socailMediaLog(@"Line Did Login fail");
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                            if (self->socailMediaResponse != nil)
                           {
                               self->socailMediaResponse(nil);
                           }
                       });
    }
    else 
    {
        [self getProfile:SOCAIL_TYPE_LINE];
    }
}

#pragma mark - Facebook FBSDKLoginButtonDelegate

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error 
{
    if (error || result.isCancelled)
    {
        socailMediaLog(@"FB login fail");
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           if (self->socailMediaResponse != nil)
                           {
                               self->socailMediaResponse(nil);
                           }
                       });
    }
//    else
//    {
//        [self getProfile:SOCAIL_TYPE_FACEBOOK];
//    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton 
{
    socailMediaLog(@"FB logout didCompleteWithResult");
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       if (self->socailMediaResponse != nil)
                       {
                           self->socailMediaResponse(nil);
                       }
                   });
}

#pragma mark - Facebook Observations

- (void)observeProfileChange:(NSNotification *)notfication
{
    socailMediaLog(@"FB observeProfileChange");

    if ([FBSDKProfile currentProfile])
    {
        [self getProfile:SOCAIL_TYPE_FACEBOOK];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           if (self->socailMediaResponse != nil)
                           {
                               self->socailMediaResponse(nil);
                           }
                       });
    }
}

//#pragma mark - GIDSignInDelegate
//
//- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
//{
//    if (error)
//    {
//        socailMediaLog(@"Google didSignInForUser Fail");
//
//        [self logout:SOCAIL_TYPE_GOOGLE Response:^(SocailProfile *socailProfile)
//        {
//            dispatch_async(dispatch_get_main_queue(), ^
//                           {
//                               self->socailMediaResponse(nil);
//                           });
//        }];
//
//    }
//    else
//    {
//        socailMediaLog(@"Google didSignInForUser Success");
//
//        if (socailMediaResponse != nil)
//        {
//            GIDGoogleUser *googleUser = [[GIDSignIn sharedInstance] currentUser];
//
//            if (googleUser.authentication)
//            {
//                NSURL *url = [[GIDSignIn sharedInstance].currentUser.profile imageURLWithDimension:100];
//
//                SocailProfile *socailProfile = [[SocailProfile alloc] init];
//
//                socailProfile.socailType = SOCAIL_TYPE_GOOGLE;
//
//                socailProfile.name = [GIDSignIn sharedInstance].currentUser.profile.name;
//
//                socailProfile.userId = [GIDSignIn sharedInstance].currentUser.userID;
//
//                socailProfile.imageUrl = [url absoluteString];
//
//                socailProfile.accessToken = googleUser.authentication.accessToken;
//
//                dispatch_async(dispatch_get_main_queue(), ^
//                               {
//                    if (self->socailMediaResponse != nil)
//                                   {
//                                       self->socailMediaResponse(socailProfile);
//                                   }
//                               });
//            }
//        }
//        else
//        {
//            socailMediaLog(@"Google socailMediaResponse is nil");
//        }
//    }
//}
//
//- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error
//{
//    socailMediaLog(@"Google didDisconnectWithUser");
//
//    dispatch_async(dispatch_get_main_queue(), ^
//                   {
//                       if (self->socailMediaResponse != nil)
//                       {
//                           self->socailMediaResponse(nil);
//                       }
//                   });
//}
//
//- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
//{
//    socailMediaLog(@"Google dismissViewController");
//
//    [rootView dismissViewControllerAnimated:YES completion:^(void)
//    {
//
//    }];
//}
//
//- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
//{
//    socailMediaLog(@"Google presentViewController");
//
//    [rootView presentViewController:viewController animated:YES completion:^(void)
//    {
//
//    }];
//}


#pragma mark - ASAuthorizationControllerDelegate

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization*)authorization API_AVAILABLE(ios(13.0))
{
    if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]])
    {
        ASAuthorizationAppleIDCredential *credential = authorization.credential;
        
        NSString *userID = credential.user;
        
        NSPersonNameComponents *fullName = credential.fullName;
        
//        NSString *email = credential.email;
//
//        NSString *authorizationCode = [[NSString alloc] initWithData:credential.authorizationCode encoding:NSUTF8StringEncoding];
//
        NSString *identityToken = [[NSString alloc] initWithData:credential.identityToken encoding:NSUTF8StringEncoding];
//
//        ASUserDetectionStatus realUserStatus = credential.realUserStatus;
        
        SocailProfile *socailProfile = [[SocailProfile alloc] init];
        
        socailProfile.socailType = SOCAIL_TYPE_APPLE;
        
        socailProfile.name = fullName.givenName;
        
        socailProfile.userId = userID;
        
        socailProfile.imageUrl = nil;
        
        socailProfile.accessToken = identityToken;
        
        if (self->socailMediaResponse != nil)
        {
            self->socailMediaResponse(socailProfile);
        }
    }
    else if ([authorization.credential isKindOfClass:[ASPasswordCredential class]])
    {
        ASPasswordCredential * passwordCredential = authorization.credential;
        
        NSString *userID = passwordCredential.user;
        
        NSString *password = passwordCredential.password;
        
        NSLog(@"userID = %@", userID);
        
        NSLog(@"password = %@", password);
        
        if (self->socailMediaResponse != nil)
        {
            self->socailMediaResponse(nil);
        }
    }
    else
    {
        if (self->socailMediaResponse != nil)
        {
            self->socailMediaResponse(nil);
        }
    }
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError*)error API_AVAILABLE(ios(13.0))
{
    NSString *errorMsg = nil;
    
    switch (error.code)
    {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"ASAuthorizationErrorCanceled";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"ASAuthorizationErrorFailed";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"ASAuthorizationErrorInvalidResponse";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"ASAuthorizationErrorNotHandled";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"ASAuthorizationErrorUnknown";
            break;
    }
    
    NSLog(@"errorMsg = %@", errorMsg);
    
    if (self->socailMediaResponse != nil)
    {
        self->socailMediaResponse(nil);
    }
}

//- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller
//{
//    return self.view.window;
//}

@end
