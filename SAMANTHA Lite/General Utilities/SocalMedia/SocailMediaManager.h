//
//  SocailMediaManager.h
//  12Ours
//
//  Created by Blues on 2017/9/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SocailProfile.h"
#import <LineSDK/LineSDK.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AuthenticationServices/AuthenticationServices.h>
//#import <GoogleSignIn/GoogleSignIn.h>
//#import <TwitterKit/TwitterKit.h>

//#define GOOGLE_CLIENTID             @"76695776483-imjd2i7eke7i2sr5ra01uplej2eeuh6k.apps.googleusercontent.com"
//#define TWITTER_ACC                 @"j9nmlc0yiIK4jxgSWs6NwwWo8"
//#define TWITTER_PWD                 @"Cc6buX5UtmCQI9xscqpEQcj4tld1GDiw1XAB9xuL1LeLCgZQzu"

typedef void (^SocailMediaResponse)(SocailProfile *socailProfile);

@interface SocailMediaManager : NSObject<LineSDKLoginDelegate, FBSDKLoginButtonDelegate, /*GIDSignInDelegate,*/ ASAuthorizationControllerDelegate>//, ASAuthorizationControllerPresentationContextProviding>
{
    FBSDKLoginButton *fbLogin;
    
    LineSDKAPI *apiClient;
    
    SocailMediaResponse socailMediaResponse;
}

@property (weak, nonatomic) id rootView;

#pragma mark - init

+ (SocailMediaManager *)sharedInstance;

- (void)registerSocalMedia:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

#pragma mark - Socail Func

- (void)login:(_SOCAIL_TYPE)socailType Response:(SocailMediaResponse)response;

- (void)logout:(_SOCAIL_TYPE)socailType Response:(SocailMediaResponse)response;

- (void)refreshAccessToken:(_SOCAIL_TYPE)socailType Response:(SocailMediaResponse)response;

@end
