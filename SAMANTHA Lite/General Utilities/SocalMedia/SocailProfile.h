//
//  SocailProfile.h
//  12Ours
//
//  Created by Blues on 2017/9/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, _SOCAIL_TYPE)
{
    SOCAIL_TYPE_NONE,
    SOCAIL_TYPE_LINE,
    SOCAIL_TYPE_FACEBOOK,
    SOCAIL_TYPE_APPLE,
    SOCAIL_TYPE_GOOGLE,
    SOCAIL_TYPE_TWITTER,
    
} ;

@interface SocailProfile : NSObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *userId;

@property (nonatomic, strong) NSString *imageUrl;

@property (nonatomic, assign) _SOCAIL_TYPE socailType;

@property (nonatomic, readonly) NSString *socailTypeName;

@property (nonatomic, strong) NSString *accessToken;

@property (nonatomic, strong) NSString *providerSecret;

- (void)printSelf;

@end
