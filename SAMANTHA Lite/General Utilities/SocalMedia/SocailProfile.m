//
//  SocailProfile.m
//  12Ours
//
//  Created by Blues on 2017/9/12.
//  Copyright © 2017年 12Ours. All rights reserved.
//

#import "SocailProfile.h"

@implementation SocailProfile
@synthesize name;
@synthesize userId;
@synthesize imageUrl;
@synthesize socailType;
@synthesize accessToken;
@synthesize providerSecret;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        socailType = SOCAIL_TYPE_NONE;
        name = @"";
        userId = @"";
        imageUrl = @"";
        accessToken = @"";
        providerSecret = @"";
    }
    
    return self;
}

- (NSString *)socailTypeName
{
    switch (socailType)
    {
        case SOCAIL_TYPE_FACEBOOK:
            return @"facebook";
            break;
        case SOCAIL_TYPE_GOOGLE:
            return @"google";
            break;
        case SOCAIL_TYPE_LINE:
            return @"line";
            break;
        case SOCAIL_TYPE_TWITTER:
            return @"twitter";
            break;
        case SOCAIL_TYPE_APPLE:
            return @"apple";
            break;
        default:
            return @"";
            break;
    }
}

- (void)printSelf
{
    NSLog(@"---- SocailProfile ----");
    NSLog(@"socailType = %ld",(long)socailType);
    NSLog(@"name = %@",name);
    NSLog(@"userId = %@",userId);
    NSLog(@"imageUrl = %@",imageUrl);
    NSLog(@"accessToken = %@",accessToken);
}

@end
